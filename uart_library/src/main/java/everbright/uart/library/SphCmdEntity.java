package everbright.uart.library;

/**
 * 串口命令
 * @author：everbright6666
 * @date：2020/02/22 18:35
 */
public class SphCmdEntity {

    public SphCmdEntity() {
    }

    public SphCmdEntity(byte[] commands) {
        this.commands = commands;
        this.commandsHex = DataConversion.encodeHexString(commands);
    }

    /**
     * 串口发送或者返回的命令
     */
    public byte[] commands;

    /**
     * 串口发送或者返回的命令(hex)
     */
    public String commandsHex;

    /**
     * 备用标识
     */
    public int flag;



}
