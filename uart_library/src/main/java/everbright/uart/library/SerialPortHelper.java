package everbright.uart.library;

import android.util.Log;

/**
 * 现调机串口驱动
 * @author ：everbright6666
 * @date：2020/02/23 18:26
 */
public class SerialPortHelper {

    private final static String TAG = SerialPortHelper.class.getSimpleName();

    private boolean mIsOpen = false;

    private SerialPortConfig serialPortConfig;
    //操作句杯
    private int fd;
    /**
     * 最大接收数据的长度
     */
    private int maxSize;

    /**
     * 初始化串口操作
     * @param maxSize 串口每次读取数据的最大长度
     */
    public SerialPortHelper(int maxSize){
        this(maxSize, new SerialPortConfig());
    }
    /**
     * 初始化串口操作
     * @param maxSize           串口每次读取数据的最大长度
     * @param serialPortConfig  串口数据
     */
    public SerialPortHelper(int maxSize,SerialPortConfig serialPortConfig) {
        this.maxSize = maxSize;
        this.serialPortConfig = serialPortConfig;
    }

    /**
     * 串口设置
     */
    public void setConfigInfo(SerialPortConfig serialPortConfig){
        this.serialPortConfig = serialPortConfig;
    }

    /**
     * 打开串口设备
     * @param path  串口地址
     */
    public boolean openDevice(String path){
        this.serialPortConfig.path = path;
        return openDevice();
    }

    /**
     * 打开串口设备
     * @param path      串口地址
     * @param baudRate  波特率
     */
    public boolean openDevice(String path,int baudRate){
        this.serialPortConfig.path = path;
        this.serialPortConfig.baudRate = baudRate;
        return openDevice();
    }


    /**
     * 打开串口设备
     */
    public boolean openDevice(){
        if(serialPortConfig==null){
            throw new IllegalArgumentException("'SerialPortConfig' must can not be null!!! ");
        }
        if(serialPortConfig.path == null){
            throw new IllegalArgumentException("You not have setting the device path, " +
                    "you must 'new SerialPortHelper(String path)' or call 'openDevice(String path)' ");
        }
             fd = SerialPortJNI.openPort(
                this.serialPortConfig.path,
                this.serialPortConfig.baudRate,
                this.serialPortConfig.dataBits,
                this.serialPortConfig.stopBits,
                this.serialPortConfig.parity);

        // 是否设置原始模式(Raw Mode)方式来通讯
        if(serialPortConfig.mode!=0){
            SerialPortJNI.setMode(fd,serialPortConfig.mode);
        }

        // 打开串口成功
        if(fd>0){
            mIsOpen = true;
            // 开启读写线程
/*            readThread = new ReadThread();
            readThread.start();*/
        }else{
            mIsOpen = false;
            Log.e(TAG,"cannot open the device !!! " +
                    "path:"+serialPortConfig.path);
        }
        return mIsOpen;
    }

    /**
     * 接收串口命令（hex）
     * @param timeout  超时时间，为0时为阻塞
     */
    public byte[] receiveCommands(int maxsize,int timeout)
    {
        byte[] bytes = SerialPortJNI.readPort(fd,maxsize,timeout);
        return bytes;
    }

    /**
     * 发送串口命令（hex）
     * @param hex  十六进制命令
     * @param flag 备用标识
     */
    public void sendCommands(String hex){
        byte[] bytes = DataConversion.decodeHexString(hex);
        sendCommands(bytes,0);
    }
    public void sendCommands(byte[] commands){
        SphCmdEntity comEntry = new SphCmdEntity();
        comEntry.commands = commands;
        comEntry.flag = 0;
        comEntry.commandsHex = DataConversion.encodeHexString(commands);
        sendCommands(comEntry);
    }
    /**
     * 发送串口命令
     * @param commands 串口命令
     * @param flag     备用标识
     */
    public void sendCommands(byte[] commands,int flag){
        SphCmdEntity comEntry = new SphCmdEntity();
        comEntry.commands = commands;
        comEntry.flag = flag;
        comEntry.commandsHex = DataConversion.encodeHexString(commands);
        sendCommands(comEntry);
    }

    /**
     * 发送串口命令
     * @param sphCmdEntity 串口命令数据
     */
    public void sendCommands(SphCmdEntity sphCmdEntity){
        if(sphCmdEntity==null){
            Log.e(TAG,"SphCmdEntity can't be null !!!");
            return;
        }
        if(!isOpenDevice()){
            Log.e(TAG,"You not open device !!! ");
            return;
        }
        //发送指令
        SerialPortJNI.writePort(fd,sphCmdEntity.commands);
        Log.d(TAG, "发送命令：" + sphCmdEntity.commandsHex);
    }

    /**
     * 关闭串口
     */
    public void closeDevice(){
        SerialPortJNI.closePort(fd);

    }

    /**
     * 判断串口是否打开
     */
    public boolean isOpenDevice(){
        return mIsOpen;
    }

}


