package Exceptions;

public class TransAllowException extends Exception {

    private static final long serialVersionUID = -1013765236027899345L;

    public TransAllowException(String message)
    {
        super(message);
    }

}
