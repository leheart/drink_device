package Class;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import com.example.administrator.drinkdevice.R;

import org.json.JSONObject;

import java.util.List;

import Const.PublicDefine;
import DataAccess.SqlLiteDataAccess;
import Enums.MsgType;
import Helper.Msgbox;
import Interface.IAsynCallBackListener;
import Interface.ICard;
import Struct.BusiState;
import Class.*;


/**
 * Created by 王彦鹏 on 2017-08-25.
 */


public class JsCallInterface {
    public String mCallJsName="";
    private  Context    mContext;
    public static  int webViewCount=0;
    private Handler mHandler = new Handler();
    private WebView mwebview;
    public ICard card;


    /**
     * 卡移入事件回调接口
     */
    public String CallCardInName="";

    /**
     * 消息传递
     */
    public String CallMsg="";


    public void CallJs(final String MName, final String Param)
    {
        if (MName!="") {
            System.out.println("CallName:" + MName + "  Param:" + Param);
            mHandler.post(new Runnable() {
                public void run() {
                    mwebview.loadUrl("javascript:" + MName + "('" + Param + "' );");
                }
            });
        }
        else
        {
            System.out.println(mContext.getResources().getString(R.string.JsCallInterface_NoCallbackIsGenerated));
        }
    }

    public JsCallInterface(Context ctx, WebView view)
    {
        mContext=ctx;
        mwebview=view;

    }

    @JavascriptInterface
    public int deleterow()
    {
        Log.d("删除行","删除行");
        return 0;
    }

    @JavascriptInterface
    public int deleteTable(final String tab)
    {
         Msgbox.Show(mContext, mContext.getResources().getString(R.string.JsCallInterface_ClearTableData), String.format(mContext.getResources().getString(R.string.JsCallInterface_AreYouSureClearTable_tip_Data), tab), MsgType.msg_Query, new IAsynCallBackListener() {
            @Override
            public void onFinish(Object sender,Object data) {
                SqlLiteDataAccess sqllite=new SqlLiteDataAccess(mContext);
                sqllite.ExecSql("delete from "+tab);
            }

            @Override
            public void onError(Object sender, Exception e) {

            }
        },null);
        return 0;
    }
    @JavascriptInterface
    public String getTable(String tab) throws Exception {
        SqlLiteDataAccess sqllite=new SqlLiteDataAccess(mContext);
        String htmstr="   <table class=\"gridtable\">\n" +
                "    <tr>\n" ;
        List<JSONObject> table= sqllite.Select("select * from "+tab);
        if (table.size()>0) {
            for(int i=0;i<table.get(0).names().length();i++) {
                htmstr+= "<th>"+table.get(0).names().get(i).toString()+"</th>" ;
            }
            htmstr+="<th>"+mContext.getResources().getString(R.string.JsCallInterface_Operation)+"</th>  </tr>\n" ;

            for (JSONObject json : table) {
                htmstr+="    <tr>\n" ;
                for(int i=0;i<json.names().length();i++) {
                    htmstr+= "<td>"+json.getString(json.names().get(i).toString())+"</td>" ;
                }
                htmstr+="<td><a href=\"#\" onclick=\"return delrow(this);\">"+mContext.getResources().getString(R.string.JsCallInterface_Delete)+"</a></td>\n" ;
                htmstr+="    </tr>\n" ;
            }
            htmstr+="    </table>\n" ;
            htmstr+="    <div style=\"margin-top: 20px;float: right;\">\n" +
                    "    <button onclick=\"return deltable('"+tab+"');\">"+mContext.getResources().getString(R.string.JsCallInterface_ClearThisTable)+"</button>\n" +
                    "    </div>\n" ;
            return htmstr;
        }
        return mContext.getResources().getString(R.string.JsCallInterface_ThisTableHasNoDataAvailable);
    }


}
