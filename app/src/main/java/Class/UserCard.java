package Class;

import android.content.Context;
import android.content.ContextWrapper;

import com.example.administrator.drinkdevice.R;

import Const.PublicDefine;
import Interface.IUserCard;
import Interface.IWebService;
import Service.WebServer;
import Factory.Factory;

/**
 * Created by 王彦鹏 on 2017-09-15.
 */

public class UserCard extends ContextWrapper implements IUserCard {
    private Context ctx;
    protected IWebService webService;

    public UserCard(Context base) {
        super(base);
        this.ctx = base;
        webService= Factory.GetInstance(WebServer.class,new Object[]{ctx});
    }

}
