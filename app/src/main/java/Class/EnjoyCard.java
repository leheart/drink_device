package Class;

import android.content.Context;
import android.content.Intent;

import com.example.administrator.drinkdevice.R;

import Const.PublicDefine;

import DataAccess.OptionDataAccess;
import Entity.BaseInfo;
import Helper.AESKeyModel;
import Helper.ByteControl;
import Helper.CRCControl;
import Helper.RSAUtils;
import Interface.ICard;
import Interface.IEnjoyCard;
import Struct.ReaderState;


public class  EnjoyCard implements IEnjoyCard {

    /**
     * 卡读写密码
     */
    protected byte[] cardPwd=new byte[6];
    /**
     * 数据密钥
     */
    protected byte[] dataKey=new byte[16];
    protected Context ctx;
    protected ICard card;

    public EnjoyCard(Context ctx) {
    }

    @Override
    public void setCard(ICard card)
    {
        this.card=card;
    }

    @Override
    public ICard getCard() {
        return card;
    }

    @Override
    public void SetIntent(Context ctx, Intent intent)  throws Exception
    {
        this.ctx=ctx;
        card.SetIntent(ctx,intent);
    }


    //    public  EnjoyCard  () {
//        super();
//    }
    @Override
    public boolean ExistsCard()
    {
        return card.ExistsCard();
    }

    @Override
    public void initCardPwd(byte[] bufId)
    {
        System.arraycopy(bufId,0,cardPwd,0,4);

        cardPwd[4]= cardPwd[0];
        cardPwd[5] = cardPwd[2];

    }

    public long GetCardNo()
    {
        return card.GetCardNo();
    }
    @Override
    public ReaderState GetReaderState() throws Exception
    {
        byte[] buf=card.GetReaderState();
        if(buf==null )
        {
            throw new Exception(ctx.getResources().getString(R.string.EnjoyCard_FailedToGetCardReadingDeviceStatus));
        }
        ReaderState res=new ReaderState();
        res.LoadData(buf);
        return res;
    }

    public BaseInfo ReadBaseInfo() throws Exception {
        long t1,t2;
        t1 = System.currentTimeMillis();

        BaseInfo info = new BaseInfo();
        byte[] b = card.ReadBlock(1, cardPwd);
        if (b == null) {
            b = card.ReadBlock(2, cardPwd);
            if (b==null)
            {
                throw new Exception(ctx.getResources().getString(R.string.EnjoyCard_FailedToReadCardPleaseRetry));
            }
        }

        t2 = System.currentTimeMillis();
        //解密信息
        b = AESKeyModel.decrypt(b, PublicDefine.commonAesKey);
        //验证CRC
        if (CRCControl.checkCrc16(b)) {
            info.LoadData(b);
            return info;
        }

        t1 = System.currentTimeMillis();

        b = card.ReadBlock(2, cardPwd);
        t2 = System.currentTimeMillis();
        byte[] buf=b;
        b = AESKeyModel.decrypt(b, PublicDefine.commonAesKey);
        //验证CRC
        if (CRCControl.checkCrc16(b)) {
            info.LoadData(b);
            return info;
        } else {
            info.LoadData(buf);
            return info;
            //throw new Exception("读卡失败，请重新刷卡");
        }
    }


    public void beep(int times){
        for(int i=1;i<=times;i++)
        {
            card.beep(50);
            try {
                Thread.sleep(100);
            }
            catch (Exception e)
            {

            }
        }
    }

    @Override
    public boolean connected() {
        return card.connected();
    }

}

