package Class;


import android.content.Context;
import android.os.AsyncTask;

import java.io.PrintWriter;
import java.io.StringWriter;

import Interface.IAsyncCallBack;
import Helper.*;

/**
 * Created by 王彦鹏 on 2017-09-23.
 */

public class AsyncTrans extends AsyncTask<IAsyncCallBack, Object, Object> {

    private Context ctx;
    ProgersssDialog progersssDialog;
    IAsyncCallBack callBack;

    public AsyncTrans(ProgersssDialog dialog)
    {
        progersssDialog=dialog;
    }


    @Override
    protected Object doInBackground(IAsyncCallBack... iAsyncCallBacks) {
        try {
            callBack=iAsyncCallBacks[0];
            return callBack.Execute();
        }
        catch (Exception e)
        {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw, true));
            Log.write("Exception",sw.toString());
            e.printStackTrace();
            iAsyncCallBacks[0].SetThrowErr(e);
            return null;
        }
    }

    @Override
    protected void   onPreExecute() {
        //这个方法是在执行异步任务之前的时候执行 }，
        // 并且是在UI Thread当中执行的，通常我们在这个方法里做一些UI控件的初始化的操作，例如弹出要给ProgressDialog
        if (progersssDialog!=null) {
            progersssDialog.show();
        }
    }
    @Override
    protected void onProgressUpdate(Object... values){
        // 这个方法也是在UI Thread当中执行的，我们在异步任务执行的时候，有时候需要将执行的进度返回给我们的UI界面
        // 例如下载一张网络图片，我们需要时刻显示其下载的进度，就可以使用这个方法来更新我们的进度。
        // 这个方法在调用之前，我们需要在 doInBackground 方法中调用一个 publishProgress(Progress)
        // 的方法来将我们的进度时时刻刻传递给 onProgressUpdate 方法来更新
    }
    @Override
    protected void onPostExecute(Object result) {
        if (progersssDialog!=null) {
            progersssDialog.hide();
        }
        try {
            callBack.ExcuteComplete(result);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw, true));
            Log.write("Exception",sw.toString());
            e.printStackTrace();
        }
    }
}
