/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Helper;

import Helper.ByteControl;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class TimeFormat {

    private Calendar c;
    private int year;
    private int month;
    private int day;
    private int hour;
    private int minute;
    private int second;

    public TimeFormat(byte[] b) {
        //long i = typeConvert.byte2int(b,0);
        long i = ByteControl.byte4ToLong(b, 0);
        second = (int) (i & 0x7f);
        minute = (int) (((i >> 7) & 0xFF) & 0x7f);
        hour = (int) (((i >> 14) & 0xFF) & 0x1f);
        day = (int) (((i >> 19) & 0xFF) & 0x1f);
        month = (int) (((i >> 24) & 0xFF) & 0x0f);
        year = (int) (((i >> 28) & 0xFF) & 0x0f);
        year = year+2010;
    }
    public TimeFormat(byte[] b, int index) {
        //long i = typeConvert.byte2int(b,0);
        long i = ByteControl.byte4ToLong(b, index);
        second = (int) (i & 0x7f);
        minute = (int) (((i >> 7) & 0xFF) & 0x7f);
        hour = (int) (((i >> 14) & 0xFF) & 0x1f);
        day = (int) (((i >> 19) & 0xFF) & 0x1f);
        month = (int) (((i >> 24) & 0xFF) & 0x0f);
        year = (int) (((i >> 28) & 0xFF) & 0x0f);
        year = year + 2010;
    }
    public TimeFormat(String TimeStr,String Format)
    {
        if (Format.equals("yyyy-MM-dd hh:mm:ss")) {
            year = Integer.parseInt(TimeStr.substring(0,4));
            month = Integer.parseInt(TimeStr.substring(5,7));
            day = Integer.parseInt(TimeStr.substring(8,10));
            hour = Integer.parseInt(TimeStr.substring(11,13));
            minute = Integer.parseInt(TimeStr.substring(14,16));
            second = Integer.parseInt(TimeStr.substring(17,19));            
        }
        else if (Format.equals("yyyyMMddhhmmss"))//20120827140532
        {
            year = Integer.parseInt(TimeStr.substring(0,4));
            month = Integer.parseInt(TimeStr.substring(4,6));
            day = Integer.parseInt(TimeStr.substring(6,8));
            hour = Integer.parseInt(TimeStr.substring(8,10));
            minute = Integer.parseInt(TimeStr.substring(10,12));
            second = Integer.parseInt(TimeStr.substring(12,14));  
        }
        else
        {
            year = 2000;
            month = 1;
            day = 1;
            hour = 0;
            minute = 0;
            second = 0;            
        }
    }
    
    public byte[] tobyte6(){
        byte[] res = new byte[6];
        
        res[0] = (byte) (year-2000);
        res[1] = (byte)month;
        res[2] = (byte)day;
        res[3] = (byte)hour;
        res[4] = (byte)minute;
        res[5] = (byte)second;
        
        return res;
    }
    public void LoadFromByte6(byte [] b,int index)
    {
        year = (int)b[index+0]+2000;
        month = (int)b[index+1];
        day = (int)b[index+2];
        hour = (int)b[index+3];
        minute = (int)b[index+4];
        second = (int)b[index+5];
    }
    

    public byte[] tobyte() {
        long l = 0;
        
        int tmp = year;
        if(tmp<2010){
            tmp = 2010;
        }
        l = (tmp-2010)<<28;
        tmp = month <<24;
        l = l+tmp;
        tmp = day<<19;
        l = l+tmp;
        tmp = hour<<14;
        l = l+tmp;
        tmp = minute<<7;
        l = l+tmp;
        tmp = second;
        l = l+tmp;

        //byte[] res = typeConvert.int2byte((int)l);
        byte[] res = ByteControl.intTobyte4((int)l);

        return res;
    }

    public TimeFormat(Date date) {
        c = Calendar.getInstance();
        c.setTime(date);
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH) + 1;
        day = c.get(Calendar.DAY_OF_MONTH);
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);
        second = c.get(Calendar.SECOND);

    }

    public String getString() {
        String s = year + "";
        if (month < 10) {
            s = s + "-0" + month;
        } else {
            s = s + "-" + month;
        }
        if (day < 10) {
            s = s + "-0" + day;
        } else {
            s = s + "-" + day;
        }
        s = s + " ";
        if (hour < 10) {
            s = s + "0" + hour;
        } else {
            s = s + hour;
        }
        if (minute < 10) {
            s = s + ":0" + minute;
        } else {
            s = s + ":" + minute;
        }
        if (second < 10) {
            s = s + ":0" + second;
        } else {
            s = s + ":" + second;
        }

        return s;
    }
    
    public String getString2() {
        String s = year + "";
        if (month < 10) {
            s = s + "0" + month;
        } else {
            s = s  + month;
        }
        if (day < 10) {
            s = s + "0" + day;
        } else {
            s = s + day;
        }
        if (hour < 10) {
            s = s + "0" + hour;
        } else {
            s = s + hour;
        }
        if (minute < 10) {
            s = s + "0" + minute;
        } else {
            s = s  + minute;
        }
        if (second < 10) {
            s = s + "0" + second;
        } else {
            s = s  + second;
        }

        return s;
    }
}
