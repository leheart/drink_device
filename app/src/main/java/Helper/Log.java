package Helper;

/**
 * 日志对象
 * Created by 王彦鹏 on 2018-03-29.
 */
public class Log {
    private static PrintLog printLog;
    /**
     * 写日志
     * @param tag 标签
     * @param log 日志内容
     */
    public static void write(String tag, String log)
    {
        if (tag.toLowerCase().contains("ttys0")){
            return;
        }
        if (printLog==null) {
            printLog = new PrintLog();
        }
        printLog.printLogMsg(tag,log);
        android.util.Log.d(tag,log);
    }
    public static void d(String tag, String log)
    {
        write(tag,log);
    }
}
