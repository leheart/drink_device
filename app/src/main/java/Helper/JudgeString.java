package Helper;

public class JudgeString {
    //判断一个字符串是否都是数字
    public static boolean isDigit(String str){
        return str.matches("[0-9]{1,}");
    }

    public static boolean isSlash(String str){
        int num = 0;
        if(str.charAt(0)!='/'&&str.charAt(str.length()-1)!='/'){
            for(int i = 1;i<str.length()-1;i++){
                if(str.charAt(i)=='/'){
                    num++;
                }
            }
            if(num==1){
                return true;
            }
        }
        return false;
    }

    public static String returnMerchant(String str){
        String merchant = "";
        for(int i = 1;i<str.length()-1;i++){
            if(str.charAt(i)=='/'){
                merchant = str.substring(0,i);
            }
        }
        return merchant;
    }

    public static String returnSite(String str){
        String site = "";
        for(int i = 1;i<str.length()-1;i++){
            if(str.charAt(i)=='/'){
                site = str.substring(i+1,str.length());
            }
        }
        return site;
    }
}
