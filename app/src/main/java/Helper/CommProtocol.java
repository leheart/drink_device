package Helper;

import android.content.Context;

import Entity.BaseEnity;
import Factory.Factory;
import org.json.JSONObject;

import Interface.IAsynCallBackListener;
import Interface.ICommProtocol;
import Interface.IHttp;

/**
 * Created by Administrator on 2017-09-02.
 */

public class CommProtocol implements ICommProtocol {

    private Context ctx;
    private IHttp http;

    public CommProtocol(Context context){
        this.ctx = context;
        http=Factory.GetInstance(Http.class,new Object[]{ctx});
    }

    //在这里更换通讯协议
    @Override
    public  String Get(String url) throws Exception {

        return http.Get(url);
    }

    @Override
    public  String Post(String url,String postData) throws Exception {

        return http.Post(url,postData);
    }

    @Override
    public String Post(String url, String postData, String contentType) throws Exception {
        return http.Post(url,postData,contentType);
    }

    @Override
    public  void Post(String url, String postData,IAsynCallBackListener callback) throws Exception
    {
        http.Post(url,postData,callback);
    }

    @Override
    public String Post(String url, String postData, IAsynCallBackListener callback, String contentType) throws Exception {
        return http.Post(url,postData,callback,contentType);
    }

    @Override
    public  JSONObject CallWebService(String url,String IName, BaseEnity... entity) throws Exception {
        return http.CallWebService(url,IName,entity);
    }

    @Override
    public String Get(String url, IAsynCallBackListener callback) throws Exception {
        return http.Get(url,callback);
    }

    @Override
    public void CallWebService(String url, String IName, IAsynCallBackListener callback, BaseEnity... entity) throws Exception {
        http.CallWebService(url,IName,callback,entity);
    }
}
