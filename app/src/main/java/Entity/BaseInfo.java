package Entity;

import java.util.Date;

import Annotation.StructIndex;
import Struct.BinaryType;
import Struct.StructBase;

/**
 * Created by 王彦鹏 on 2017-09-09.
 */

public class BaseInfo extends StructBase {
    @StructIndex(0)
    private BinaryType ver = new BinaryType(1,byte.class);//   版本	1
    @StructIndex(1)
    private BinaryType acState = new BinaryType(1,byte.class);//    挂失状态	1
    @StructIndex(2)
    private BinaryType cardKind = new BinaryType(1,byte.class);//    卡种类	1
    @StructIndex(3)
    private BinaryType block = new BinaryType(1,byte.class);//    冻结标志	1
    @StructIndex(4)
    private BinaryType blockType = new BinaryType(1,byte.class);//冻结的设备类型
    @StructIndex(5)
    private BinaryType cNo = new BinaryType(1,byte.class);//机号
    @StructIndex(6)
    private BinaryType deviceID = new BinaryType(3,String.class);
    @StructIndex(7)
    private BinaryType opTime = new BinaryType(4,Date.class);//    冻结时间	4
    @StructIndex(8)
    private BinaryType cardType = new BinaryType(1,String.class);//会员类型
    @StructIndex(9)
    private BinaryType crc=new BinaryType(2,short.class);//Crc


    public BinaryType getVer() {
        return ver;
    }

    public void setVer(BinaryType ver) {
        this.ver = ver;
    }

    public BinaryType getAcState() {
        return acState;
    }

    public void setAcState(BinaryType acState) {
        this.acState = acState;
    }

    public BinaryType getCardKind() {
        return cardKind;
    }

    public void setCardKind(BinaryType cardKind) {
        this.cardKind = cardKind;
    }

    public BinaryType getBlock() {
        return block;
    }

    public void setBlock(BinaryType block) {
        this.block = block;
    }

    public BinaryType getBlockType() {
        return blockType;
    }

    public void setBlockType(BinaryType blockType) {
        this.blockType = blockType;
    }

    public BinaryType getCNo() {
        return cNo;
    }

    public void setCNo(BinaryType CNo) {
        this.cNo = CNo;
    }

    public BinaryType getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(BinaryType deviceID) {
        this.deviceID = deviceID;
    }

    public BinaryType getOpTime() {
        return opTime;
    }

    public void setOpTime(BinaryType opTime) {
        this.opTime = opTime;
    }

    public BinaryType getCardType() {
        return cardType;
    }

    public void setCardType(BinaryType cardType) {
        this.cardType = cardType;
    }

    public BinaryType getCrc() {
        return crc;
    }

    public void setCrc(BinaryType crc) {
        this.crc = crc;
    }



}
