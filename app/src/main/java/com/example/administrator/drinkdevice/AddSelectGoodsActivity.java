package com.example.administrator.drinkdevice;


import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.example.administrator.drinkdevice.databinding.ViewmodelAddselectgoodsBinding;
import com.example.administrator.drinkdevice.databinding.ViewmodelSysteminitBinding;

import BaseActivity.BaseActivity;
import Const.PublicDefine;
import DataAccess.OptionDataAccess;
import Factory.Factory;
import Service.WebServer;
import ViewModel.AddSelectGoods_ViewModel;
import ViewModel.SystemInit_ViewModel;

public class AddSelectGoodsActivity extends BaseActivity {

    private AddSelectGoods_ViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new AddSelectGoods_ViewModel(this);
        ViewmodelAddselectgoodsBinding dataBinding = DataBindingUtil.setContentView(this, R.layout.viewmodel_addselectgoods);
        dataBinding.setViewModel(viewModel);
        viewModel.Init();
    }
}
