package com.example.administrator.drinkdevice;


import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.example.administrator.drinkdevice.databinding.ViewmodelSysteminitBinding;

import Const.PublicDefine;
import DataAccess.OptionDataAccess;
import Service.WebServer;
import ViewModel.SystemInit_ViewModel;
import BaseActivity.BaseActivity;
import Factory.Factory;

public class SystemInitActivity extends BaseActivity {

    private SystemInit_ViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new SystemInit_ViewModel(this);
        ViewmodelSysteminitBinding dataBinding = DataBindingUtil.setContentView(this, R.layout.viewmodel_systeminit);
        dataBinding.setViewModel(viewModel);
        PublicDefine.optionData = new OptionDataAccess(this);
        PublicDefine.webService = Factory.GetInstance(WebServer.class, new Object[]{this});
        viewModel.Init();
    }
}
