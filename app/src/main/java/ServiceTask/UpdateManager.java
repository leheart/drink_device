package ServiceTask;

import android.content.Context;


/**
 * App升级
 */

public class UpdateManager  {

    private static UpdateControll update;
	/**
	 * 检测软件更新
	 */
	public static void DownFile(Context ctx, String url, boolean install, IDownProgress downProgress)
	{
		update=new UpdateControll(ctx,install, downProgress);
		update.appUrl=url;
	}

	/**
	 * 终止程序并删除缓存文件
	 * @param fileName
	 */
	public static void DownCancel(String fileName)
    {
        update.cancelUpdate=true;
        update.fileName = fileName;
    }



}
