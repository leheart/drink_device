package Const;


import com.example.administrator.drinkdevice.BuildConfig;

import org.json.JSONObject;

import DataAccess.OptionDataAccess;
import Device.cola.ColaReader;
import Device.colaMixing.mixing_control;
import Device.printer.printer_control;
import Device.scanner_mp86.mp86_control;
import Interface.IEnjoyCard;
import Interface.IWebService;

public class PublicDefine {
    //卡操作
    public static IEnjoyCard enjoyCard;
    //卡密钥
    public static byte[] commonAesKey = new byte[]{
            (byte) 0x34, (byte) 0x78, (byte) 0xEF, (byte) 0x00, (byte) 0xDC, (byte) 0x90,
            (byte) 0x69, (byte) 0x32, (byte) 0x53, (byte) 0x54, (byte) 0x43, (byte) 0x5F,
            (byte) 0x48, (byte) 0x4A, (byte) 0xB8, (byte) 0xBA};

    //网络链接状态
    public static boolean NetworkConnState=false;

    //日志服务器地址
    public static String UpdateHost= "http://up.yingjiayun.com";

    //饮料机
    public static ColaReader colaReader;

    //打印机
    public static printer_control printerControl;

    //扫描枪+读卡器
    public static mp86_control mp86Control;

    //系统设置数据库
    public static OptionDataAccess optionData;

    //http请求类
    public static IWebService webService;

    //影院服务器地址
    public static String serviceUrl = "";

    //影院信息
    public static JSONObject cinemaInfo = null;

}
