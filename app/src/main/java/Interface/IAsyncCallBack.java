package Interface;

import android.content.Intent;

/**
 * Created by 王彦鹏 on 2017-09-25.
 */

public interface IAsyncCallBack extends IInterface {
    /**
     * 执行业务
     * @return
     * @throws Exception
     */
    Object Execute() throws Exception;

    /**
     * 执行异常
     * @param e
     */
    void SetThrowErr(Throwable e);

    /**
     * 执行完成
     * @param result
     */
    void ExcuteComplete(Object result) throws Exception;
}
