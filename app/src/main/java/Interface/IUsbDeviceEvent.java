package Interface;

import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;

import java.util.Objects;

/**
 * Created by 王彦鹏 on 2017-11-18.
 */

public interface IUsbDeviceEvent extends IInterface {
    /**
     * usb接入事件
     * @param device  当前接入的设备
     */
    void UsbLineOnEvent(UsbDevice device, UsbDeviceConnection connection);

    /**
     * usb断开事件
     * @param device 当前断开的设备
     */
    void UsbLineOffEvent(UsbDevice device, UsbDeviceConnection connection);

}
