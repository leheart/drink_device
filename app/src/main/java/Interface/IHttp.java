package Interface;

import org.json.JSONObject;

import Entity.BaseEnity;

/**
 * Created by Administrator on 2017/9/28.
 */

public interface IHttp extends IInterface {
     String  Get(String url) throws Exception;
     String Post(String url, String postData) throws Exception;
     String Post(String url, String postData,String contentType) throws Exception;
     void Post(String url, String postData,IAsynCallBackListener callback) throws Exception;
     String Post(String url, String postData,IAsynCallBackListener callback,String contentType) throws Exception;
     JSONObject CallWebService(String url, String IName, BaseEnity... entity) throws Exception;
     String  Get(String url,IAsynCallBackListener callback) throws Exception;
     void CallWebService(String url, String IName,IAsynCallBackListener callback, BaseEnity... entity) throws Exception;
}
