package Interface;

import android.content.Context;
import android.content.Intent;

import java.io.IOException;

/**
 * Created by 王彦鹏 on 2017-09-06.
 */

public interface ICard extends IInterface {
    boolean ExistsCard();
    byte[] GetCardId();
    long GetCardNo();
    void  SetIntent(Context ctx, Intent intent) throws Exception;
    void SetBrushEvent(IBrushCardEvent event);
    byte[] ReadSector(int SectorNo, byte[] readkey) throws Exception;
    byte[] ReadBlock(int BlockNo, byte[] readkey) throws Exception;
    int WriteSector(int SectorNo, byte[] Data, byte[] writekey) throws  Exception;
    int WriteBlock(int BlockNo, byte[] Data, byte[] writekey) throws  Exception;
    boolean AuthentReadKey(int SectorNo,byte[] Key) throws Exception;
    boolean AuthentWruteKey(int SectorNo,byte[] Key) throws Exception;
    boolean connected();
    int GetSectorCount();

    int GetBlockCount();
    void  beep(int ms);
    Object CardSyncObj();

    /***
     * 获取读卡设备的状态
     * BYTE HardVer[8];//硬件版本
     * 	BYTE SoftVer[8];//软件版本
     * 	BYTE CardState;//卡状态：0=无卡，1=卡存在
     * 	BYTE CardType;//卡种类：01=s50卡，02=s70，03=ML
     * 	BYTE DevId[12];//设备ID
     * @return 30字节的结构
     */
    byte[] GetReaderState();
}
