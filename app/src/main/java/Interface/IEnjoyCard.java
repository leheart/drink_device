package Interface;

import android.content.Context;
import android.content.Intent;
import Annotation.Filter;
import Entity.BaseInfo;
import Struct.ReaderState;

/**
 * Created by 王彦鹏 on 2017-09-06.
 */

public interface IEnjoyCard {
    BaseInfo ReadBaseInfo() throws Exception;
    void setCard(ICard card);
    ICard getCard();
    void SetIntent(Context ctx, Intent intent)  throws Exception;
    void initCardPwd(byte[] bufId);
    long GetCardNo();
    boolean ExistsCard();
    void beep(int times);

    /***
     * 获取读卡设备的状态
     * @return
     */
    ReaderState GetReaderState() throws Exception;

    boolean connected();
}
