package Interface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;

public interface IWebService extends IInterface {

    //上传状态
    public void PostState(String cinemaCode,IAsynCallBackListener callback) throws Exception;

    //获取商品列凑
    public void getShopGoods(IAsynCallBackListener callBackListener) throws Exception;

    //获取设备信息
    public void getDeviceInfo(IAsynCallBackListener callBackListener) throws Exception;

    //获取前端吧台可售卖品
    public void getBarList(IAsynCallBackListener callBackListener) throws Exception;

    //商品价格计算
    public void saleGoodsCalculate(JSONArray goods,String memberId,int payType,IAsynCallBackListener callBackListener) throws Exception;

    //查询会员列表
    public void getMemberInfoList(String cardNo,IAsynCallBackListener callBackListener) throws Exception;

    //获取会员储值卡列表
    public void getMemberStoredCard(String memberId,IAsynCallBackListener callBackListener) throws Exception;

    //卖品下单
    public void createSaleOrder(String memberId,String tel,JSONArray goods, int payType, BigDecimal payCash,BigDecimal discount,BigDecimal ducprice,String deviceName,IAsynCallBackListener callBackListener) throws Exception;

    //支付
    public void payV2(BigDecimal totalPayAmount,int payType,String memberId,String phone,JSONArray orders,JSONArray storeCards,IAsynCallBackListener callBackListener) throws Exception;

    //取货
    public void take(String takeCode,String storageId,String deviceName,IAsynCallBackListener callBackListener) throws Exception;

    //检查升级
    public void UpdateVerify(String appName, Integer appVer,String produceVer,IAsynCallBackListener callback);

    //获取设备当日销售汇总
    public void getDeviceSaleSum(IAsynCallBackListener callBackListener) throws Exception;

    //获取仓库下货位列表
    public void getGoodsLocationList(int storageId,IAsynCallBackListener callBackListener) throws Exception;

    //检查库存
    public void checkGoodsStock(int storageId,int locationId,JSONArray goodsList,IAsynCallBackListener callBackListener) throws Exception;

    //刷卡支付
    public void microPay(String orderId,String payOrderId,String authCode,IAsynCallBackListener callBackListener) throws Exception;

    //查询支付订单状态
    public String searchPay(String payOrderId) throws Exception;

    //完成订单
    public void completeOrder(String orderId,String paymentOrderId,IAsynCallBackListener callBackListener) throws Exception;

    //根据订单id获取影票信息
    public void ticketMessage(String orderId,IAsynCallBackListener callBackListener) throws Exception;
}
