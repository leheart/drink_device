package Device.Card.UsbCard;

/**
 * Created by 王彦鹏 on 2017-11-18.
 */

public class ReaderCommand {
    private byte cmdCode;
    private byte [] inData;
    private int inDataLen;
    private byte [] outData;
    private int outDataLen;
    public ReaderCommand(){
        inData = new byte[64];
        outData = new byte[64];
    }

    public byte getCmdCode() {
        return cmdCode;
    }

    public void setCmdCode(byte cmdCode) {
        this.cmdCode = cmdCode;
    }

    public byte[] getInData() {
        return inData;
    }

    public void setInData(byte[] inData) {
        this.inData = inData;
    }

    public int getInDataLen() {
        return inDataLen;
    }

    public void setInDataLen(int inDataLen) {
        this.inDataLen = inDataLen;
    }

    public byte[] getOutData() {
        return outData;
    }

    public void setOutData(byte[] outData) {
        this.outData = outData;
    }

    public int getOutDataLen() {
        return outDataLen;
    }

    public void setOutDataLen(int outDataLen) {
        this.outDataLen = outDataLen;
    }
}
