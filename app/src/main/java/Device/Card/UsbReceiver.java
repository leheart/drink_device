package Device.Card;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;

import Device.Card.IUsbDevice;

/**
 * Usb事件处理
 * Created by 王彦鹏 on 2017-11-18.
 */

public class UsbReceiver extends BroadcastReceiver {
    protected static final String ACTION_USB_PERMISSION =  "com.android.example.USB_PERMISSION";
    protected UsbDevice usbDevice;
    protected Context mctx;
    protected Intent intent;
    protected UsbManager usbManager;
    protected PendingIntent  mPermissionIntent;
    protected UsbEndpoint usbEndpointOut;
    protected UsbEndpoint usbEndpointIn;
    protected UsbDeviceConnection usbDeviceConnection;
    /**
     * 设备是否连接
     */
    protected boolean isConnection=false;

    public void SetIntent(Context ctx,Intent intent) throws InterruptedException {
        mctx=ctx;
        this.intent=intent;
        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        mctx.registerReceiver(this, filter);
        mPermissionIntent = PendingIntent.getBroadcast(mctx, 0, new Intent(ACTION_USB_PERMISSION), 0);
        usbManager=(UsbManager)mctx.getSystemService(Context.USB_SERVICE);
        if (this instanceof IUsbDevice) {
            usbDevice = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
            ((IUsbDevice)this).OpenDevice(usbDevice);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent)
    {
        // TODO Auto-generated method stub
        String action = intent.getAction();
        switch (action) {
            case ACTION_USB_PERMISSION: {
                //权限许可
                synchronized (this) {
                    usbDevice = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    boolean usbPremission = intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false);
                    if ((usbPremission) && (usbDevice != null)) {
                        int countInterface = usbDevice.getInterfaceCount();
                        if (countInterface > 0) {
                            UsbInterface usb = usbDevice.getInterface(0);
                            usbDevice = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                            if (usb!=null &&  this instanceof IUsbDevice)  {
                                ((IUsbDevice)this).OpenDevice(usbDevice);
                            }
                        }
                    } else {
                    }
                    break;
                }
            }
            case UsbManager.ACTION_USB_DEVICE_ATTACHED: {
                //接入
                usbDevice = (UsbDevice) intent .getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (usbDevice != null) {
                    PendingIntent mPermissionIntent = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_USB_PERMISSION), 0);
                    usbManager.requestPermission(usbDevice, mPermissionIntent);
                } else {
                }
                break;
            }
            case UsbManager.ACTION_USB_DEVICE_DETACHED: {

                //断开
                usbDevice = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if  (this instanceof IUsbDevice) {
                    ((IUsbDevice)this).CloseUsb(usbDevice);
                }
                //isConnection=false;
                break;
            }
        }
    }


}
