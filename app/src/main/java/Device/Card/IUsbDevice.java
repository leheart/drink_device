package Device.Card;

import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbInterface;

/**
 * Created by 王彦鹏 on 2018-01-22.
 */

public interface IUsbDevice {
    /**
     * 打开设备
     */
    void OpenDevice(UsbDevice device);

    /**
     * 打开 Usb 的链接
     * @param Iusb
     * @return
     */
    UsbDeviceConnection OpenUsb(UsbInterface Iusb);

    /**
     * 关闭Udb链接
     * @param device
     */
    void CloseUsb(UsbDevice device);
}
