package Device.printer;

/**
 * 声明常量
 * @author wangxy
 * @date 2016-08-10
 */
public class printerConstant {
	public static String WebAddress = "www.baidu.com";

	
	// 小票测试打印文字中英文对照
	public static int ADD_NUM = 1000;
	public static String TITLE_CN = "中国农业银行\n\n" + "办理业务(一)\n\n";
	public static String STRDATA_CN = "您前面有 10 人等候，请注意叫号\n" + "欢迎光临！我们将竭诚为你服务。\n";
	//电影票打印内容
	public static String MOVIE_HALL_CN = "6号厅";
	public static String MOVIE_PLAY_DATE = "2020/03/09";
	public static String MOVIE_PLAY_TIME = "14:30";
	public static String MOVIE_NAME = "唐人街探案3";
	public static String MOVIE_CUSTOMER_SEAT = "5排10号";
	public static String MOVIE_SEE_FEE = "35元";
	public static String MOVIE_TICKET_TYPE = "国语3D";
	public static String MOVIE_TICKET_SELL_DATETIME = "2020/03/09 13:25";
	public static String MOVIE_SELLER_NO = "007";	//工号
	public static String MOVIE_INTRODUCE = "该片讲述了“曼谷夺金杀人案”“纽约五行连\n环杀人案”后，“唐人街神探组合”唐仁，\n秦风被野田昊请到东京，调查一桩离奇的谋杀\n案的故事\n";
	public static String MOVIE_TICKET_NO = "YJ1004009";	//票号






}
