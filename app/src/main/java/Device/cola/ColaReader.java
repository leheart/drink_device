package Device.cola;

/**
 * Created by 王彦鹏 on 2017-11-18.
 */

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.os.Handler;
import android.os.Message;
import android.os.Process;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import Device.Card.IUsbDevice;
import Device.Card.UsbCard.ReaderCommand;
import Device.Card.UsbCard.UsbHidDevice;
import Device.colaMixing.param_Constant;
import Device.colaMixing.slave_DeviceInfo;
import Helper.Log;
import Interface.IBrushCardEvent;
import Interface.ICard;
import Interface.IUsbDeviceEvent;
import Struct.BinaryType;
import everbright.uart.library.DataConversion;

public class ColaReader extends UsbHidDevice implements IUsbDevice {
    /**
     * 盈加宝盒的 Vid 与 Pid
     */
    private int vId = 0x0483;
    private int pId = 0x1290;

    private final byte UART_BUFFER_SIZE = (byte) 0x40;
    private final byte UART_CMD_HEADER_LENGTH = (byte) 0x07;
    private final byte UART_CMD_HEADER_TAIL_LENGTH = (byte) 0x09;
    private final byte CMD_HEADER1 = (byte) 0xA2;
    private final byte CMD_HEADER2 = (byte) 0x13;
    private final byte CMD_HEADER3 = (byte) 0xB4;
    private final byte CMD_TAIL = (byte) 0x00;
    //命令字
    private final byte CMD_NONE = (byte) 0x00;
    private final byte RS232_CMD_Read_DEVICE_Status_info = (byte) 0x01;
    private final byte RS232_CMD_All_Slave_Device_Info = (byte) 0x02;       //获取所有从机设备信息
    private final byte RS232_CMD_Get_Slave_Device_Status = (byte) 0x03;       //读取指定分机状态信息
    private final byte RS232_CMD_Get_Slave_Device_Sum = (byte) 0x04;       //读取指定分机码表信息
    private final byte RS232_CMD_Clear_Slave_Device_Sum = (byte) 0x05;
    private final byte RS232_CMD_Start_Get_Water = (byte) 0x06;       //启动接杯指令信息
    private final byte RS232_CMD_Abort_Get_Water = (byte) 0x07;
    private final byte RS232_CMD_ENTER_SETTING = (byte) 0x08;       //进入接杯参数设置指令
    private final byte RS232_CMD_EXIT_SETTING = (byte) 0x09;       //退出设置指令
    private final byte RS232_CMD_DELETE_SLAVE_DEVICE = (byte) 0x0A;       //删除指定从机设备指令
    private final byte RS232_CMD_POSITION_DEVICE = (byte) 0x0B;       //定位
    //设备码
    private final byte MIXING_DEVICE_CODE = (byte) 0x10;    //device code ,select which device to communicate with

    public String deviceStatus_Version;
    public String deviceStatus_DevID  = "";
    public int current_CupType;         //当前接杯类型
    public int deviceStatus_SlaveDeviceNumber =   0;
    public int deviceStatus_SettingState = 0;

    protected Object syncobj = new Object();

    //从设备信息
    public slave_DeviceInfo slave_DeviceInfo = new slave_DeviceInfo();

    @Override
    public void OpenDevice(UsbDevice device) {
        if (device != null && (device.getProductId() != pId || device.getVendorId() != vId)) {
            return;
        }
        if (isConnection) {
            if (mctx instanceof IUsbDeviceEvent) {
                ((IUsbDeviceEvent) mctx).UsbLineOnEvent(usbDevice, usbDeviceConnection);
            }
            return;
        }
        HashMap<String, UsbDevice> deviceList = usbManager.getDeviceList();
        Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();
        while (deviceIterator.hasNext()) {
            usbDevice = deviceIterator.next();
            if ((usbDevice.getVendorId() == vId) && (usbDevice.getProductId() == pId)) {
                if (!usbManager.hasPermission(usbDevice)) {
                    usbManager.requestPermission(usbDevice, mPermissionIntent);
                } else {
                    UsbInterface usb = usbDevice.getInterface(0);
                    usbDeviceConnection = OpenUsb(usb);
                    if (usbDeviceConnection != null) {
                        isConnection = true;
                        if (mctx instanceof IUsbDeviceEvent) {
                            ((IUsbDeviceEvent) mctx).UsbLineOnEvent(usbDevice, usbDeviceConnection);
                        }
                    }
                }
                return;
            }
        }
        return;
    }

    private boolean Check_Response_is_OK(byte[] received_buffer) {
        synchronized (syncobj) {
            byte check;
            byte i, len, len1, len2;

            if ((received_buffer[0] == CMD_HEADER1) && (received_buffer[1] == CMD_HEADER2) && (received_buffer[2] == CMD_HEADER3))      //检查命令头
            {
                len1 = received_buffer[3];    //包长度
                len2 = received_buffer[5];    //包长度反码
                len2 = (byte) (0xff - len2);
                if ((len1 == len2))    //检查长度及其反码
                {
                    //指令长度超过缓冲区不处理
                    if (len1 >= (UART_BUFFER_SIZE - UART_CMD_HEADER_TAIL_LENGTH)) {
                        return false;
                    }
                    if (received_buffer[len1 + UART_CMD_HEADER_TAIL_LENGTH - 1] != CMD_TAIL)    //检查命令尾
                    {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }

            len = received_buffer[3];    //包长度
            check = 0;
            for (i = 0; i < len; i++) {
                check += received_buffer[i + UART_CMD_HEADER_LENGTH];
            }
            if (check == received_buffer[len + UART_CMD_HEADER_LENGTH])
                return true;
            else
                return false;
        }
    }

    public int getMixingDeviceStatus() throws IOException {
        synchronized (syncobj) {
            ReaderCommand rc = new ReaderCommand();
            rc.setCmdCode(RS232_CMD_Read_DEVICE_Status_info);
            rc.setInDataLen(0);
            int iRet = SendCommand(rc);
            if (iRet != 0) {
                return iRet;
            }
            if (rc.getOutData() == null) {
                return param_Constant.CMD_ERR_RESPONSE_TIMEOUT;
            }

            byte[] temp_buffer = new byte[16];
            int result = rc.getOutData()[1];
            temp_buffer[0] = rc.getOutData()[2];
            temp_buffer[1] = rc.getOutData()[3];
            temp_buffer[2] = rc.getOutData()[4];
            temp_buffer[3] = rc.getOutData()[5];
            temp_buffer[4] = 0;
            temp_buffer[5] = 0;
            temp_buffer[6] = 0;
            //版本
            deviceStatus_Version = new String(temp_buffer);
            //设备ID,一个字节一个字节转换
            StringBuffer hexStringBuffer = new StringBuffer();
            for (int i = 0; i < 4; i++) {
                hexStringBuffer.append(DataConversion.byteToHex(rc.getOutData()[9 - i]));
            }
            deviceStatus_DevID = hexStringBuffer.toString();
            //挂在基站的从设备个数
            deviceStatus_SlaveDeviceNumber = rc.getOutData()[10];
            deviceStatus_SettingState = rc.getOutData()[11];
            return result;
        }
    }


    //读所有从机设备状态
    public int getAllSlaveDeviceStatus() {
        synchronized (syncobj) {
            ReaderCommand rc = new ReaderCommand();
            rc.setCmdCode(RS232_CMD_All_Slave_Device_Info);
            rc.setInDataLen(0);
            int iRet = SendCommand(rc);
            if (iRet != 0) {
                return iRet;
            }
            if (rc.getOutData() == null) {
                return param_Constant.CMD_ERR_RESPONSE_TIMEOUT;
            }
            int result = rc.getOutData()[1];
            if (result == 0) {
                slave_DeviceInfo.total_device_number = (int) rc.getOutData()[2];
                int j = 0;
                for (int i = 0; i < slave_DeviceInfo.total_device_number; i++) {
                    StringBuffer hexStringBuffer = new StringBuffer();
                    hexStringBuffer.append(DataConversion.byteToHex(rc.getOutData()[6 + j]));
                    hexStringBuffer.append(DataConversion.byteToHex(rc.getOutData()[5 + j]));
                    hexStringBuffer.append(DataConversion.byteToHex(rc.getOutData()[4 + j]));
                    hexStringBuffer.append(DataConversion.byteToHex(rc.getOutData()[3 + j]));
                    slave_DeviceInfo.dev_id[i] = hexStringBuffer.toString();
                    slave_DeviceInfo.Config_Flag[i] = rc.getOutData()[7 + j];
                    j = j + 5;
                }
            }
            return result;
        }
    }

    //读指定从机设备状态
    public int getGivenSlaveDeviceStatus(String DevID)
    {
        synchronized (syncobj) {
            if (DevID.isEmpty()) return -1;
            ReaderCommand rc = new ReaderCommand();
            rc.setCmdCode(RS232_CMD_Get_Slave_Device_Status);
            byte[] send_buffer = new byte[UART_BUFFER_SIZE];
            byte[] id_buffer = DataConversion.decodeHexString(DevID);
            send_buffer[0] = id_buffer[3];//反序
            send_buffer[1] = id_buffer[2];
            send_buffer[2] = id_buffer[1];
            send_buffer[3] = id_buffer[0];
            rc.setInDataLen(4);
            rc.setInData(send_buffer);

            int iRet = SendCommand(rc);
            if (iRet != 0) {
                return iRet;
            }
            if (rc.getOutData() == null) {
                return param_Constant.CMD_ERR_RESPONSE_TIMEOUT;
            }

            int result = rc.getOutData()[1];
            if (result == 0) {
                //设置版本
                int iTemp = rc.getOutData()[3];
                iTemp = (iTemp << 8) | rc.getOutData()[2];
                slave_DeviceInfo.setStatus_Version(DataConversion.intToHex(iTemp));
                slave_DeviceInfo.setStatus_State(rc.getOutData()[4]);       //接杯状态
                slave_DeviceInfo.setContained_time(rc.getOutData()[5]);     //已经接杯时长
                slave_DeviceInfo.setContained_time_100ms(rc.getOutData()[6]);   //已接杯时长的小数部分
                slave_DeviceInfo.setLittle_USED_TIME(rc.getOutData()[7]);   //接满小杯所用时长
                slave_DeviceInfo.setMiddle_USED_TIME(rc.getOutData()[8]);   //接满中杯所用时长
                slave_DeviceInfo.setBig_USED_TIME(rc.getOutData()[9]);      //接满大杯所用时长
                slave_DeviceInfo.setMAX_Big_USED_TIME(rc.getOutData()[10]);     //接满超大杯所用时长
                slave_DeviceInfo.setSet_Cup_State(rc.getOutData()[11]);     //设置接杯状态
            }
            return result;
        }
    }
    //读指定从机设备的码表信息
    public int getGivenSlaveDevice_SumInfo(String DevID)
    {
        synchronized (syncobj) {
            if (DevID.isEmpty()) return -1;
            ReaderCommand rc = new ReaderCommand();
            rc.setCmdCode(RS232_CMD_Get_Slave_Device_Sum);
            byte[] send_buffer = new byte[UART_BUFFER_SIZE];
            byte[] id_buffer = DataConversion.decodeHexString(DevID);
            send_buffer[0] = id_buffer[3];//反序
            send_buffer[1] = id_buffer[2];
            send_buffer[2] = id_buffer[1];
            send_buffer[3] = id_buffer[0];
            rc.setInDataLen(4);
            rc.setInData(send_buffer);

            int iRet = SendCommand(rc);
            if (iRet != 0) {
                return iRet;
            }
            if (rc.getOutData() == null) {
                return param_Constant.CMD_ERR_RESPONSE_TIMEOUT;
            }

            int result = rc.getOutData()[1];
            if (result == 0) {
                //接杯总值
                int offset = 5;
                int lTemp = rc.getOutData()[offset];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 1];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 2];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 3];
                slave_DeviceInfo.setTotal_Cup_Sum(lTemp);
                //接小杯总值,占3字节
                offset = offset + 3;
                lTemp = rc.getOutData()[offset];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 1];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 2];
                slave_DeviceInfo.setLittle_Cup_Sum(lTemp);
                //接中杯总值,占3字节
                offset = offset + 3;
                lTemp = rc.getOutData()[offset];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 1];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 2];
                slave_DeviceInfo.setMiddle_Cup_Sum(lTemp);
                //接大杯总值,占4字节
                offset = offset + 4;
                lTemp = rc.getOutData()[offset];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 1];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 2];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 3];
                slave_DeviceInfo.setBig_Cup_Sum(lTemp);
                //接超大杯总值,占3字节
                offset = offset + 3;
                lTemp = rc.getOutData()[offset];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 1];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 2];
                slave_DeviceInfo.setMax_Big_Cup_Sum(lTemp);
                //终止接杯总值,占3字节
                offset = offset + 3;
                lTemp = rc.getOutData()[offset];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 1];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 2];
                slave_DeviceInfo.setAbort_Cup_Sum(lTemp);
            }
            return result;
        }
    }
    //清除指定从机设备的码表信息
    public int clearGivenSlaveDevice_SumInfo(String DevID)
    {
        synchronized (syncobj) {
            if (DevID.isEmpty()) return -1;
            ReaderCommand rc = new ReaderCommand();
            rc.setCmdCode(RS232_CMD_Clear_Slave_Device_Sum);
            byte[] send_buffer = new byte[UART_BUFFER_SIZE];
            byte[] id_buffer = DataConversion.decodeHexString(DevID);
            send_buffer[0] = id_buffer[3];//反序
            send_buffer[1] = id_buffer[2];
            send_buffer[2] = id_buffer[1];
            send_buffer[3] = id_buffer[0];
            rc.setInDataLen(4);
            rc.setInData(send_buffer);

            int iRet = SendCommand(rc);
            if (iRet != 0) {
                return iRet;
            }
            if (rc.getOutData() == null) {
                return param_Constant.CMD_ERR_RESPONSE_TIMEOUT;
            }

            int result = rc.getOutData()[1];
            if (result == 0) {
                //接杯总值
                int offset = 5;
                int lTemp = rc.getOutData()[offset];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 1];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 2];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 3];
                slave_DeviceInfo.setTotal_Cup_Sum(lTemp);
                //接小杯总值,占3字节
                offset = offset + 3;
                lTemp = rc.getOutData()[offset];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 1];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 2];
                slave_DeviceInfo.setLittle_Cup_Sum(lTemp);
                //接中杯总值,占3字节
                offset = offset + 3;
                lTemp = rc.getOutData()[offset];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 1];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 2];
                slave_DeviceInfo.setMiddle_Cup_Sum(lTemp);
                //接大杯总值,占4字节
                offset = offset + 4;
                lTemp = rc.getOutData()[offset];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 1];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 2];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 3];
                slave_DeviceInfo.setBig_Cup_Sum(lTemp);
                //接超大杯总值,占3字节
                offset = offset + 3;
                lTemp = rc.getOutData()[offset];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 1];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 2];
                slave_DeviceInfo.setMax_Big_Cup_Sum(lTemp);
                //终止接杯总值,占3字节
                offset = offset + 3;
                lTemp = rc.getOutData()[offset];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 1];
                lTemp = (lTemp << 8) | rc.getOutData()[offset - 2];
                slave_DeviceInfo.setAbort_Cup_Sum(lTemp);
            }
            return result;
        }
    }
    //启动指定从机设备接杯
    public int startGivenSlaveDevice_GetWater(String DevID,int cup_type,int number)
    {
        synchronized (syncobj) {
            if (DevID.isEmpty()) return -1;
            ReaderCommand rc = new ReaderCommand();
            rc.setCmdCode(RS232_CMD_Start_Get_Water);
            byte[] send_buffer = new byte[UART_BUFFER_SIZE];
            byte[] id_buffer = DataConversion.decodeHexString(DevID);
            send_buffer[0] = id_buffer[3];//反序
            send_buffer[1] = id_buffer[2];
            send_buffer[2] = id_buffer[1];
            send_buffer[3] = id_buffer[0];
            send_buffer[4] = (byte) cup_type; //接杯类型:1小杯，2中杯,3大杯,4特大杯
            send_buffer[5] = (byte) number;   //接杯数量
            rc.setInDataLen(6);
            rc.setInData(send_buffer);

            int iRet = SendCommand(rc);
            if (iRet != 0) {
                return iRet;
            }
            if (rc.getOutData() == null) {
                return param_Constant.CMD_ERR_RESPONSE_TIMEOUT;
            }

            int result = (int) rc.getOutData()[1];
            if (result == 0) {
                current_CupType = cup_type;
            }
            return result;
        }
    }
    //终止指定从机设备接杯
    public int abortGivenSlaveDevice_GetWater(String DevID)
    {
        synchronized (syncobj) {
            if (DevID.isEmpty()) return -1;
            ReaderCommand rc = new ReaderCommand();
            rc.setCmdCode(RS232_CMD_Abort_Get_Water);
            byte[] send_buffer = new byte[UART_BUFFER_SIZE];
            byte[] id_buffer = DataConversion.decodeHexString(DevID);
            send_buffer[0] = id_buffer[3];//反序
            send_buffer[1] = id_buffer[2];
            send_buffer[2] = id_buffer[1];
            send_buffer[3] = id_buffer[0];
            rc.setInDataLen(4);
            rc.setInData(send_buffer);

            int iRet = SendCommand(rc);
            if (iRet != 0) {
                return iRet;
            }
            if (rc.getOutData() == null) {
                return param_Constant.CMD_ERR_RESPONSE_TIMEOUT;
            }

            int result = (int) rc.getOutData()[1];
            current_CupType = param_Constant.GET_CUP_TYPE_NONE;
            return result;
        }
    }
    public int enter_getCup_setting(String DevID,int cup_type)
    {
        return enter_getCup_setting(DevID,cup_type,0);
    }
    //进入接杯参数设置,second为0时，手动设置
    public int enter_getCup_setting(String DevID,int cup_type,int second)
    {
        synchronized (syncobj) {
            if (DevID.isEmpty()) return -1;
            ReaderCommand rc = new ReaderCommand();
            rc.setCmdCode(RS232_CMD_ENTER_SETTING);
            byte[] send_buffer = new byte[UART_BUFFER_SIZE];
            byte[] id_buffer = DataConversion.decodeHexString(DevID);
            send_buffer[0] = id_buffer[3];//反序
            send_buffer[1] = id_buffer[2];
            send_buffer[2] = id_buffer[1];
            send_buffer[3] = id_buffer[0];
            send_buffer[4] = (byte) cup_type;
            send_buffer[5] = (byte) second;
            rc.setInDataLen(6);
            rc.setInData(send_buffer);

            int iRet = SendCommand(rc);
            if (iRet != 0) {
                return iRet;
            }
            if (rc.getOutData() == null) {
                return param_Constant.CMD_ERR_RESPONSE_TIMEOUT;
            }

            int result = (int) rc.getOutData()[1];
            return result;
        }
    }
    //退出接杯参数设置
    public int exit_getCup_setting(String DevID)
    {
        synchronized (syncobj) {
            if (DevID.isEmpty()) return -1;
            ReaderCommand rc = new ReaderCommand();
            rc.setCmdCode(RS232_CMD_EXIT_SETTING);
            byte[] send_buffer = new byte[UART_BUFFER_SIZE];
            byte[] id_buffer = DataConversion.decodeHexString(DevID);
            send_buffer[0] = id_buffer[3];//反序
            send_buffer[1] = id_buffer[2];
            send_buffer[2] = id_buffer[1];
            send_buffer[3] = id_buffer[0];
            rc.setInDataLen(4);
            rc.setInData(send_buffer);

            int iRet = SendCommand(rc);
            if (iRet != 0) {
                return iRet;
            }
            if (rc.getOutData() == null) {
                return param_Constant.CMD_ERR_RESPONSE_TIMEOUT;
            }

            int result = (int) rc.getOutData()[1];
            if (result == 0) {
                slave_DeviceInfo.setStatus_State(rc.getOutData()[4]);       //接杯状态
                slave_DeviceInfo.setContained_time(rc.getOutData()[5]);     //已经接杯时长
                slave_DeviceInfo.setContained_time_100ms(rc.getOutData()[6]);   //已接杯时长的小数部分
                slave_DeviceInfo.setLittle_USED_TIME(rc.getOutData()[7]);   //接满小杯所用时长
                slave_DeviceInfo.setMiddle_USED_TIME(rc.getOutData()[8]);   //接满中杯所用时长
                slave_DeviceInfo.setBig_USED_TIME(rc.getOutData()[9]);      //接满大杯所用时长
                slave_DeviceInfo.setMAX_Big_USED_TIME(rc.getOutData()[10]);     //接满超大杯所用时长
            }
            return result;
        }
    }
    //删除指定从机设备
    public int delete_this_SlaveDevice(String DevID)
    {
        synchronized (syncobj) {
            if (DevID.isEmpty()) return -1;
            ReaderCommand rc = new ReaderCommand();
            rc.setCmdCode(RS232_CMD_DELETE_SLAVE_DEVICE);
            byte[] send_buffer = new byte[UART_BUFFER_SIZE];
            byte[] id_buffer = DataConversion.decodeHexString(DevID);
            send_buffer[0] = id_buffer[3];//反序
            send_buffer[1] = id_buffer[2];
            send_buffer[2] = id_buffer[1];
            send_buffer[3] = id_buffer[0];
            rc.setInDataLen(4);
            rc.setInData(send_buffer);

            int iRet = SendCommand(rc);
            if (iRet != 0) {
                return iRet;
            }
            if (rc.getOutData() == null) {
                return param_Constant.CMD_ERR_RESPONSE_TIMEOUT;
            }

            int result = (int) rc.getOutData()[1];
            if (result == 0) {
                slave_DeviceInfo.total_device_number = (int) rc.getOutData()[2];
                int j = 0;
                for (int i = 0; i < slave_DeviceInfo.total_device_number; i++) {
                    StringBuffer hexStringBuffer = new StringBuffer();
                    hexStringBuffer.append(DataConversion.byteToHex(rc.getOutData()[6 + j]));
                    hexStringBuffer.append(DataConversion.byteToHex(rc.getOutData()[5 + j]));
                    hexStringBuffer.append(DataConversion.byteToHex(rc.getOutData()[4 + j]));
                    hexStringBuffer.append(DataConversion.byteToHex(rc.getOutData()[3 + j]));
                    slave_DeviceInfo.dev_id[i] = hexStringBuffer.toString();
                    slave_DeviceInfo.Config_Flag[i] = rc.getOutData()[7 + j];
                    j = j + 5;
                }
            }
            return result;
        }
    }

    //定位
    public int position_Device(String DevID)
    {
        synchronized (syncobj) {
            if (DevID.isEmpty()) return -1;
            ReaderCommand rc = new ReaderCommand();
            rc.setCmdCode(RS232_CMD_POSITION_DEVICE);
            byte[] send_buffer = new byte[UART_BUFFER_SIZE];
            byte[] id_buffer = DataConversion.decodeHexString(DevID);
            send_buffer[0] = id_buffer[3];//反序
            send_buffer[1] = id_buffer[2];
            send_buffer[2] = id_buffer[1];
            send_buffer[3] = id_buffer[0];
            rc.setInDataLen(4);
            rc.setInData(send_buffer);

            int iRet = SendCommand(rc);
            if (iRet != 0) {
                return iRet;
            }
            if (rc.getOutData() == null) {
                return param_Constant.CMD_ERR_RESPONSE_TIMEOUT;
            }
            return 0;
        }
    }
}
