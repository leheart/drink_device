package Device.scanner_mp86;

/**
 * 声明常量
 * @author Hanyl
 * @date 2020-07-17
 */
public class mp86_Constant {

    public static byte REQUEST_CMD_QR_DM_BARCODE_NFC_SETTING	        =   0x21;
    public static byte REQUEST_CMD_SCAN_WORK_MODE           	        =   0x22;
                    public static byte SCAN_WORK_MODE_NORMAL        =   1;  //输出所有扫描结果
                    public static byte SCAN_WORK_MODE_SINGLE        =   2;  //相同的码只输出一次
                    public static byte SCAN_WORK_MODE_INTERVAL      =   3;  //相同的码在一定时间间隔内只输出一次
    public static byte REQUEST_CMD_SCAN_SUCCESS_LED_STATUS     	        =   0x24;
                    public static byte SCAN_SUCCESS_LED_OFF         =  (byte) 0x00;
                    public static byte SCAN_SUCCESS_LED_WHITE       =  (byte)0x01;
                    public static byte SCAN_SUCCESS_LED_RED         =  (byte)0x02;
                    public static byte SCAN_SUCCESS_LED_GREEN       =  (byte) 0x04;
    public static byte REQUEST_CMD_SCAN_SUCCESS_BEEP_STATUS   	        =   0x25;
                    public static byte SCAN_SUCCESS_BEEP_OFF      =   (byte)0x00;
                    public static byte SCAN_SUCCESS_BEEP_ON       =   (byte)0x01;
    public static byte REQUEST_CMD_CHECK_DEVICE_STATUS   	            =   (byte)0x01;
    public static byte REQUEST_CMD_LED_AND_BEEP_CONTROL  	            =   (byte)0x04;
                public static byte LED_AND_BEEP_CONTROL_ALL_OFF  =   (byte)0x00;
                public static byte LED_AND_BEEP_CONTROL_RED_LED  =   (byte)0x02;
                public static byte LED_AND_BEEP_CONTROL_GREEN_LED  = (byte)0x04;
                public static byte LED_AND_BEEP_CONTROL_BEEP      =  (byte)0x08;
    public static byte REQUEST_CMD_SCANNER_ON_OFF  	            =   (byte)0x05;
                public static byte SCANNER_ON  =   (byte)0x00;
                public static byte SCANNER_OFF =   (byte)0x01;
    public static byte REQUEST_CMD_SCANNER_Get_Result              =   (byte)0x30;
    public static byte REQUEST_CMD_SCANNER_RESULT_NOTIFY_MODE      =   (byte)0x31;
                public static byte SCANNER_RESULT_NOTIFY    =   (byte)0x01;     //主动上报
                public static byte SCANNER_RESULT_CMD_LOOP  =   (byte)0x00;     //命令查询
    public static byte REQUEST_CMD_CONTROL_COMMAND_MODE           =   (byte)0x53;
    public static byte REQUEST_CMD_READ_S50_CARD_BLOCK            =   (byte)0x51;
    public static byte READ_S50_BLOCK_KEY_A          =   (byte)0x60;
    public static byte READ_S50_BLOCK_KEY_B          =   (byte)0x61;


    public static byte SCANNER_RESPONSE_OK              =   (byte)0x00;
    public static byte SCANNER_RESPONSE_RESULT_NONE     =   (byte)0x01;
    public static byte SCANNER_RESPONSE_RESULT_QR       =   (byte)0x02;
    public static byte SCANNER_RESPONSE_RESULT_CARD     =   (byte)0x03;
    public static byte SCANNER_RESPONSE_TIMEOUT         =   (byte)0x80;
    public static byte SCANNER_RESPONSE_VERIFY_ERROR    =   (byte)0x81;

}
