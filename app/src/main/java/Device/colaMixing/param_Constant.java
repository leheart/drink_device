package Device.colaMixing;

 // 声明可乐控制参数常量
 // Created by everbright6666 on 2020/2/26.

public class param_Constant {

	//命令响应结果码
	public static int CMD_OK	        =   0x00;
	public static int CMD_FAIL	        =   0x01;
	public static int CMD_GET_WATER_PARAM_ERROR		=	0x0D;		//接杯参数未设置
	public static int CMD_UNSUPPORT_ERROR         =   0x02;
	public static int CMD_ERR_NO_THIS_DEVICE      =   0x0C;			//无此设备
	public static int CMD_ERR_RESPONSE_TIMEOUT    =   0x18;
	public static int CMD_WIRELESS_RECEIVE_ERROR  =   0x20;			//从分机接收失败
	public static int CMD_WIRELESS_SEND_ERROR  	  =   0x21;			//向分机发送失败
	public static int CMD_WIRELESS_DIFFERENT_ERROR =   0x22;			//分机命令不一致失败
	public static int CMD_VERIFY_SUM_ERROR	       =   0x23;		//检验和错误

	//接杯状态
	public static int GET_WATER_STATE_IDLE		=	0;			//空闲
	public static int GET_WATER_STATE_PAUSE		=	1;			//暂停
	public static int GET_WATER_STATE_Continue	=	2;			//继续
	public static int GET_WATER_STATE_Outting 	=	3;			//正在接杯
	public static int GET_WATER_STATE_Finished 	=	4;			//完成接杯
	//接杯类型
	public static int GET_CUP_TYPE_NONE			=	0;			//接杯类型:无
	public static int GET_CUP_TYPE_LITTLE		=	1;			//接杯类型：小杯
	public static int GET_CUP_TYPE_MIDDLE		=	2;			//接杯类型：中杯
	public static int GET_CUP_TYPE_BIG			=	3;			//接杯类型：大杯
	public static int GET_CUP_TYPE_SUPER_BIG 	=	4;			//接杯类型：特大杯

	public static byte CONFIG_CONFIG_BIT		=	0x01;			//配置位
	public static byte CONFIG_ONLINE_BIT		=	0x02;			//配置位

}
