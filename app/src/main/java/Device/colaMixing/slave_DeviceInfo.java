package Device.colaMixing;

import java.io.Serializable;

public class slave_DeviceInfo implements Serializable{
    public String[] dev_id      = new String[16];
    public byte[] Config_Flag = new byte[16];           // 配置标志

    public int total_device_number;
    private long current_id;
    //码表信息
    private long Total_Sum;                 //总接杯累计
    private long Little_Sum;                //小杯接杯累计次数
    private long Middle_Sum;                //中杯接杯累计次数
    private long Big_Sum;                   //大杯接杯累计次数
    private long Max_Big_Sum;               //特大杯接杯次数
    private long Abort_Sum;                 //终止累计次数
    //当前状态
    private String status_version;          //版本
    private int status_Cup_Type;            //杯类型
    private int status_State;               //接杯状态:0空闲,0x01暂停;0x03正在接杯；0x04接杯完成
    private int status_Contained_time;		//已接杯时长
    private int status_Contained_time_100ms;   //已经接杯时间小数部分
    //各种杯接杯时长
    private int  Cup_Little_USED_TIME;		//接满小杯所用时长
    private int  Cup_Middle_USED_TIME;		//接满中杯所用时长
    private int  Cup_Big_USED_TIME;			//接满大杯所用时长
    private int  Cup_MAX_Big_USED_TIME;		//接满最大杯所用时长
    private int  Set_Cup_State;		        //设置接杯状态 0空闲; 1正在设置; 2等待设置;

    public slave_DeviceInfo() {
        super();
    }
    public slave_DeviceInfo(long id, int status_Cup_Type, int status_State, int status_Contained_time) {
        super();
        this.current_id  = id;
        this.status_Cup_Type = status_Cup_Type;
        this.status_State    = status_State;
        this.status_Contained_time = status_Contained_time;
    }

    public int getSet_Cup_State() {
        return Set_Cup_State;
    }

    public void setSet_Cup_State(int set_Cup_State) {
        Set_Cup_State = set_Cup_State;
    }

    public long getId() {
        return current_id;
    }
    public void setId(long id) {
        this.current_id = id;
    }
    public byte getConfig_Flag(int index) {
        return Config_Flag[index];
    }
    public void setConfig_Flag(int index,byte flag) {
        this.Config_Flag[index] = flag;
    }
    public String getStatus_Version() {
        return status_version;
    }
    public void setStatus_Version(String version) {
        this.status_version = version;
    }
    public int getStatus_Cup_Type() {
        return status_Cup_Type;
    }
    public void setStatus_Cup_Type(int Cup_Type) {
        this.status_Cup_Type = Cup_Type;
    }
    public int getStatus_State() {
        return status_State;     }
    public void setStatus_State(int status_State) {
        this.status_State = status_State;
    }
    public int getContained_time() {
        return status_Contained_time;
    }
    public void setContained_time(int status_Contained_time) {
        this.status_Contained_time = status_Contained_time;
    }
    public int getContained_time_100ms() {
        return status_Contained_time_100ms;
    }
    public void setContained_time_100ms(int status_time_100ms) {
        this.status_Contained_time_100ms = status_time_100ms;
    }
    public int getLittle_USED_TIME() {
        return Cup_Little_USED_TIME;
    }
    public void setLittle_USED_TIME(int time) {
        this.Cup_Little_USED_TIME = time;
    }
    public int getMiddle_USED_TIME() {
        return Cup_Middle_USED_TIME;
    }
    public void setMiddle_USED_TIME(int time) {
        this.Cup_Middle_USED_TIME = time;
    }
    public int getBig_USED_TIME() {
        return Cup_Big_USED_TIME;
    }
    public void setBig_USED_TIME(int time) {
        this.Cup_Big_USED_TIME = time;
    }
    public int getMAX_Big_USED_TIME() {
        return Cup_MAX_Big_USED_TIME;
    }
    public void setMAX_Big_USED_TIME(int time) {
        this.Cup_MAX_Big_USED_TIME = time;
    }

    public long getTotal_Cup_Sum() {
        return Total_Sum;
    }
    public void setTotal_Cup_Sum(long Total_Sum) {
        this.Total_Sum = Total_Sum;     }
    public long getLittle_Cup_Sum() {
        return Little_Sum;
    }
    public void setLittle_Cup_Sum(long Little_Sum) {
        this.Little_Sum = Little_Sum;
    }
    public long getMiddle_Cup_Sum() {
        return Middle_Sum;
    }
    public void setMiddle_Cup_Sum(long Middle_Sum) {
        this.Middle_Sum = Middle_Sum;
    }
    public long getBig_Cup_Sum() {
        return Big_Sum;
    }
    public void setBig_Cup_Sum(long Big_Sum) {
        this.Big_Sum = Big_Sum;
    }
    public long getMax_Big_Cup_Sum() {
        return Max_Big_Sum;
    }
    public void setMax_Big_Cup_Sum(long Max_Big_Sum) {
        this.Max_Big_Sum = Max_Big_Sum;
    }
    public long getAbort_Cup_Sum() {
        return Abort_Sum;
    }
    public void setAbort_Cup_Sum(long Abort_Sum) {
        this.Abort_Sum = Abort_Sum;
    }

}
