package Device.colaMixing;

import Device.Card.UsbCard.UsbHidDevice;
import everbright.uart.library.DataConversion;
import everbright.uart.library.SerialPortConfig;
import everbright.uart.library.SerialPortHelper;

/**
 * Created by everbright6666 on 2020/2/24.
 */

public class mixing_control {

    private final byte UART_BUFFER_SIZE         =  (byte)0x40;
    private final byte UART_CMD_HEADER_LENGTH   =   (byte)0x07;
    private final byte UART_CMD_HEADER_TAIL_LENGTH  =   (byte)0x09;
    private final byte CMD_HEADER1 =(byte)0xA2;
    private final byte CMD_HEADER2 =(byte)0x13;
    private final byte CMD_HEADER3 =(byte)0xB4;
    private final byte CMD_TAIL    =(byte)0x00;
    //命令字
    private final byte CMD_NONE                          =(byte)0x00;
    private final byte RS232_CMD_Read_DEVICE_Status_info =(byte)0x01;
    private final byte RS232_CMD_All_Slave_Device_Info   =(byte)0x02;       //获取所有从机设备信息
    private final byte RS232_CMD_Get_Slave_Device_Status =(byte)0x03;       //读取指定分机状态信息
    private final byte RS232_CMD_Get_Slave_Device_Sum    =(byte)0x04;       //读取指定分机码表信息
    private final byte RS232_CMD_Clear_Slave_Device_Sum  =(byte)0x05;
    private final byte RS232_CMD_Start_Get_Water         =(byte)0x06;       //启动接杯指令信息
    private final byte RS232_CMD_Abort_Get_Water         =(byte)0x07;
    private final byte RS232_CMD_ENTER_SETTING           =(byte)0x08;       //进入接杯参数设置指令
    private final byte RS232_CMD_EXIT_SETTING            =(byte)0x09;       //退出设置指令
    private final byte RS232_CMD_DELETE_SLAVE_DEVICE     =(byte)0x0A;       //删除指定从机设备指令
    private final byte RS232_CMD_POSITION_DEVICE     =(byte)0x0B;       //定位
    //设备码
    private final byte MIXING_DEVICE_CODE           =(byte) 0x10;    //device code ,select which device to communicate with


    private SerialPortHelper serialPortHelper;

    private static final String TAG = "MixingSerialPort";

    public boolean isOpen;
    //读设备状态时返回
    public String deviceStatus_Version;
    public String deviceStatus_DevID  = "";
    public int current_CupType;         //当前接杯类型
    public int deviceStatus_SlaveDeviceNumber =   0;
    //从设备信息
    public slave_DeviceInfo slave_DeviceInfo = new slave_DeviceInfo();
    /*******************************************************************************
     * Function Name  : openSerialPort
     * Description    : 打开相应串口
     * Input          : None
     * Return         : <0时失败，>=0时成功
     *******************************************************************************/
    public int openSerialPort(){

        // 串口参数
        SerialPortConfig serialPortConfig = new SerialPortConfig();
        serialPortConfig.mode = 0;
        serialPortConfig.path = "/dev/ttyS4";
        serialPortConfig.baudRate = 115200;
        serialPortConfig.dataBits = 8;
        serialPortConfig.parity   = 'N';
        serialPortConfig.stopBits = 1;

        if(isOpen) return 0;
        // 初始化串口
        serialPortHelper = new SerialPortHelper(256);
        // 设置串口参数
        serialPortHelper.setConfigInfo(serialPortConfig);
        // 开启串口
        isOpen = serialPortHelper.openDevice();
        if(!isOpen){
            return -1;
        }
        return 1;   //正常返回正值
    }
    /*******************************************************************************
     * Function Name  : closeSerialPort
     * Description    : 关闭相应串口
     * Input          : None
     * Return         : None
     *******************************************************************************/
    public void closeSerialPort() {
        if(isOpen){
            serialPortHelper.closeDevice();
            isOpen  =   false;
        }
    }
    /*******************************************************************************
     * Function Name  : Check_Response_is_OK
     * Description    : 检查串口命令校验和
     * Input          : None
     * Return         : true:成功；
     * 影 响			 ：None
     *******************************************************************************/
    private boolean Check_Response_is_OK(byte[] received_buffer )
    {
        byte check;
        byte i,len,len1,len2;

        if((received_buffer[0]==CMD_HEADER1)&&(received_buffer[1]==CMD_HEADER2)&&(received_buffer[2]==CMD_HEADER3))	  //检查命令头
        {
            len1	=	received_buffer[3];	//包长度
            len2	=	received_buffer[5];	//包长度反码
            len2	=	(byte)(0xff-len2);
            if((len1==len2))	//检查长度及其反码
            {
                //指令长度超过缓冲区不处理
                if(len1>=(UART_BUFFER_SIZE-UART_CMD_HEADER_TAIL_LENGTH))
                {
                    return false;
                }
                if(received_buffer[len1+UART_CMD_HEADER_TAIL_LENGTH-1]!=CMD_TAIL)	//检查命令尾
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }

        len	=	received_buffer[3];	//包长度
        check	=	0;
        for(i=0;i<len;i++)
        {check	+=	received_buffer[i+UART_CMD_HEADER_LENGTH];
        }
        if(check==received_buffer[len+UART_CMD_HEADER_LENGTH])
            return true;
        else
            return false;
    }

    /*****************************************************************
     程序名: Mixing_Transceive_Command
     输入: send_len:为data数据长度，命令字不包含在内
     输出: 无
     返回: 无
     说明：JCM接口，发送接收指令
     *****************************************************************/
    private int Mixing_Transceive_Command(byte cmd,byte[] data,int send_len,byte[]receive_buf)
    {
        int i,len,size;
        int ret;
        byte[] RS232_Cmd_BUFFER   =   new byte[10+send_len];

        len = send_len+7;			//传入send_len，为data数据长度，命令字不包含在内
        RS232_Cmd_BUFFER[0] = CMD_HEADER1;
        RS232_Cmd_BUFFER[1] = CMD_HEADER2;
        RS232_Cmd_BUFFER[2] = CMD_HEADER3;
        RS232_Cmd_BUFFER[3] = (byte)(send_len+1);
        RS232_Cmd_BUFFER[4] = 0;
        RS232_Cmd_BUFFER[5] = (byte)(0xff-RS232_Cmd_BUFFER[3]);
        RS232_Cmd_BUFFER[6] = (byte)(0xff-RS232_Cmd_BUFFER[4]);
        RS232_Cmd_BUFFER[7] = cmd;

        if(send_len>0)
        {
            for(i=0;i<send_len;i++)
            {
                RS232_Cmd_BUFFER[8+i] = data[i];
            }
        }
        //计算累加和
        byte check = 0;
        len = send_len+1;
        for(i=0;i<len;i++)
        {
            check += RS232_Cmd_BUFFER[7+i];
        }
        RS232_Cmd_BUFFER[len+7]   = check;
        RS232_Cmd_BUFFER[len+8] = CMD_TAIL;
        serialPortHelper.sendCommands(RS232_Cmd_BUFFER,0);
        try {
            Thread.sleep(100);
        } catch (Exception e){
            e.printStackTrace();
        }
        //len = Wait_Receive(100);
        //if(len>=UART_CMD_HEADER_TAIL_LENGTH)
        byte[] all_receive_buf =  serialPortHelper.receiveCommands(UART_BUFFER_SIZE,2000);
        if (all_receive_buf != null)
        {
            if(Check_Response_is_OK(all_receive_buf))
            {
                len = all_receive_buf[3];	//包长度
                for(i=0;i<len;i++)
                {
                    receive_buf[i]  =   all_receive_buf[i+UART_CMD_HEADER_LENGTH];
                }
                return param_Constant.CMD_OK;
            }
            else
            {
                return param_Constant.CMD_VERIFY_SUM_ERROR;
            }
        }
        else
        {
            ret	=	param_Constant.CMD_ERR_RESPONSE_TIMEOUT;;
        }
        return ret;
    }
    //读现调机状态
    public int getMixingDeviceStatus()
    {
        int ret;
        int result;
        byte[] send_buffer   =   new byte[UART_BUFFER_SIZE];
        byte[] response_buffer   =   new byte[UART_BUFFER_SIZE];
        byte[] temp_buffer = new byte[16];
        ret = Mixing_Transceive_Command(RS232_CMD_Read_DEVICE_Status_info,send_buffer,0,response_buffer);
        if(ret==param_Constant.CMD_OK)
        {
            result = (int)response_buffer[1];
            temp_buffer[0]  =   response_buffer[2];
            temp_buffer[1]  =   response_buffer[3];
            temp_buffer[2]  =   response_buffer[4];
            temp_buffer[3]  =   response_buffer[5];
            temp_buffer[4]  =   0;temp_buffer[5]  =   0;temp_buffer[6]  =   0;
            //版本
            deviceStatus_Version   = new String(temp_buffer);
            //设备ID,一个字节一个字节转换
            StringBuffer hexStringBuffer = new StringBuffer();
            for (int i = 0; i < 4; i++) {
                hexStringBuffer.append(DataConversion.byteToHex(response_buffer[9-i]));
            }
            deviceStatus_DevID  =  hexStringBuffer.toString();
            //挂在基站的从设备个数
            deviceStatus_SlaveDeviceNumber =  response_buffer[10];
            return result;
        }
        return ret;

    }
    //读所有从机设备状态
    public int getAllSlaveDeviceStatus()
    {
        int ret;
        int result;
        byte[] send_buffer   =   new byte[UART_BUFFER_SIZE];
        byte[] response_buffer   =   new byte[UART_BUFFER_SIZE];
        ret = Mixing_Transceive_Command(RS232_CMD_All_Slave_Device_Info,send_buffer,0,response_buffer);
        if(ret==param_Constant.CMD_OK)
        {
            result = (int)response_buffer[1];
            if(result==0) {
                slave_DeviceInfo.total_device_number = (int) response_buffer[2];
                int j=0;
                for(int i=0;i<slave_DeviceInfo.total_device_number;i++)
                {
                    StringBuffer hexStringBuffer = new StringBuffer();
                    hexStringBuffer.append(DataConversion.byteToHex(response_buffer[6+j]));
                    hexStringBuffer.append(DataConversion.byteToHex(response_buffer[5+j]));
                    hexStringBuffer.append(DataConversion.byteToHex(response_buffer[4+j]));
                    hexStringBuffer.append(DataConversion.byteToHex(response_buffer[3+j]));
                    slave_DeviceInfo.dev_id[i] = hexStringBuffer.toString();
                    slave_DeviceInfo.Config_Flag[i] = response_buffer[7+j];
                    j=j+5;
                }
            }
            return result;
        }
        return ret;

    }

    //读指定从机设备状态
    public int getGivenSlaveDeviceStatus(String DevID)
    {
        int ret,iTemp;
        int result;
        //参数不正确
        if(DevID.isEmpty()) return -1;
        byte[] send_buffer   =   new byte[UART_BUFFER_SIZE];
        byte[] response_buffer   =   new byte[UART_BUFFER_SIZE];
        byte[] id_buffer  = DataConversion.decodeHexString(DevID);
        send_buffer[0]  =   id_buffer[3];//反序
        send_buffer[1]  =   id_buffer[2];
        send_buffer[2]  =   id_buffer[1];
        send_buffer[3]  =   id_buffer[0];
        ret = Mixing_Transceive_Command(RS232_CMD_Get_Slave_Device_Status,send_buffer,4,response_buffer);
        if(ret==param_Constant.CMD_OK)
        {
            result = (int)response_buffer[1];
            if(result==0) {
                //设置版本
                iTemp = response_buffer[3];
                iTemp = (iTemp<<8)|response_buffer[2];
                slave_DeviceInfo.setStatus_Version(DataConversion.intToHex(iTemp));
                slave_DeviceInfo.setStatus_State(response_buffer[4]);       //接杯状态
                slave_DeviceInfo.setContained_time(response_buffer[5]);     //已经接杯时长
                slave_DeviceInfo.setContained_time_100ms(response_buffer[6]);   //已接杯时长的小数部分
                slave_DeviceInfo.setLittle_USED_TIME(response_buffer[7]);   //接满小杯所用时长
                slave_DeviceInfo.setMiddle_USED_TIME(response_buffer[8]);   //接满中杯所用时长
                slave_DeviceInfo.setBig_USED_TIME(response_buffer[9]);      //接满大杯所用时长
                slave_DeviceInfo.setMAX_Big_USED_TIME(response_buffer[10]);     //接满超大杯所用时长
                slave_DeviceInfo.setSet_Cup_State(response_buffer[11]);     //设置接杯状态
            }
             return result;
        }
        return ret;

    }
    //读指定从机设备的码表信息
    public int getGivenSlaveDevice_SumInfo(String DevID)
    {
        int ret,offset;
        int result;
        long lTemp;
        //参数不正确
        if(DevID.isEmpty()) return -1;
        byte[] send_buffer   =   new byte[UART_BUFFER_SIZE];
        byte[] response_buffer   =   new byte[UART_BUFFER_SIZE];
        byte[] id_buffer  = DataConversion.decodeHexString(DevID);
        send_buffer[0]  =   id_buffer[3];//反序
        send_buffer[1]  =   id_buffer[2];
        send_buffer[2]  =   id_buffer[1];
        send_buffer[3]  =   id_buffer[0];
        ret = Mixing_Transceive_Command(RS232_CMD_Get_Slave_Device_Sum,send_buffer,4,response_buffer);
        if(ret==param_Constant.CMD_OK)
        {
            result = (int)response_buffer[1];
            if(result==0) {
                //接杯总值
                offset = 5;
                lTemp = response_buffer[offset];
                lTemp = (lTemp<<8)|response_buffer[offset-1];
                lTemp = (lTemp<<8)|response_buffer[offset-2];
                lTemp = (lTemp<<8)|response_buffer[offset-3];
                slave_DeviceInfo.setTotal_Cup_Sum(lTemp);
                //接小杯总值,占3字节
                offset = offset+3;
                lTemp = response_buffer[offset];
                lTemp = (lTemp<<8)|response_buffer[offset-1];
                lTemp = (lTemp<<8)|response_buffer[offset-2];
                slave_DeviceInfo.setLittle_Cup_Sum(lTemp);
                //接中杯总值,占3字节
                offset = offset+3;
                lTemp = response_buffer[offset];
                lTemp = (lTemp<<8)|response_buffer[offset-1];
                lTemp = (lTemp<<8)|response_buffer[offset-2];
                slave_DeviceInfo.setMiddle_Cup_Sum(lTemp);
                //接大杯总值,占4字节
                offset = offset+4;
                lTemp = response_buffer[offset];
                lTemp = (lTemp<<8)|response_buffer[offset-1];
                lTemp = (lTemp<<8)|response_buffer[offset-2];
                lTemp = (lTemp<<8)|response_buffer[offset-3];
                slave_DeviceInfo.setBig_Cup_Sum(lTemp);
                //接超大杯总值,占3字节
                offset = offset+3;
                lTemp = response_buffer[offset];
                lTemp = (lTemp<<8)|response_buffer[offset-1];
                lTemp = (lTemp<<8)|response_buffer[offset-2];
                slave_DeviceInfo.setMax_Big_Cup_Sum(lTemp);
                //终止接杯总值,占3字节
                offset = offset+3;
                lTemp = response_buffer[offset];
                lTemp = (lTemp<<8)|response_buffer[offset-1];
                lTemp = (lTemp<<8)|response_buffer[offset-2];
                slave_DeviceInfo.setAbort_Cup_Sum(lTemp);
            }
            return result;
        }
        return ret;
    }
    //清除指定从机设备的码表信息
    public int clearGivenSlaveDevice_SumInfo(String DevID)
    {
        int ret,offset;
        int result;
        long lTemp;
        //参数不正确
        if(DevID.isEmpty()) return -1;
        byte[] send_buffer   =   new byte[UART_BUFFER_SIZE];
        byte[] response_buffer   =   new byte[UART_BUFFER_SIZE];
        byte[] id_buffer  = DataConversion.decodeHexString(DevID);
        send_buffer[0]  =   id_buffer[3];//反序
        send_buffer[1]  =   id_buffer[2];
        send_buffer[2]  =   id_buffer[1];
        send_buffer[3]  =   id_buffer[0];
        ret = Mixing_Transceive_Command(RS232_CMD_Clear_Slave_Device_Sum,send_buffer,4,response_buffer);
        if(ret==param_Constant.CMD_OK)
        {
            result = (int)response_buffer[1];
            if(result==0) {
                //接杯总值
                offset = 5;
                lTemp = response_buffer[offset];
                lTemp = (lTemp<<8)|response_buffer[offset-1];
                lTemp = (lTemp<<8)|response_buffer[offset-2];
                lTemp = (lTemp<<8)|response_buffer[offset-3];
                slave_DeviceInfo.setTotal_Cup_Sum(lTemp);
                //接小杯总值,占3字节
                offset = offset+3;
                lTemp = response_buffer[offset];
                lTemp = (lTemp<<8)|response_buffer[offset-1];
                lTemp = (lTemp<<8)|response_buffer[offset-2];
                slave_DeviceInfo.setLittle_Cup_Sum(lTemp);
                //接中杯总值,占3字节
                offset = offset+3;
                lTemp = response_buffer[offset];
                lTemp = (lTemp<<8)|response_buffer[offset-1];
                lTemp = (lTemp<<8)|response_buffer[offset-2];
                slave_DeviceInfo.setMiddle_Cup_Sum(lTemp);
                //接大杯总值,占4字节
                offset = offset+4;
                lTemp = response_buffer[offset];
                lTemp = (lTemp<<8)|response_buffer[offset-1];
                lTemp = (lTemp<<8)|response_buffer[offset-2];
                lTemp = (lTemp<<8)|response_buffer[offset-3];
                slave_DeviceInfo.setBig_Cup_Sum(lTemp);
                //接超大杯总值,占3字节
                offset = offset+3;
                lTemp = response_buffer[offset];
                lTemp = (lTemp<<8)|response_buffer[offset-1];
                lTemp = (lTemp<<8)|response_buffer[offset-2];
                slave_DeviceInfo.setMax_Big_Cup_Sum(lTemp);
                //终止接杯总值,占3字节
                offset = offset+3;
                lTemp = response_buffer[offset];
                lTemp = (lTemp<<8)|response_buffer[offset-1];
                lTemp = (lTemp<<8)|response_buffer[offset-2];
                slave_DeviceInfo.setAbort_Cup_Sum(lTemp);
            }
            return result;
        }
        return ret;
    }
    //启动指定从机设备接杯
    public int startGivenSlaveDevice_GetWater(String DevID,int cup_type,int number)
    {
        int ret;
        int result;
        //参数不正确
        if(DevID.isEmpty()) return -1;
        byte[] send_buffer   =   new byte[UART_BUFFER_SIZE];
        byte[] response_buffer   =   new byte[UART_BUFFER_SIZE];
        byte[] id_buffer  = DataConversion.decodeHexString(DevID);
        send_buffer[0]  =   id_buffer[3];//反序
        send_buffer[1]  =   id_buffer[2];
        send_buffer[2]  =   id_buffer[1];
        send_buffer[3]  =   id_buffer[0];
        send_buffer[4]  =   (byte)cup_type; //接杯类型:1小杯，2中杯,3大杯,4特大杯
        send_buffer[5]  =   (byte)number;   //接杯数量
        ret = Mixing_Transceive_Command(RS232_CMD_Start_Get_Water,send_buffer,6,response_buffer);
        if(ret==param_Constant.CMD_OK)
        {
            result = (int)response_buffer[1];
            if(result==0)
            {
                current_CupType = cup_type;
            }
            return result;
        }
        return ret;
    }
    //终止指定从机设备接杯
    public int abortGivenSlaveDevice_GetWater(String DevID)
    {
        int ret;
        int result;
        //参数不正确
        if(DevID.isEmpty()) return -1;
        byte[] send_buffer   =   new byte[UART_BUFFER_SIZE];
        byte[] response_buffer   =   new byte[UART_BUFFER_SIZE];
        byte[] id_buffer  = DataConversion.decodeHexString(DevID);
        send_buffer[0]  =   id_buffer[3];//反序
        send_buffer[1]  =   id_buffer[2];
        send_buffer[2]  =   id_buffer[1];
        send_buffer[3]  =   id_buffer[0];
        ret = Mixing_Transceive_Command(RS232_CMD_Abort_Get_Water,send_buffer,4,response_buffer);
        if(ret==param_Constant.CMD_OK)
        {
            result = (int)response_buffer[1];
            current_CupType = param_Constant.GET_CUP_TYPE_NONE;
            return result;
        }
        return ret;
    }
    public int enter_getCup_setting(String DevID,int cup_type)
    {
        return enter_getCup_setting(DevID,cup_type,0);
    }
    //进入接杯参数设置,second为0时，手动设置
    public int enter_getCup_setting(String DevID,int cup_type,int second)
    {
        int ret;
        int result;
        //参数不正确
        if(DevID.isEmpty()) return -1;
        byte[] send_buffer   =   new byte[UART_BUFFER_SIZE];
        byte[] response_buffer   =   new byte[UART_BUFFER_SIZE];
        byte[] id_buffer  = DataConversion.decodeHexString(DevID);
        send_buffer[0]  =   id_buffer[3];//反序
        send_buffer[1]  =   id_buffer[2];
        send_buffer[2]  =   id_buffer[1];
        send_buffer[3]  =   id_buffer[0];
        send_buffer[4]  =   (byte)cup_type;
        send_buffer[5]  =   (byte)second;
        ret = Mixing_Transceive_Command(RS232_CMD_ENTER_SETTING,send_buffer,6,response_buffer);
        if(ret==param_Constant.CMD_OK)
        {
            result = (int)response_buffer[1];
            return result;
        }
        return ret;
    }
    //退出接杯参数设置
    public int exit_getCup_setting(String DevID)
    {
        int ret;
        int result;
        //参数不正确
        if(DevID.isEmpty()) return -1;
        byte[] send_buffer   =   new byte[UART_BUFFER_SIZE];
        byte[] response_buffer   =   new byte[UART_BUFFER_SIZE];
        byte[] id_buffer  = DataConversion.decodeHexString(DevID);
        send_buffer[0]  =   id_buffer[3];//反序
        send_buffer[1]  =   id_buffer[2];
        send_buffer[2]  =   id_buffer[1];
        send_buffer[3]  =   id_buffer[0];
        ret = Mixing_Transceive_Command(RS232_CMD_EXIT_SETTING,send_buffer,4,response_buffer);
        if(ret==param_Constant.CMD_OK)
        {
            result = (int)response_buffer[1];
            if(result==0) {
                slave_DeviceInfo.setStatus_State(response_buffer[4]);       //接杯状态
                slave_DeviceInfo.setContained_time(response_buffer[5]);     //已经接杯时长
                slave_DeviceInfo.setContained_time_100ms(response_buffer[6]);   //已接杯时长的小数部分
                slave_DeviceInfo.setLittle_USED_TIME(response_buffer[7]);   //接满小杯所用时长
                slave_DeviceInfo.setMiddle_USED_TIME(response_buffer[8]);   //接满中杯所用时长
                slave_DeviceInfo.setBig_USED_TIME(response_buffer[9]);      //接满大杯所用时长
                slave_DeviceInfo.setMAX_Big_USED_TIME(response_buffer[10]);     //接满超大杯所用时长
            }
            return result;
        }
        return ret;
    }
    //删除指定从机设备
    public int delete_this_SlaveDevice(String DevID)
    {
        int ret;
        int result;
        //参数不正确
        if(DevID.isEmpty()) return -1;
        byte[] send_buffer   =   new byte[UART_BUFFER_SIZE];
        byte[] response_buffer   =   new byte[UART_BUFFER_SIZE];
        byte[] id_buffer  = DataConversion.decodeHexString(DevID);
        send_buffer[0]  =   id_buffer[3];//反序
        send_buffer[1]  =   id_buffer[2];
        send_buffer[2]  =   id_buffer[1];
        send_buffer[3]  =   id_buffer[0];
        ret = Mixing_Transceive_Command(RS232_CMD_DELETE_SLAVE_DEVICE,send_buffer,4,response_buffer);
        if(ret==param_Constant.CMD_OK)
        {
            result = (int)response_buffer[1];
            if(result==0) {
                slave_DeviceInfo.total_device_number = (int) response_buffer[2];
                int j=0;
                for(int i=0;i<slave_DeviceInfo.total_device_number;i++)
                {
                    StringBuffer hexStringBuffer = new StringBuffer();
                    hexStringBuffer.append(DataConversion.byteToHex(response_buffer[6+j]));
                    hexStringBuffer.append(DataConversion.byteToHex(response_buffer[5+j]));
                    hexStringBuffer.append(DataConversion.byteToHex(response_buffer[4+j]));
                    hexStringBuffer.append(DataConversion.byteToHex(response_buffer[3+j]));
                    slave_DeviceInfo.dev_id[i] = hexStringBuffer.toString();
                    slave_DeviceInfo.Config_Flag[i] = response_buffer[7+j];
                    j=j+5;
                }
            }
            return result;
        }
        return ret;
    }

    //定位
    public int position_Device(String DevID)
    {
        int ret;
        int result;
        //参数不正确
        if(DevID.isEmpty()) return -1;
        byte[] send_buffer   =   new byte[UART_BUFFER_SIZE];
        byte[] response_buffer   =   new byte[UART_BUFFER_SIZE];
        byte[] id_buffer  = DataConversion.decodeHexString(DevID);
        send_buffer[0]  =   id_buffer[3];//反序
        send_buffer[1]  =   id_buffer[2];
        send_buffer[2]  =   id_buffer[1];
        send_buffer[3]  =   id_buffer[0];
        ret = Mixing_Transceive_Command(RS232_CMD_POSITION_DEVICE,send_buffer,4,response_buffer);
        if(ret!=param_Constant.CMD_OK) {
            return -1;
        }
        return 0;
    }
}
