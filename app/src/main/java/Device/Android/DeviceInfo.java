package Device.Android;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;

import java.util.List;

import BaseActivity.BaseActivity;
import Entity.RequestPermissionsResult;
import Interface.IAsynCallBackListener;

/**
 * Created by Administrator on 2017-09-02.
 */

public class DeviceInfo {
    /**
     * 获取手机 Imemi 号
     * @return
     */
    public static String GetImei(Context ctx) {

        try {
            if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                ((BaseActivity)ctx).RequestPermission("Manifest.permission.READ_PHONE_STATE",new IAsynCallBackListener()
                {
                    @Override
                    public void onFinish(Object sender,Object data) {
                        List<RequestPermissionsResult> list=(List<RequestPermissionsResult>)data;
                        for ( RequestPermissionsResult permiss:list) {
                            if (permiss.getPermissionsName().equalsIgnoreCase("android.permission.READ_PHONE_STATE")
                                    && permiss.getRequestRes()==0) {

                            }
                        }
                    }

                    @Override
                    public void onError(Object sender, Exception e) {

                    }
                });
            }
            else {
                TelephonyManager tm = (TelephonyManager) ctx.getSystemService(ctx.TELEPHONY_SERVICE);
                if (tm.getDeviceId()==null)
                {
                    return GetAndroid_ID(ctx);
                }
                else {
                    return tm.getDeviceId();
                }
            }
        }
        catch (Exception e)
        {
            //Intent intent = new Intent(ctx,CaptureActivity.class);
            //ctx.startActivity(intent);
        }
        return "";
    }

    public static String GetAndroid_ID(Context ctx) {
        return Settings.System.getString(ctx.getContentResolver(), Settings.System.ANDROID_ID);
    }
    /**
     * 获取系统版本号
     * @return
     */
    public static String GetOsVer()
    {
        return Build.VERSION.RELEASE;
    }
    /**
     * 获取系统版本号
     * @return
     */
    public static String GetOsVerCode()
    {
        return Build.VERSION.SDK;
    }
}
