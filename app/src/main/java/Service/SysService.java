/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import android.content.Context;
import android.os.Build;

import com.example.administrator.drinkdevice.BuildConfig;

import org.json.JSONObject;

import Const.PublicDefine;
import DataAccess.OptionDataAccess;
import Device.Android.DeviceInfo;
import Helper.*;

public class SysService {
    public static long DeviceID = 0x88888888l;
    public static long DeviceType = 0xEF000001l;
    public static String HardVersion = "V1.0";
    public static String SoftVersion = "V2.5";


    public static InfoList m_InfoList = null;

    public static boolean ReadSysInfo(Context ctx)  {
        try {
            HardVersion = DeviceInfo.GetOsVer();
            SoftVersion = App.getVersionName(ctx);
            DeviceID = CRCControl.getCrc32(Build.SERIAL.getBytes());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return true;
    }

}
