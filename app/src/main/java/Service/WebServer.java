/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import Const.PublicDefine;
import Interface.IAsynCallBackListener;
import Interface.ICommProtocol;
import Helper.*;
import Interface.IWebService;
import Factory.Factory;

public class WebServer implements IWebService {

    private Context ctx;
    private ICommProtocol commProtocol;


    public WebServer(Context context) {
        this.ctx = context;
        commProtocol = Factory.GetInstance(CommProtocol.class, new Object[]{ctx});
    }

    @Override
    public void PostState(String cinemaCode, IAsynCallBackListener callback) throws Exception {
        String url = "http://" + PublicDefine.serviceUrl + "/bar/HallDevice/PostState";
        JSONObject param = new JSONObject();
        param.put("id", String.format("%08X", SysService.DeviceID));
        param.put("cinemaCode", cinemaCode);
        param.put("type", String.format("%08X", SysService.DeviceType));
        param.put("hardVer", "V" + SysService.HardVersion.trim());
        param.put("softVer", "V" + SysService.SoftVersion);
        param.put("ip", App.getHostIP());
        param.put("name", "饮料机");

        commProtocol.Post(url, param.toString(), callback, "application/json");

    }

    @Override
    public void getShopGoods(IAsynCallBackListener callBack) throws Exception {
        String url = "http://" + PublicDefine.serviceUrl + "/goods/goods/getShopGoods";
        JSONObject searchInfo = new JSONObject();
        searchInfo.put("shopId", PublicDefine.cinemaInfo.getString("shopId"));
        searchInfo.put("breedId", 3);
        searchInfo.put("state", 1);

        JSONObject param = new JSONObject();
        param.put("pageIndex", 1);
        param.put("pageSize", 9999);
        param.put("searchInfo", searchInfo);
        commProtocol.Post(url, param.toString(), callBack, "application/json");
    }

    @Override
    public void getDeviceInfo(IAsynCallBackListener callBackListener) throws Exception {
        String url = "http://" + PublicDefine.serviceUrl + "/bar/HallDevice/getHallDeviceInfo?id="
                + String.format("%08X", SysService.DeviceID);

        commProtocol.Get(url, callBackListener);
    }

    @Override
    public void getBarList(IAsynCallBackListener callBackListener) throws Exception {
        String url = "http://" + PublicDefine.serviceUrl + "/bar/BisBarGoodsList/getBarList?device_id="
                + String.format("%08X", SysService.DeviceID) + "&shopId=" + PublicDefine.cinemaInfo.getInt("shopId");

        commProtocol.Get(url, callBackListener);
    }

    @Override
    public void saleGoodsCalculate(JSONArray goods, String memberId, int payType, IAsynCallBackListener callBackListener) throws Exception {
        String url = "http://" + PublicDefine.serviceUrl + "/api/order/saleGoodsCalculate";

        JSONObject param = new JSONObject();
        param.put("cinemaId", PublicDefine.cinemaInfo.getString("id"));
        param.put("goods", goods);
        if (memberId != null) {
            param.put("memberId", memberId);
        }
        param.put("channelId", 3);
        param.put("payType", payType);
        commProtocol.Post(url, param.toString(), callBackListener, "application/json");
    }

    @Override
    public void getMemberInfoList(String cardNo, IAsynCallBackListener callBackListener) throws Exception {
        String url = "http://" + PublicDefine.serviceUrl + "/api/member/getMemberInfoList?pageSize=9999&pageNo=1&cardNo=" + cardNo;
        commProtocol.Get(url, callBackListener);
    }

    @Override
    public void getMemberStoredCard(String memberId, IAsynCallBackListener callBackListener) throws Exception {
        String url = "http://" + PublicDefine.serviceUrl + "/api/activity/getStoredCard?"
                + "cinemaId=" + PublicDefine.cinemaInfo.getString("id")
                + "&memberId=" + memberId;
        commProtocol.Get(url, callBackListener);
    }

    @Override
    public void createSaleOrder(String memberId,String tel,JSONArray goods, int payType, BigDecimal payCash, BigDecimal discount, BigDecimal ducprice,String deviceName, IAsynCallBackListener callBackListener) throws Exception {
        String url = "http://" + PublicDefine.serviceUrl + "/api/order/createSaleOrder";

        JSONObject param = new JSONObject();
        param.put("cinemaId", PublicDefine.cinemaInfo.getString("id"));
        param.put("goods", goods);
        param.put("payType", payType);
        param.put("payCash", payCash);
        param.put("discount", discount);
        param.put("channelId", 3);
        param.put("ducprice", ducprice);
        param.put("terminal", "");
        param.put("terminalCode", String.format("%08X", SysService.DeviceID));
        param.put("casherId", String.format("%08X", SysService.DeviceID));
        param.put("casherName", deviceName);
        if (memberId != null){
            param.put("memberId", memberId);
        }
        if (tel != null){
            param.put("tel", tel);
        }
        commProtocol.Post(url, param.toString(), callBackListener, "application/json");
    }

    @Override
    public void payV2(BigDecimal totalPayAmount, int payType, String memberId, String phone, JSONArray orders,JSONArray storeCards, IAsynCallBackListener callBackListener) throws Exception {
        String url = "http://" + PublicDefine.serviceUrl + "/api/pay/payV2";

        JSONObject param = new JSONObject();
        param.put("totalPayAmount", totalPayAmount);
        param.put("payType", payType);
        param.put("orders", orders);
        if (memberId != null){
            param.put("memberId", memberId);
        } else {
            param.put("memberId", "");
        }
        if (phone != null){
            param.put("phone", phone);
        } else {
            param.put("phone", "");
        }
        if (storeCards != null){
            param.put("storeCards", storeCards);
        }
        commProtocol.Post(url, param.toString(), callBackListener, "application/json");
    }

    @Override
    public void take(String takeCode, String storageId,String deviceName, IAsynCallBackListener callBackListener) throws Exception {
        String url = "http://" + PublicDefine.serviceUrl + "/barCarsher/take";

        JSONObject param = new JSONObject();
        param.put("takeCode", takeCode);
        param.put("operatorId", String.format("%08X", SysService.DeviceID));
        param.put("operatorName", deviceName);
        param.put("storageId", storageId);
        param.put("deviceId", String.format("%08X", SysService.DeviceID));
        commProtocol.Post(url, param.toString(), callBackListener, "application/json");
    }

    @Override
    public void UpdateVerify(String appName, Integer appVer,
                             String produceVer,final IAsynCallBackListener callback) {
        String url = String.format("http://up.yingjiayun.com/AppUpdate/UpdateVerify?appName=%s&CurrentVer=%d&deviceid=%s&produceVer=%s",
                appName, appVer, String.format("%08X", SysService.DeviceID), produceVer);
        try {
            commProtocol.Get(url, new IAsynCallBackListener() {
                @Override
                public void onFinish(Object sender, Object data) {
                    try {
                        String ResStr = data.toString();
                        JSONObject jsonObject = new JSONObject(ResStr);
                        callback.onFinish(sender, jsonObject);
                    } catch (Exception e) {
                        e.printStackTrace();
                        callback.onError(sender, e);
                    }
                }

                @Override
                public void onError(Object sender, Exception e) {
                    callback.onError(sender, e);
                }
            });
        } catch (Exception e) {
            callback.onError(this, e);
        }
    }

    @Override
    public void getDeviceSaleSum(IAsynCallBackListener callBackListener) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String url = "http://" + PublicDefine.serviceUrl + "/GoodsDayReport/getDeviceSaleSum?"
                + "pageNo=1"
                + "&pageSize=99999"
                + "&reportDay=" + sdf.format(new Date())
                + "&deviceId=" + String.format("%08X", SysService.DeviceID)
                + "&cinemaId=" + PublicDefine.cinemaInfo.getString("id");
        commProtocol.Get(url, callBackListener);
    }

    @Override
    public void getGoodsLocationList(int storageId,IAsynCallBackListener callBackListener) throws Exception {
        String url = "http://" + PublicDefine.serviceUrl + "/goods/storage/getGoodsLocationList";

        JSONObject searchInfo = new JSONObject();
        searchInfo.put("storageId",storageId);

        JSONObject param = new JSONObject();
        param.put("pageIndex", 1);
        param.put("pageSize", 9999);
        param.put("shopId", PublicDefine.cinemaInfo.getInt("shopId"));
        param.put("searchInfo", searchInfo);
        commProtocol.Post(url, param.toString(), callBackListener, "application/json");
    }

    @Override
    public void checkGoodsStock(int storageId, int locationId, JSONArray goodsList, IAsynCallBackListener callBackListener) throws Exception {
        String url = "http://" + PublicDefine.serviceUrl + "/goods/storage/checkGoodsStock";

        JSONObject param = new JSONObject();
        param.put("storageId", storageId);
        param.put("locationId", locationId);
        param.put("goodsList", goodsList);
        commProtocol.Post(url, param.toString(), callBackListener, "application/json");
    }

    @Override
    public void microPay(String orderId, String payOrderId, String authCode, IAsynCallBackListener callBackListener) throws Exception {
        String url = "http://" + PublicDefine.serviceUrl + "/api/pay/microPay";

        JSONObject param = new JSONObject();
        param.put("orderId", orderId);
        param.put("payOrderId", payOrderId);
        param.put("authCode", authCode);
        commProtocol.Post(url, param.toString(), callBackListener, "application/json");
    }

    @Override
    public String searchPay(String payOrderId) throws Exception {
        String url = "http://" + PublicDefine.serviceUrl + "/api/pay/searchPay";

        JSONObject param = new JSONObject();
        param.put("payOrderId", payOrderId);
        return commProtocol.Post(url, param.toString(), "application/json");
    }

    @Override
    public void completeOrder(String orderId, String paymentOrderId, IAsynCallBackListener callBackListener) throws Exception {
        String url = "http://" + PublicDefine.serviceUrl + "/bar/order/completeOrder";

        JSONObject param = new JSONObject();
        param.put("orderId", orderId);
        param.put("paymentOrderId", paymentOrderId);
        commProtocol.Post(url, param.toString(), callBackListener, "application/json");
    }

    @Override
    public void ticketMessage(String orderId,IAsynCallBackListener callBackListener) throws Exception {
        String url = "http://" + PublicDefine.serviceUrl + "/api/order/ticketMessage?orderId=" + orderId;
        commProtocol.Get(url, callBackListener);
    }
}
