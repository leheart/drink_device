package ViewModel;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Message;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.ArrayAdapter;

import com.example.administrator.drinkdevice.AddSelectGoodsActivity;
import com.example.administrator.drinkdevice.MainActivity;
import com.example.administrator.drinkdevice.R;
import com.example.administrator.drinkdevice.SystemInitActivity;
import com.google.gson.JsonParseException;

import org.json.JSONArray;
import org.json.JSONObject;

import Const.PublicDefine;
import Helper.FlowLayout;
import Interface.IAsynCallBackListener;
import Service.SysService;
import views.Activity_SelectGoods_GoodsInfo;

public class AddSelectGoods_ViewModel extends Base_ViewModel{

    public TextBind time = new TextBind("");
    public TextBind date = new TextBind("");
    public TextBind week = new TextBind("");
    private FlowLayout flowLayout;

    private JSONArray goodsList;
    private JSONObject selectGoods;
    private Activity_SelectGoods_GoodsInfo selectView;

    public AddSelectGoods_ViewModel(Context ctx) {
        super(ctx);
        BrushSystemTime();
    }

    public void Init() {
        try {
            flowLayout = ((AddSelectGoodsActivity)getBaseContext()).findViewById(R.id.flowLayout);
            flowLayout.removeAllViews();
            //获取卖品列表
            PublicDefine.webService.getBarList(new IAsynCallBackListener() {
                @Override
                public void onFinish(Object sender, Object data) {
                    final String resStr = (String) data;
                    Message msg = new Message();
                    msg.obj = new IAsynCallBackListener() {
                        @Override
                        public void onFinish(Object sender, Object data) {
                            try {
                                JSONObject res = new JSONObject(resStr);
                                if (!res.getBoolean("success")){
                                    throw new Exception(res.getString("message"));
                                }
                                JSONObject resData = res.getJSONObject("result");
                                goodsList = resData.getJSONArray("records");
                                for (int i = 0; i < goodsList.length(); i++) {
                                    JSONObject goodsInfo = goodsList.getJSONObject(i);
                                    Activity_SelectGoods_GoodsInfo goodsView = new Activity_SelectGoods_GoodsInfo(getBaseContext(), goodsInfo, new IAsynCallBackListener() {
                                        @Override
                                        public void onFinish(Object sender, Object data) {
                                            selectView = (Activity_SelectGoods_GoodsInfo) sender;
                                            selectGoods = (JSONObject) data;
                                            setSelectView();
                                        }

                                        @Override
                                        public void onError(Object sender, Exception e) {

                                        }
                                    });
                                    flowLayout.addView(goodsView);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                alert(GetExceptionMsg(e));
                            }
                        }

                        @Override
                        public void onError(Object sender, Exception e) {

                        }
                    };
                    mHandler.sendMessage(msg);
                }

                @Override
                public void onError(Object sender, Exception e) {
                    e.printStackTrace();
                    alert(GetExceptionMsg(e));
                }
            });
        } catch (Exception e){
            e.printStackTrace();
            alert(GetExceptionMsg(e));
        }
    }

    private void BrushSystemTime() {
        BroadcastReceiver receiver = new BroadcastReceiver() {
            private String currentDate = null;

            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Intent.ACTION_TIME_TICK)) {
                    getSystemTime();
                }
            }
        };

        getSystemTime();

        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_TIME_TICK);
        registerReceiver(receiver, filter);
    }

    private void getSystemTime() {
        Message msg = new Message();
        msg.arg1 = 1;
        msg.obj = new IAsynCallBackListener() {

            @Override
            public void onFinish(Object sender, Object data) {
                long sysTime = System.currentTimeMillis();//获取系统时间
                CharSequence sysTimeStr = DateFormat.format("HH:mm", sysTime);//时间显示格式
                time.Text.set(sysTimeStr.toString());
                sysTimeStr = DateFormat.format("yyyy年MM月dd日", sysTime);//时间显示格式
                date.Text.set(sysTimeStr.toString());
                sysTimeStr = DateFormat.format("EEEE", sysTime);//时间显示格式
                week.Text.set(sysTimeStr.toString());
            }

            @Override
            public void onError(Object sender, Exception e) {

            }
        };
        mHandler.sendMessage(msg);
    }

    public void okBtnClick(View view) {
        try {
            if (selectGoods == null){
                throw new Exception("请选择卖品!");
            }

            Intent intent = new Intent();
            intent.putExtra("goodsInfo", selectGoods.toString());
            ((AddSelectGoodsActivity)getBaseContext()).setResult(1, intent);
            ((AddSelectGoodsActivity)getBaseContext()).finish();
        } catch (Exception e){
            e.printStackTrace();
            alert(GetExceptionMsg(e));
        }
    }

    public void cancelBtnClick(View view) {
        try {
            Intent intent = new Intent();
            ((AddSelectGoodsActivity)getBaseContext()).setResult(0, intent);
            ((AddSelectGoodsActivity)getBaseContext()).finish();
        } catch (Exception e){
            e.printStackTrace();
            alert(GetExceptionMsg(e));
        }
    }

    public void setSelectView() {
        if (flowLayout == null){
            return;
        }

        for (int i = 0; i < flowLayout.getChildCount(); i++) {
            Activity_SelectGoods_GoodsInfo goodsView = (Activity_SelectGoods_GoodsInfo) flowLayout.getChildAt(i);
            if (goodsView == selectView){
                goodsView.setCheck(true);
            } else if ((boolean)goodsView.getTag()){
                goodsView.setCheck(false);
            }
        }
    }

}
