package ViewModel;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.text.format.DateFormat;
import android.view.View;


import org.json.JSONObject;

import Interface.IAsynCallBackListener;


public class MainActivity_ViewModel extends Base_ViewModel{

    public TextBind time = new TextBind("");
    public TextBind date = new TextBind("");
    public TextBind week = new TextBind("");

    public JSONObject pickDevice = null;
    public int checkCup = 1;
    public JSONObject selectGoods;
    public int takeCupFlag = 0;

    public MainActivity_ViewModel(Context ctx){
        super(ctx);
        BrushSystemTime();
    }

    private void BrushSystemTime() {
        BroadcastReceiver receiver = new BroadcastReceiver() {
            private String currentDate = null;

            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Intent.ACTION_TIME_TICK)) {
                    getSystemTime();
                }
            }
        };

        getSystemTime();

        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_TIME_TICK);
        registerReceiver(receiver, filter);
    }

    private void getSystemTime() {
        Message msg = new Message();
        msg.arg1 = 1;
        msg.obj = new IAsynCallBackListener() {

            @Override
            public void onFinish(Object sender, Object data) {
                long sysTime = System.currentTimeMillis();//获取系统时间
                CharSequence sysTimeStr = DateFormat.format("HH:mm", sysTime);//时间显示格式
                time.Text.set(sysTimeStr.toString());
                sysTimeStr = DateFormat.format("yyyy年MM月dd日", sysTime);//时间显示格式
                date.Text.set(sysTimeStr.toString());
                sysTimeStr = DateFormat.format("EEEE", sysTime);//时间显示格式
                week.Text.set(sysTimeStr.toString());
            }

            @Override
            public void onError(Object sender, Exception e) {

            }
        };
        mHandler.sendMessage(msg);
    }

}
