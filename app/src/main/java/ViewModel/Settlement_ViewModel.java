package ViewModel;

import android.content.Context;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import android.view.View;

import com.example.administrator.drinkdevice.MainActivity;
import com.example.administrator.drinkdevice.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.net.ResponseCache;
import java.security.spec.MGF1ParameterSpec;
import java.util.Map;
import java.util.regex.Pattern;

import Component.CustomViewPager;
import Const.PublicDefine;
import DataAccess.DeviceDataAccess;
import Device.Android.DeviceInfo;
import Device.colaMixing.param_Constant;
import Enums.PayType;
import Helper.FlowLayout;
import Interface.IAsynCallBackListener;
import views.Activity_SelectGoods_GoodsInfo;
import views.Dialog_Member_Payment;
import views.Dialog_Settlement;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;


public class Settlement_ViewModel extends Base_ViewModel {

    private CustomViewPager viewPager;
    private MainActivity_ViewModel mainViewModel;
    private DeviceDataAccess deviceDataAccess;

    public TextBind originPrice = new TextBind("0.00");
    public TextBind discountPrice = new TextBind("0.00");
    public TextBind salePrice = new TextBind("0.00");
    public TextBind timeout = new TextBind("");
    public ButtonBind okBtn = new ButtonBind("支付");
    public TextBind goodsName = new TextBind("");

    private Thread timeOutThread;//倒计时线程
    private Thread searchPayThread;//查单线程
    private boolean exitflag = false;//倒计时线程结束标志
    private boolean searchExitflag = false;//查单线程结束标志
    private int outTime = 30;//倒计时时间
    private JSONObject memberInfo;//会员信息
    private JSONObject storedCard;//储值卡信息
    private PayType payType = PayType.PAYMENT_WECHATMIR;//支付类型
    private int busiState = 0;//状态 0 起始状态; 1支付完成未完成订单; 2支付完成未取货; 3取货完成;
    private JSONObject deviceInfo;//设备信息
    private String authCode;//支付码
    private String orderId;//订单id
    private String payOrderId;//支付订单id
    private boolean scanFlag;
    private Dialog_Member_Payment paymentDialog;

    public Settlement_ViewModel(Context ctx, CustomViewPager viewPager, MainActivity_ViewModel mainViewModel) {
        super(ctx);
        this.viewPager = viewPager;
        this.mainViewModel = mainViewModel;
        deviceDataAccess = new DeviceDataAccess(ctx);
    }

    public void Init() {
        try {
            //初始化页面数据
            scanFlag = false;
            okBtn.Enabled.set(false);
            okBtn.Text.set("支付");
            goodsName.Text.set(mainViewModel.selectGoods.getString("goodsName"));
            storedCard = null;
            memberInfo = null;
            busiState = 0;
            deviceInfo = null;
            authCode = null;
            orderId = null;
            payOrderId = null;
            payType = PayType.PAYMENT_WECHATMIR;
            discountPrice.Text.set("0.00");
            originPrice.Text.set(doubleCoverHalfUp(mainViewModel.selectGoods.getDouble("goodsPrice")));
            salePrice.Text.set(originPrice.Text.get());


            saleGoodsCalculate();
            getDeviceInfo();

            startTimeOut(30);
        } catch (Exception e) {
            e.printStackTrace();
            alert(GetExceptionMsg(e));
        }
    }

    //double转四舍五入字符串
    private String doubleCoverHalfUp(double num) {
        return new BigDecimal(num).setScale(2, BigDecimal.ROUND_HALF_UP) + "";
    }

    //开始倒计时
    private void startTimeOut(int time) {
        exitflag = false;
        outTime = time;
        timeOutThread = new Thread(runnableUi);
        timeOutThread.start();
    }

    //倒计时线程
    Runnable runnableUi = new Runnable() {
        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            while (!exitflag) {
                try {
                    timeout.Text.set(outTime + "s");
                    outTime--;
                    if (outTime == 0) {
                        cancelClick(null);
                        exitflag = true;
                        return;
                    }
                    Thread.sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                    alert(GetExceptionMsg(e));
                }
            }
        }
    };

    //取消按钮
    public void cancelClick(View view) {
        Message msg = new Message();
        msg.obj = new IAsynCallBackListener() {
            @Override
            public void onFinish(Object sender, Object data) {
                viewPager.setCurrentItem(0);
                exitflag = true;
            }

            @Override
            public void onError(Object sender, Exception e) {

            }
        };
        mHandler.sendMessage(msg);
    }

    //交易计算
    public void saleGoodsCalculate() {
        try {
            JSONArray goodsArray = new JSONArray();
            JSONObject goodsInfo = new JSONObject();
            goodsInfo.put("goodsId", mainViewModel.selectGoods.getString("goodsId"));
            goodsInfo.put("amount", 1);
            goodsInfo.put("dosing", new JSONArray());
            goodsArray.put(goodsInfo);

            String memberId = null;
            if (memberInfo != null && payType != PayType.PAYMENT_WECHATMIR) {
                memberId = memberInfo.getString("id");
            }

            PublicDefine.webService.saleGoodsCalculate(goodsArray, memberId, payType.ordinal(), new IAsynCallBackListener() {
                @Override
                public void onFinish(Object sender, Object data) {
                    final String resStr = (String) data;
                    Message msg = new Message();
                    msg.obj = new IAsynCallBackListener() {
                        @Override
                        public void onFinish(Object sender, Object data) {
                            try {

                                JSONObject res = new JSONObject(resStr);
                                if (!res.getBoolean("success")) {
                                    throw new Exception(res.getString("message"));
                                }
                                JSONObject resData = res.getJSONObject("result");
                                discountPrice.Text.set(doubleCoverHalfUp(resData.getJSONObject("discount").getDouble("cash") / 100));
                                salePrice.Text.set(doubleCoverHalfUp(resData.getDouble("bisCash") / 100));

                                if (memberInfo != null) {
                                    okBtn.Enabled.set(true);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                alert(GetExceptionMsg(e));
                            }
                        }

                        @Override
                        public void onError(Object sender, Exception e) {

                        }
                    };
                    mHandler.sendMessage(msg);
                }

                @Override
                public void onError(Object sender, Exception e) {
                    e.printStackTrace();
                    alert(GetExceptionMsg(e));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            alert(GetExceptionMsg(e));
        }
    }

    //扫码事件
    public void scan(String qrCode) {
        try {
            if (busiState != 0 || scanFlag){
                return;
            }
            scanFlag = true;
            Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
            if (pattern.matcher(qrCode).matches() &&
                    (qrCode.substring(0, 2).equals("10")
                            || qrCode.substring(0, 2).equals("11")
                            || qrCode.substring(0, 2).equals("12")
                            || qrCode.substring(0, 2).equals("13")
                            || qrCode.substring(0, 2).equals("14")
                            || qrCode.substring(0, 2).equals("15"))
                    && qrCode.length() == 18) {
                //微信
                payType = PayType.PAYMENT_WECHATMIR;
                authCode = qrCode;
            } else if (pattern.matcher(qrCode).matches()
                    && (qrCode.substring(0, 2).equals("25")
                    || qrCode.substring(0, 2).equals("26")
                    || qrCode.substring(0, 2).equals("27")
                    || qrCode.substring(0, 2).equals("28")
                    || qrCode.substring(0, 2).equals("29")
                    || qrCode.substring(0, 2).equals("30"))
                    && qrCode.length() >= 16 && qrCode.length() <= 24) {
                //支付宝
                payType = PayType.PAYMENT_ALIMIR;
                authCode = qrCode;
            } else {
                throw new Exception("无效二维码");
            }
            JSONArray goodsArray = new JSONArray();
            JSONObject goodsInfo = new JSONObject();
            goodsInfo.put("goodsId", mainViewModel.selectGoods.getString("goodsId"));
            goodsInfo.put("amount", 1);
            goodsInfo.put("dosing", new JSONArray());
            goodsArray.put(goodsInfo);

            PublicDefine.webService.saleGoodsCalculate(goodsArray, null, payType.ordinal(), new IAsynCallBackListener() {
                @Override
                public void onFinish(Object sender, Object data) {
                    final String resStr = (String) data;
                    Message msg = new Message();
                    msg.obj = new IAsynCallBackListener() {
                        @Override
                        public void onFinish(Object sender, Object data) {
                            try {

                                JSONObject res = new JSONObject(resStr);
                                if (!res.getBoolean("success")) {
                                    throw new Exception(res.getString("message"));
                                }
                                JSONObject resData = res.getJSONObject("result");
                                discountPrice.Text.set(doubleCoverHalfUp(resData.getJSONObject("discount").getDouble("cash") / 100));
                                salePrice.Text.set(doubleCoverHalfUp(resData.getDouble("bisCash") / 100));

                                okBtnClick(null);
                            } catch (Exception e) {
                                scanFlag = false;
                                e.printStackTrace();
                                alert(GetExceptionMsg(e));
                            }
                        }

                        @Override
                        public void onError(Object sender, Exception e) {

                        }
                    };
                    mHandler.sendMessage(msg);
                }

                @Override
                public void onError(Object sender, Exception e) {
                    scanFlag = false;
                    e.printStackTrace();
                    alert(GetExceptionMsg(e));
                }
            });
        } catch (Exception e) {
            scanFlag = false;
            e.printStackTrace();
            alert(GetExceptionMsg(e));
        }
    }

    //刷卡事件
    public void brushCard(String cardNo) {
        try {
            if (scanFlag || (paymentDialog != null && paymentDialog.isShowing())){
                return;
            }

            exitflag = true;
            PublicDefine.webService.getMemberInfoList(cardNo, new IAsynCallBackListener() {
                @Override
                public void onFinish(Object sender, Object data) {
                    final String resStr = (String) data;
                    Message msg = new Message();
                    msg.obj = new IAsynCallBackListener() {
                        @Override
                        public void onFinish(Object sender, Object data) {
                            try {
                                JSONObject res = new JSONObject(resStr);
                                if (!res.getBoolean("success")) {
                                    throw new Exception(res.getString("message"));
                                }
                                JSONArray records = res.getJSONObject("result").getJSONArray("records");
                                if (records.length() == 0) {
                                    throw new Exception("卡号未绑定会员");
                                }
                                memberInfo = records.getJSONObject(0);

                                JSONArray goodsArray = new JSONArray();
                                JSONObject goodsInfo = new JSONObject();
                                goodsInfo.put("goodsId", mainViewModel.selectGoods.getString("goodsId"));
                                goodsInfo.put("amount", 1);
                                goodsInfo.put("dosing", new JSONArray());
                                goodsArray.put(goodsInfo);

                                paymentDialog = new Dialog_Member_Payment(getBaseContext(), memberInfo,goodsArray, new IAsynCallBackListener() {
                                    @Override
                                    public void onFinish(Object sender, Object data) {
                                        try {
                                            Map payCashInfo = (Map) sender;
                                            JSONObject payment = (JSONObject) data;
                                            startTimeOut(30);
                                            if (payment.getInt("id") == 0) {
                                                payType = PayType.PAYMENT_BALANCE;
                                            } else {
                                                storedCard = payment;
                                                payType = PayType.PAYMENT_STORECARD;
                                            }
                                            salePrice.Text.set((String)payCashInfo.get("salePrice"));
                                            discountPrice.Text.set((String)payCashInfo.get("discountPrice"));
                                            okBtnClick(null);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            alert(GetExceptionMsg(e));
                                        }
                                    }

                                    @Override
                                    public void onError(Object sender, Exception e) {

                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                                alert(GetExceptionMsg(e));
                            }
                        }

                        @Override
                        public void onError(Object sender, Exception e) {

                        }
                    };
                    mHandler.sendMessage(msg);
                }

                @Override
                public void onError(Object sender, Exception e) {
                    e.printStackTrace();
                    alert(GetExceptionMsg(e));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            alert(GetExceptionMsg(e));
        }
    }

    //支付按钮
    public void okBtnClick(View view) {
        okBtn.Enabled.set(false);
        exitflag = true;
        try {
            //如果支付完成后完成订单异常,再次调用完成订单
            if (busiState == 1) {
                completeOrder();
                return;
            }
            //如果是支付完成后取货异常,则直接取货
            if (busiState == 2) {
                take();
                return;
            }

            JSONArray goodsArray = new JSONArray();
            JSONObject goodsInfo = new JSONObject();
            goodsInfo.put("goodsId", mainViewModel.selectGoods.getString("goodsId"));
            goodsInfo.put("amount", 1);
            goodsInfo.put("dosing", new JSONArray());
            goodsArray.put(goodsInfo);

            String memberId = null;
            String tel = null;
            if (memberInfo != null && payType != PayType.PAYMENT_WECHATMIR && payType != PayType.PAYMENT_ALIMIR) {
                memberId = memberInfo.getString("id");
                tel = memberInfo.getString("phone");
            }
            BigDecimal payCash = new BigDecimal(salePrice.Text.get()).multiply(new BigDecimal(100));
            BigDecimal discount = new BigDecimal(discountPrice.Text.get()).multiply(new BigDecimal(100));
            PublicDefine.webService.createSaleOrder(memberId, tel, goodsArray, payType.ordinal(),
                    payCash, discount, new BigDecimal(0), deviceInfo.getString("name"),
                    new IAsynCallBackListener() {
                        @Override
                        public void onFinish(Object sender, Object data) {
                            final String resStr = (String) data;
                            Message msg = new Message();
                            msg.obj = new IAsynCallBackListener() {
                                @Override
                                public void onFinish(Object sender, Object data) {
                                    try {

                                        JSONObject res = new JSONObject(resStr);
                                        if (!res.getBoolean("success")) {
                                            throw new Exception(res.getString("message"));
                                        }
                                        JSONObject orderInfo = res.getJSONObject("result");

                                        //支付
                                        pay(orderInfo);
                                    } catch (Exception e) {
                                        scanFlag = false;
                                        e.printStackTrace();
                                        alert(GetExceptionMsg(e));
                                        okBtn.Enabled.set(true);
                                    }
                                }

                                @Override
                                public void onError(Object sender, Exception e) {

                                }
                            };
                            mHandler.sendMessage(msg);
                        }

                        @Override
                        public void onError(Object sender, Exception e) {
                            scanFlag = false;
                            e.printStackTrace();
                            alert(GetExceptionMsg(e));
                            Message msg = new Message();
                            msg.obj = new IAsynCallBackListener() {
                                @Override
                                public void onFinish(Object sender, Object data) {
                                    okBtn.Enabled.set(true);
                                }

                                @Override
                                public void onError(Object sender, Exception e) {

                                }
                            };
                            mHandler.sendMessage(msg);
                        }
                    });


        } catch (Exception e) {
            scanFlag = false;
            e.printStackTrace();
            alert(GetExceptionMsg(e));
            okBtn.Enabled.set(true);
        }
    }

    //查询设备信息
    public void getDeviceInfo() throws Exception {
        PublicDefine.webService.getDeviceInfo(new IAsynCallBackListener() {
            @Override
            public void onFinish(Object sender, Object data) {
                final String resStr = (String) data;
                Message msg = new Message();
                msg.obj = new IAsynCallBackListener() {
                    @Override
                    public void onFinish(Object sender, Object data) {
                        try {
                            JSONObject res = new JSONObject(resStr);
                            if (!res.getBoolean("success")) {
                                throw new Exception(res.getString("message"));
                            }

                            deviceInfo = res.getJSONObject("result");

                        } catch (Exception e) {
                            e.printStackTrace();
                            alert(GetExceptionMsg(e));
                        }
                    }

                    @Override
                    public void onError(Object sender, Exception e) {
                    }
                };
                mHandler.sendMessage(msg);
            }

            @Override
            public void onError(Object sender, Exception e) {
                e.printStackTrace();
                alert(GetExceptionMsg(e));
            }
        });
    }

    //支付
    public void pay(final JSONObject orderInfo) throws Exception {
        String memberId = null;
        String tel = null;
        if (memberInfo != null && payType != PayType.PAYMENT_WECHATMIR && payType != PayType.PAYMENT_ALIMIR) {
            memberId = memberInfo.getString("id");
            tel = memberInfo.getString("phone");
        }


        JSONArray orders = new JSONArray();
        JSONObject order = new JSONObject();
        order.put("payAmount", orderInfo.getDouble("payAmount"));
        order.put("orderId", orderInfo.getString("id"));
        order.put("payType", orderInfo.getInt("payType"));
        order.put("phone", orderInfo.getString("memberPhone"));
        order.put("type", orderInfo.getInt("type"));
        orders.put(order);

        JSONArray storedCards = null;
        if (payType == PayType.PAYMENT_STORECARD) {
            storedCards = new JSONArray();
            storedCards.put(storedCard.getString("id"));
        }

        PublicDefine.webService.payV2(new BigDecimal(orderInfo.getDouble("payAmount")),
                payType.ordinal(), memberId, tel, orders, storedCards, new IAsynCallBackListener() {
                    @Override
                    public void onFinish(Object sender, Object data) {
                        final String resStr = (String) data;
                        Message msg = new Message();
                        msg.obj = new IAsynCallBackListener() {
                            @Override
                            public void onFinish(Object sender, Object data) {
                                try {
                                    JSONObject res = new JSONObject(resStr);
                                    if (!res.getBoolean("success")) {
                                        throw new Exception(res.getString("message"));
                                    }
                                    payOrderId = res.getJSONObject("result").getString("payOrderId");
                                    orderId = orderInfo.getString("id");
                                    if (payType == PayType.PAYMENT_WECHATMIR || payType == PayType.PAYMENT_ALIMIR) {
                                        //刷卡支付
                                        microPay();
                                    } else {
                                        //取货
                                        busiState = 2;
                                        take();
                                    }
                                } catch (Exception e) {
                                    scanFlag = false;
                                    e.printStackTrace();
                                    alert(GetExceptionMsg(e));
                                    okBtn.Enabled.set(true);
                                }
                            }

                            @Override
                            public void onError(Object sender, Exception e) {

                            }
                        };
                        mHandler.sendMessage(msg);
                    }

                    @Override
                    public void onError(Object sender, Exception e) {
                        scanFlag = false;
                        Message msg = new Message();
                        msg.obj = new IAsynCallBackListener() {
                            @Override
                            public void onFinish(Object sender, Object data) {
                                okBtn.Enabled.set(true);
                            }

                            @Override
                            public void onError(Object sender, Exception e) {

                            }
                        };
                        mHandler.sendMessage(msg);
                    }
                });
    }

    //取货
    public void take() {
        try {
            PublicDefine.webService.ticketMessage(orderId, new IAsynCallBackListener() {
                @Override
                public void onFinish(Object sender, Object data) {
                    final String resStr = (String) data;
                    Message msg = new Message();
                    msg.obj = new IAsynCallBackListener() {
                        @Override
                        public void onFinish(Object sender, Object data) {
                            try {
                                JSONObject res = new JSONObject(resStr);
                                if (!res.getBoolean("success")) {
                                    throw new Exception(res.getString("message"));
                                }
                                JSONObject result = res.getJSONObject("result");
                                JSONObject order = result.getJSONObject("order");
                                String takeCode = order.getString("printNo");
                                //取货
                                PublicDefine.webService.take(takeCode, deviceInfo.getString("storageId"), deviceInfo.getString("name"),
                                        new IAsynCallBackListener() {
                                            @Override
                                            public void onFinish(Object sender, Object data) {
                                                final String resStr = (String) data;
                                                Message msg = new Message();
                                                msg.obj = new IAsynCallBackListener() {
                                                    @Override
                                                    public void onFinish(Object sender, Object data) {
                                                        try {
                                                            JSONObject res = new JSONObject(resStr);
                                                            if (!res.getBoolean("success")) {
                                                                throw new Exception(res.getString("message"));
                                                            }
                                                            busiState = 3;

                                                            //跳转支付完成
                                                            viewPager.setCurrentItem(12);
                                                            scanFlag = false;
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                            alert(GetExceptionMsg(e));
                                                            okBtn.Enabled.set(true);
                                                            okBtn.Text.set("重试");
                                                        }
                                                    }

                                                    @Override
                                                    public void onError(Object sender, Exception e) {

                                                    }
                                                };
                                                mHandler.sendMessage(msg);
                                            }

                                            @Override
                                            public void onError(Object sender, Exception e) {
                                                e.printStackTrace();
                                                alert(GetExceptionMsg(e));
                                                Message msg = new Message();
                                                msg.obj = new IAsynCallBackListener() {
                                                    @Override
                                                    public void onFinish(Object sender, Object data) {
                                                        okBtn.Enabled.set(true);
                                                        okBtn.Text.set("重试");
                                                    }

                                                    @Override
                                                    public void onError(Object sender, Exception e) {

                                                    }
                                                };
                                                mHandler.sendMessage(msg);
                                            }
                                        });
                            } catch (Exception e) {
                                e.printStackTrace();
                                alert(GetExceptionMsg(e));
                                okBtn.Enabled.set(true);
                                okBtn.Text.set("重试");
                            }
                        }

                        @Override
                        public void onError(Object sender, Exception e) {

                        }
                    };
                    mHandler.sendMessage(msg);
                }

                @Override
                public void onError(Object sender, Exception e) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            alert(GetExceptionMsg(e));
            okBtn.Enabled.set(true);
            okBtn.Text.set("重试");
        }
    }

    //刷卡支付
    public void microPay() {
        try {
            PublicDefine.webService.microPay(orderId, payOrderId, authCode, new IAsynCallBackListener() {
                @Override
                public void onFinish(Object sender, Object data) {
                    try {
                        String resStr = (String) data;
                        JSONObject res = new JSONObject(resStr);
                        if (!res.getBoolean("success")) {
                            throw new Exception(res.getString("message"));
                        }
                        JSONObject result = res.getJSONObject("result");

                        //如果已完成直接调用业务完成接口,未完成进行循环查单
                        int code = result.getInt("code");
                        switch (code){
                            case 0:
                                completeOrder();
                                break;
                            case 1:
                                outTime = 60*5;
                                searchExitflag = false;
                                searchPayThread = new Thread(searchPayRunable);
                                searchPayThread.start();
                                break;
                            case 2:
                            case -1:
                                throw new Exception(result.getString("msg"));
                            default:
                                throw new Exception("未知错误");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        alert(GetExceptionMsg(e));
                    }
                }

                @Override
                public void onError(Object sender, Exception e) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            alert(GetExceptionMsg(e));
        }
    }

    //查单线程
    Runnable searchPayRunable = new Runnable() {
        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            while (!searchExitflag) {
                try {
                    if (outTime == 0) {
                        searchExitflag = true;
                        return;
                    }

                    String resStr = PublicDefine.webService.searchPay(payOrderId);
                    JSONObject res = new JSONObject(resStr);
                    if (!res.getBoolean("success")) {
                        throw new Exception(res.getString("message"));
                    }
                    JSONObject result = res.getJSONObject("result");
                    int code = result.getInt("code");
                    switch (code){
                        case 0:
                            searchExitflag = true;
                            completeOrder();
                            break;
                        case 2:
                        case -1:
                            searchExitflag = true;
                            throw new Exception(result.getString("msg"));
                        default:
                            break;
                    }
                    Thread.sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                    alert(GetExceptionMsg(e));
                }
            }
        }
    };

    //完成订单
    public void completeOrder() {
        try {
            busiState = 1;
            PublicDefine.webService.completeOrder(orderId, payOrderId, new IAsynCallBackListener() {
                @Override
                public void onFinish(Object sender, Object data) {
                    try {
                        String resStr = (String) data;
                        JSONObject res = new JSONObject(resStr);
                        if (!res.getBoolean("success")) {
                            throw new Exception(res.getString("message"));
                        }

                        //取货
                        take();
                    } catch (Exception e) {
                        e.printStackTrace();
                        alert(GetExceptionMsg(e));
                        okBtn.Enabled.set(true);
                        okBtn.Text.set("重试");
                    }
                }

                @Override
                public void onError(Object sender, Exception e) {
                    e.printStackTrace();
                    alert(GetExceptionMsg(e));
                    okBtn.Enabled.set(true);
                    okBtn.Text.set("重试");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            alert(GetExceptionMsg(e));
            okBtn.Enabled.set(true);
            okBtn.Text.set("重试");
        }
    }
}
