package ViewModel;

import android.content.Context;
import android.content.ContextWrapper;
import android.databinding.BaseObservable;
import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administrator.drinkdevice.R;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import BaseActivity.BaseActivity;
import Const.PublicDefine;
import Helper.Log;
import Interface.IAsynCallBackListener;
import Service.SysService;

/**
 * Created by 王彦鹏 on 2018-03-16.
 */

public class Base_ViewModel extends ContextWrapper {

    public Base_ViewModel(Context base) {
        super(base);
    }

    //获取异常原因
    protected String GetExceptionMsg(Throwable e) {
        if (e.getCause() != null) {
            return GetExceptionMsg(e.getCause());
        } else {
            return e.getMessage();
        }
    }

    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.obj instanceof IAsynCallBackListener) {
                ((IAsynCallBackListener) msg.obj).onFinish("", msg.arg1);
            }
        }
    };

    public void alert(final String info) {
        Message msg = new Message();
        msg.obj = new IAsynCallBackListener() {
            @Override
            public void onFinish(Object sender, Object data) {
                View toastRoot = ((BaseActivity)getBaseContext()).getLayoutInflater().inflate(R.layout.my_toast, null);
                Toast toast=new Toast(getApplicationContext());
                toast.setView(toastRoot);
                TextView tv=(TextView)toastRoot.findViewById(R.id.TextViewInfo);
                tv.setText(info);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

            }

            @Override
            public void onError(Object sender, Exception e) {

            }
        };
        mHandler.sendMessage(msg);
    }

    private IAsynCallBackListener callBackListener;
    protected int TimeOutClose = 30;//自动关闭
    private Handler MsgHandle = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {
                callBackListener.onFinish("", TimeOutClose);
                if (TimeOutClose <= 0) {
                    removeMessages(1);
                } else {
                    TimeOutClose--;
                    MsgHandle.sendEmptyMessageDelayed(1, 1000);
                }
            } else {
                removeMessages(1);
            }
        }
    };

    protected void StartAutoClose(int times, IAsynCallBackListener callBackListener) {
        TimeOutClose = times;
        this.callBackListener = callBackListener;
        Message msg = new Message();
        msg.what = 1;
        MsgHandle.sendMessage(msg);
    }

    protected void StopAutoClose() {
        MsgHandle.sendEmptyMessage(2);
    }
}
