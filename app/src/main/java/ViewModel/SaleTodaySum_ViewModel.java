package ViewModel;

import android.content.Context;
import android.os.Message;
import android.view.View;
import android.widget.LinearLayout;

import com.example.administrator.drinkdevice.MainActivity;
import com.example.administrator.drinkdevice.R;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.math.BigDecimal;

import Component.CustomViewPager;
import Const.PublicDefine;
import Interface.IAsynCallBackListener;
import Service.SysService;
import views.View_Member_Payment;
import views.View_SaleTodaySum_GoodsInfo;

public class SaleTodaySum_ViewModel extends Base_ViewModel{
    private CustomViewPager viewPager;
    private MainActivity_ViewModel mainViewModel;
    private LinearLayout goodsList;
    public TextBind numView = new TextBind("0");
    public TextBind priceView = new TextBind("0.00");

    public SaleTodaySum_ViewModel(Context ctx, CustomViewPager viewPager, MainActivity_ViewModel mainViewModel) {
        super(ctx);
        this.viewPager = viewPager;
        this.mainViewModel = mainViewModel;
    }

    public void Init() {
        try {
            //获取卖品售卖列表
            PublicDefine.webService.getDeviceSaleSum(new IAsynCallBackListener() {
                @Override
                public void onFinish(Object sender, Object data) {
                    final String resStr = (String) data;
                    Message msg = new Message();
                    msg.obj = new IAsynCallBackListener() {
                        @Override
                        public void onFinish(Object sender, Object data) {
                            try {
                                JSONObject res = new JSONObject(resStr);
                                if (!res.getBoolean("success")) {
                                    throw new Exception(res.getString("message"));
                                }
                                JSONArray goodsArray = res.getJSONObject("result").getJSONArray("records");

                                goodsList = ((MainActivity)getBaseContext()).findViewById(R.id.goodsList);
                                goodsList.removeAllViews();
                                int amount = 0;
                                double price = 0.00;
                                for (int i = 0; i < goodsArray.length(); i++) {
                                    JSONObject goodsInfo = goodsArray.getJSONObject(i);
                                    View_SaleTodaySum_GoodsInfo goodsInfoView = new View_SaleTodaySum_GoodsInfo(getBaseContext(),goodsInfo);
                                    goodsList.addView(goodsInfoView);
                                    amount += goodsInfo.getInt("saleAmount");
                                    price += goodsInfo.getDouble("salePrice") + goodsInfo.getDouble("memberPrice");
                                }
                                numView.Text.set(amount+"");
                                priceView.Text.set(new BigDecimal(price).setScale(2, BigDecimal.ROUND_HALF_UP)+"");
                            } catch (Exception e) {
                                e.printStackTrace();
                                alert(e.getMessage());
                            }
                        }

                        @Override
                        public void onError(Object sender, Exception e) {

                        }
                    };
                    mHandler.sendMessage(msg);
                }

                @Override
                public void onError(Object sender, Exception e) {

                }
            });
        } catch (Exception e){
            e.printStackTrace();
            alert(GetExceptionMsg(e));
        }
    }


    public void cancelBtnClick(View view) {
        viewPager.setCurrentItem(2);
    }

}
