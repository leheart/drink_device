package ViewModel;

import android.content.Context;
import android.os.Message;
import android.view.View;
import android.widget.ArrayAdapter;

import com.example.administrator.drinkdevice.R;

import org.json.JSONObject;
import org.w3c.dom.Text;

import Component.CustomViewPager;
import Const.PublicDefine;
import Interface.IAsynCallBackListener;
import Service.SysService;

public class CheckDevice_ViewModel extends Base_ViewModel{
    private CustomViewPager viewPager;
    private MainActivity_ViewModel mainViewModel;

    public TextBind deviceNameView = new TextBind("检测中...");
    public TextBind deviceIdView = new TextBind("检测中...");
    public TextBind netStateView = new TextBind("检测中...");
    public TextBind cardReadStateView = new TextBind("检测中...");
    public TextBind scanStateView = new TextBind("检测中...");
    public TextBind printStateView = new TextBind("检测中...");

    public CheckDevice_ViewModel(Context ctx, CustomViewPager viewPager, MainActivity_ViewModel mainViewModel) {
        super(ctx);
        this.viewPager = viewPager;
        this.mainViewModel = mainViewModel;
    }

    public void Init() {
        try {
            //设备Id
            deviceIdView.Text.set(String.format("%08X", SysService.DeviceID));

            //获取设备信息设置设备名称
            PublicDefine.webService.getDeviceInfo(new IAsynCallBackListener() {
                @Override
                public void onFinish(Object sender, Object data) {
                    final String resStr = (String) data;
                    Message msg = new Message();
                    msg.obj = new IAsynCallBackListener() {
                        @Override
                        public void onFinish(Object sender, Object data) {
                            try {
                                JSONObject res = new JSONObject(resStr);
                                if (!res.getBoolean("success")){
                                    throw new Exception(res.getString("message"));
                                }

                                deviceNameView.Text.set(res.getJSONObject("result").getString("name"));
                            } catch (Exception e){
                                e.printStackTrace();
                                alert(GetExceptionMsg(e));
                            }
                        }

                        @Override
                        public void onError(Object sender, Exception e) {

                        }
                    };
                    mHandler.sendMessage(msg);
                }

                @Override
                public void onError(Object sender, Exception e) {

                }
            });

            //网络状态
            netStateView.Text.set(PublicDefine.NetworkConnState ? "网通" : "不通");

            //读卡器扫描枪状态
            if (PublicDefine.mp86Control.Check_Device_Is_OK() == 0){
                cardReadStateView.Text.set("正常");
                scanStateView.Text.set("正常");
            } else {
                cardReadStateView.Text.set("不正常");
                scanStateView.Text.set("不正常");
            }


            //打印机
            if (PublicDefine.printerControl.isOpen){
                int state = PublicDefine.printerControl.GetStatus();
                switch (state){
                    case 0:
                        printStateView.Text.set("正常");
                        break;
                    case 2:
                        printStateView.Text.set("调用库不匹配");
                        break;
                    case 3:
                        printStateView.Text.set("打印头打开");
                        break;
                    case 4:
                        printStateView.Text.set("切刀未复位");
                        break;
                    case 5:
                        printStateView.Text.set("打印头过热");
                        break;
                    case 6:
                        printStateView.Text.set("黑标错误");
                        break;
                    case 7:
                        printStateView.Text.set("纸尽");
                        break;
                    case 8:
                        printStateView.Text.set("纸将尽");
                        break;
                    case 1:
                    default:
                        printStateView.Text.set("未接");
                        break;
                }
            } else {
                printStateView.Text.set("未接");
            }

        } catch (Exception e){
            e.printStackTrace();
            alert(GetExceptionMsg(e));
        }
    }

    public void cancelBtnClick(View view) {
        viewPager.setCurrentItem(2);
    }


}
