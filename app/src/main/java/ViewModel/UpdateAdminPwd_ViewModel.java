package ViewModel;

import android.content.Context;
import android.os.Message;
import android.view.View;

import org.json.JSONObject;

import Component.CustomViewPager;
import Const.PublicDefine;
import Interface.IAsynCallBackListener;
import Service.SysService;

public class UpdateAdminPwd_ViewModel extends Base_ViewModel{
    private CustomViewPager viewPager;
    private MainActivity_ViewModel mainViewModel;

    public EditBind newPwd = new EditBind();
    public EditBind checkPwd = new EditBind();

    public UpdateAdminPwd_ViewModel(Context ctx, CustomViewPager viewPager, MainActivity_ViewModel mainViewModel) {
        super(ctx);
        this.viewPager = viewPager;
        this.mainViewModel = mainViewModel;
    }

    public void Init() {
        try {
            newPwd.TextValue.set("");
            checkPwd.TextValue.set("");
        } catch (Exception e){
            e.printStackTrace();
            alert(GetExceptionMsg(e));
        }
    }

    public void okBtnClick(View view) {
        try {
            if (newPwd.TextValue.get().equals("")) {
                alert("密码不能为空");
                return;
            }
            if (newPwd.TextValue.get().length() < 8 || newPwd.TextValue.get().length() > 16){
                alert("密码由8-16字母、数字或字符组成");
                return;
            }
            if (!checkPwd.TextValue.get().equals(newPwd.TextValue.get())) {
                alert("两次密码不一致");
                return;
            }
            PublicDefine.optionData.SetOption("adminPwd", "svalue", newPwd.TextValue.get());
            alert("修改成功");
        } catch (Exception e){
            e.printStackTrace();
            alert(GetExceptionMsg(e));
        }
    }

    public void cancelBtnClick(View view) {
        viewPager.setCurrentItem(2);
    }


}
