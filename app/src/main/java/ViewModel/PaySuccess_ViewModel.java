package ViewModel;

import android.content.Context;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.example.administrator.drinkdevice.MainActivity;
import com.example.administrator.drinkdevice.R;

import org.json.JSONArray;
import org.json.JSONObject;

import Component.CustomViewPager;
import Const.PublicDefine;
import DataAccess.DeviceDataAccess;
import Interface.IAsynCallBackListener;

public class PaySuccess_ViewModel extends Base_ViewModel {

    private CustomViewPager viewPager;
    private MainActivity_ViewModel mainViewModel;

    public TextBind timeout = new TextBind("");
    public TextBind goodsInfo = new TextBind("");

    private Thread getDeviceStateThread;
    private boolean exitflag = false;
    private int outTime = 3;


    public PaySuccess_ViewModel(Context ctx, CustomViewPager viewPager, MainActivity_ViewModel mainViewModel) {
        super(ctx);
        this.viewPager = viewPager;
        this.mainViewModel = mainViewModel;
    }

    public void Init() {
        try {
            outTime = 3;

            goodsInfo.Text.set(mainViewModel.selectGoods.getString("goodsName") + "*1");

            exitflag = false;
            getDeviceStateThread = new Thread(runnableUi);
            getDeviceStateThread.start();
        } catch (Exception e) {
            e.printStackTrace();
            alert(GetExceptionMsg(e));
        }
    }

    Runnable runnableUi = new Runnable() {
        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            while (!exitflag) {
                try {
                    timeout.Text.set(outTime + "s");
                    outTime--;
                    if (outTime < 0) {
                        Message msg = new Message();
                        msg.obj = new IAsynCallBackListener() {
                            @Override
                            public void onFinish(Object sender, Object data) {
                                mainViewModel.takeCupFlag = 1;
                                viewPager.setCurrentItem(0);
                            }

                            @Override
                            public void onError(Object sender, Exception e) {

                            }
                        };
                        mHandler.sendMessage(msg);
                        exitflag = true;
                        return;
                    }
                    Thread.sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                    alert(GetExceptionMsg(e));
                }
            }
        }
    };
}

