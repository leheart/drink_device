package ViewModel;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import com.example.administrator.drinkdevice.R;
import com.example.administrator.drinkdevice.databinding.SqllitemanageDialogBinding;

import java.util.List;

import DataAccess.SqlLiteDataAccess;
import Helper.Msgbox;
import Class.JsCallInterface;

/**
 * Created by 王彦鹏 on 2018-01-19.
 */

public class SqlliteManage_ViewModel extends Base_ViewModel {
    public TextBind Btn_Close=new TextBind("×");

    private AlertDialog dialog;
    private Context ctx;

    private LinearLayout tablelist;
    private SqlLiteDataAccess sqllite;
    private WebView webView;
    private JsCallInterface jsCall;

    public SqlliteManage_ViewModel(Context ctx)
    {
        super(ctx);
        this.ctx=ctx;
        sqllite=new SqlLiteDataAccess(ctx);
    }
    public void Show() throws Exception {
        dialog = Msgbox.ShowDialog(ctx, R.layout.sqllitemanage_dialog);
        dialog.show();

        LayoutInflater inflater = LayoutInflater.from(this);
        SqllitemanageDialogBinding sqllitemanageDialogBinding =SqllitemanageDialogBinding.inflate(inflater);

        sqllitemanageDialogBinding.setViewModel(this);
        dialog.getWindow().setContentView(sqllitemanageDialogBinding.getRoot());

        Init();
    }

    private void LoadTableList() throws Exception {
        String heml="<html>\n" +
                "<head>    \n" +
                "    <style type=\"text/css\">    table.gridtable {\n" +
                "        font-family: verdana,arial,sans-serif;font-size:11px;color:#333333;border-width: 1px;border-color: #666666;border-collapse: collapse;\n" +
                "    }\n" +
                "    table.gridtable th {border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #dedede;\n" +
                "    }\n" +
                "    table.gridtable td {border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;\n" +
                "    }\n" +
                "    </style>\n" +
                "    <script type=\"text/javascript\">\n" +
                "        function delrow(row)\n" +
                "        {\n" +
                "            var res=window.external.deleterow();\n" +
                "        }\n" +
                "        function deltable(tab)\n" +
                "        {\n" +
                "            var res=window.external.deleteTable(tab);\n" +
                "        }\n" +
                "       function settablelist(list){ \n"+
                "           document.getElementById('tablist').innerHTML=list; \n"+
                "           }\n"+
                "       function showData(tab){\n"+
                "           var res=window.external.getTable(tab.innerText);\n"+
                "           document.getElementById('tabledata').innerHTML=res; \n"+
                "       }\n"+
                "    </script>\n" +
                "</head>\n" +
                "<body>\n" +
                "<div id='tablist' style=\"position: fixed;top:0px;height: 40px;border: solid 1px #666666;width: 98%;background: #eeeeee;\">\n" +
                "    \n" +
                "</div>\n" +
                "<div id='tabledata' style=\"margin-top: 48px;width:100%;\">\n" +

                "</div>\n" +
                "</body>\n" +
                "</html>";
        webView.loadDataWithBaseURL(null,heml,"text/html" , "utf-8",null);

    }

    public void Btn_Close_OnClick(View view)
    {
        dialog.dismiss();
    }
    private void Init() throws Exception {

        webView= dialog.findViewById(R.id.web_tabdata);
        jsCall = new JsCallInterface(ctx,webView);
        webView.addJavascriptInterface(jsCall, "external");
        WebSettings webSettings = webView.getSettings();
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webSettings.setSupportZoom(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setAppCacheMaxSize(1024*1024*8);
        webSettings.setAllowFileAccess(true);
        webSettings.setAppCacheEnabled(true);


        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);

        webSettings.setSupportMultipleWindows(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
                return Msgbox.JSAlert(ctx,message,result);
            }

            @Override
            public void onCloseWindow(WebView window) {
                //TODO something
                super.onCloseWindow(window);
            }
        });

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url)
            {
                //获取所有表名

                List<String> tablist= null;
                try {
                    tablist = sqllite.queryTable();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String htmstr="";
                for (int i = 0; i < tablist.size(); i++) {
                    htmstr+=String.format("<button onclick=\"return showData(this);\" style=\"margin:10px 10px; \">%s</button>",tablist.get(i),tablist.get(i));

                }
                htmstr+=String.format("<button onclick=\"return showData(this);\" style=\"margin:10px 10px; \">%s</button>",
                        ctx.getResources().getString(R.string.SqlliteManage_ViewModel_RestoreTheFactory));
                jsCall.CallJs("settablelist",htmstr);
            }
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                    view.loadUrl(url);

                return true;

            }
        });
        LoadTableList();

    }
}
