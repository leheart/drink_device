package ViewModel;

import android.content.Context;
import android.os.Message;
import android.view.View;

import com.example.administrator.drinkdevice.MainActivity;
import com.example.administrator.drinkdevice.SystemInitActivity;

import org.json.JSONObject;

import Const.PublicDefine;
import Interface.IAsynCallBackListener;
import Service.SysService;

public class SystemInit_ViewModel extends Base_ViewModel{

    public EditBind serverHost = new EditBind();
    public EditBind cinemaCode = new EditBind();

    public SystemInit_ViewModel(Context ctx) {
        super(ctx);
    }

    public void Init() {
        try {
            //初始化设备信息
            SysService.ReadSysInfo(getBaseContext());

            JSONObject serverHostJson = PublicDefine.optionData.GetOption("serverHost");
            if (serverHostJson != null) {
                serverHost.TextValue.set(serverHostJson.getString("svalue"));
            } else {
                serverHost.TextValue.set("");
            }

            JSONObject cinemaIdJson = PublicDefine.optionData.GetOption("cinemaCode");
            if (cinemaIdJson != null) {
                cinemaCode.TextValue.set(cinemaIdJson.getString("svalue"));
            } else {
                cinemaCode.TextValue.set("");
            }

            if (!serverHost.TextValue.get().equals("") && !cinemaCode.TextValue.get().equals("")){
                okBtnClick(null);
            }
        } catch (Exception e){
            e.printStackTrace();
            alert(GetExceptionMsg(e));
        }
    }

    public void okBtnClick(View view) {
        try {
            if (serverHost.TextValue.get().equals("")) {
                alert("服务器地址非法");
                return;
            }
            if (cinemaCode.TextValue.get().equals("")) {
                alert("门店编码不能为空");
                return;
            }
            PublicDefine.optionData.SetOption("serverHost", "svalue", serverHost.TextValue.get());
            PublicDefine.optionData.SetOption("cinemaCode", "svalue", cinemaCode.TextValue.get());

            PublicDefine.serviceUrl = serverHost.TextValue.get();
            //上传状态
            PublicDefine.webService.PostState(cinemaCode.TextValue.get(),new IAsynCallBackListener() {
                @Override
                public void onFinish(Object sender, Object data) {
                    final String resStr = (String) data;
                    Message msg = new Message();
                    msg.obj = new IAsynCallBackListener() {
                        @Override
                        public void onFinish(Object sender, Object data) {
                            try {
                                JSONObject res = new JSONObject(resStr);
                                if (!res.getBoolean("success")){
                                    throw new Exception(res.getString("message"));
                                }
                                JSONObject resData = res.getJSONObject("result");

                                PublicDefine.cinemaInfo = resData;

                                ((SystemInitActivity)getBaseContext()).StartActivityForResult(MainActivity.class);
                                ((SystemInitActivity)getBaseContext()).finish();
                            } catch (Exception e){
                                e.printStackTrace();
                                alert(GetExceptionMsg(e));
                            }
                        }

                        @Override
                        public void onError(Object sender, Exception e) {

                        }
                    };
                    mHandler.sendMessage(msg);
                }

                @Override
                public void onError(Object sender, Exception e) {
                    e.printStackTrace();
                    alert(GetExceptionMsg(e));
                }
            });
        } catch (Exception e){
            e.printStackTrace();
            alert(GetExceptionMsg(e));
        }
    }




}
