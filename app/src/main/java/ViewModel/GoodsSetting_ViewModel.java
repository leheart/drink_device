package ViewModel;

import android.content.Context;
import android.os.Message;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.administrator.drinkdevice.MainActivity;
import com.example.administrator.drinkdevice.R;

import org.json.JSONArray;
import org.json.JSONObject;

import Component.CustomViewPager;
import Const.PublicDefine;
import DataAccess.DeviceDataAccess;
import DataAccess.OptionDataAccess;
import Device.colaMixing.mixing_control;
import Device.colaMixing.param_Constant;
import Helper.Log;
import Interface.IAsynCallBackListener;
import views.View_GoodsSetting_DeviceInfo;


public class GoodsSetting_ViewModel extends Base_ViewModel {

    private CustomViewPager viewPager;
    private MainActivity_ViewModel mainViewModel;
    private LinearLayout deviceListLinear;
    private DeviceDataAccess deviceDataAccess;

    public GoodsSetting_ViewModel(Context ctx, CustomViewPager viewPager, MainActivity_ViewModel mainViewModel) {
        super(ctx);
        this.viewPager = viewPager;
        this.mainViewModel = mainViewModel;
        deviceDataAccess = new DeviceDataAccess(ctx);
    }

    public void Init() {
        try {
            deviceListLinear = ((MainActivity) getBaseContext()).findViewById(R.id.deviceListLinear);
            deviceListLinear.removeAllViews();

            //获取所有分级信息
            int ret = PublicDefine.colaReader.getAllSlaveDeviceStatus();
            if (ret != 0) {
                throw new Exception("获取全部分机信息失败[" + ret + "]");
            }

            PublicDefine.webService.getBarList(new IAsynCallBackListener() {
                @Override
                public void onFinish(Object sender, Object data) {
                    final String resStr = (String) data;
                    Message msg = new Message();
                    msg.obj = new IAsynCallBackListener() {
                        @Override
                        public void onFinish(Object sender, Object data) {
                            try {
                                JSONObject res = new JSONObject(resStr);
                                if (!res.getBoolean("success")) {
                                    throw new Exception(res.getString("message"));
                                }
                                JSONObject resData = res.getJSONObject("result");
                                JSONArray goodsList = resData.getJSONArray("records");


                                for (int i = 0; i < PublicDefine.colaReader.slave_DeviceInfo.total_device_number; i++) {
                                    String deviceId = PublicDefine.colaReader.slave_DeviceInfo.dev_id[i];
                                    boolean online = (PublicDefine.colaReader.slave_DeviceInfo.Config_Flag[i] & param_Constant.CONFIG_ONLINE_BIT)
                                            == param_Constant.CONFIG_ONLINE_BIT;
                                    String onlineStr = online ? "在线" : "离线";


                                    //更新卖品信息
                                    JSONObject deviceInfo = deviceDataAccess.getById(deviceId);
                                    if (deviceInfo != null) {
                                        JSONArray goodsArray = deviceInfo.getJSONArray("goodsList");
                                        for (int j = goodsArray.length() - 1; j >= 0; j--) {
                                            JSONObject goodsInfo = goodsArray.getJSONObject(j);
                                            boolean hasGoods = false;
                                            for (int k = 0; k < goodsList.length(); k++) {
                                                JSONObject goodsJson = goodsList.getJSONObject(k);
                                                if (goodsInfo.getInt("goodsId") == goodsJson.getInt("goodsId")) {
                                                    hasGoods = true;
                                                    JSONObject pickTime = null;
                                                    if (goodsInfo.has("pickTime")) {
                                                        pickTime = goodsInfo.getJSONObject("pickTime");
                                                    }
                                                    goodsInfo = goodsJson;
                                                    if (pickTime != null){
                                                        goodsInfo.put("pickTime", pickTime);
                                                    }
                                                    goodsArray.put(j, goodsInfo);
                                                    break;
                                                }
                                            }
                                            if (!hasGoods) {
                                                goodsArray.remove(j);
                                            }
                                        }
                                        deviceInfo.put("goodsList", goodsArray);
                                        deviceDataAccess.setDevice(deviceInfo);
                                    }


                                    View_GoodsSetting_DeviceInfo deviceView = new View_GoodsSetting_DeviceInfo(getBaseContext(), deviceId, onlineStr, new IAsynCallBackListener() {
                                        @Override
                                        public void onFinish(Object sender, Object data) {
                                            mainViewModel.pickDevice = (JSONObject) data;
                                            viewPager.setCurrentItem(5);
                                        }

                                        @Override
                                        public void onError(Object sender, Exception e) {

                                        }
                                    });
                                    deviceListLinear.addView(deviceView);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                alert(GetExceptionMsg(e));
                            }
                        }

                        @Override
                        public void onError(Object sender, Exception e) {

                        }
                    };
                    mHandler.sendMessage(msg);
                }

                @Override
                public void onError(Object sender, Exception e) {
                    e.printStackTrace();
                    alert(GetExceptionMsg(e));
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            alert(GetExceptionMsg(e));
        }
    }

    public void goBackClick(View view) {
        viewPager.setCurrentItem(2);
    }
}
