package ViewModel;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Message;
import android.view.View;
import android.widget.ProgressBar;

import com.example.administrator.drinkdevice.BuildConfig;
import com.example.administrator.drinkdevice.R;

import org.json.JSONObject;

import Component.CustomViewPager;
import Const.PublicDefine;
import Enums.MsgType;
import Helper.App;
import Helper.Msgbox;
import Interface.IAsynCallBackListener;
import Service.SysService;
import ServiceTask.IDownProgress;
import ServiceTask.UpdateManager;

public class CheckUpdate_ViewModel extends Base_ViewModel{
    private CustomViewPager viewPager;
    private MainActivity_ViewModel mainViewModel;

    public TextBind sysVer = new TextBind("");

    String filename = "";

    public CheckUpdate_ViewModel(Context ctx, CustomViewPager viewPager, MainActivity_ViewModel mainViewModel) {
        super(ctx);
        this.viewPager = viewPager;
        this.mainViewModel = mainViewModel;
    }

    public void Init() {
        try {
            sysVer.Text.set("V"+SysService.SoftVersion);
        } catch (Exception e){
            e.printStackTrace();
            alert(GetExceptionMsg(e));
        }
    }

    public void okBtnClick(final View view) {
        try {
            view.setEnabled(false);
            String appName = App.getAppName(this);
            int appVer = App.getVersionCode(this);
            PublicDefine.webService.UpdateVerify(appName, appVer, BuildConfig.VERSION_NAME,
                    new IAsynCallBackListener() {
                        @Override
                        public void onFinish(Object sender, Object data) {
                            final JSONObject jsonObject = (JSONObject) data;
                            Message msg = new Message();
                            msg.obj = new IAsynCallBackListener() {
                                @Override
                                public void onFinish(Object sender, Object data) {
                                    try {

                                        if(jsonObject.getInt("Result")==0){
                                            final AlertDialog dialog= Msgbox.ShowDialog(getBaseContext(), R.layout.update_dialog);
                                            dialog.findViewById(R.id.btn_update).setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    dialog.dismiss();
                                                    final AlertDialog dialog = Msgbox.ShowDialog(getBaseContext(), R.layout.progressdialog_view);
                                                    Message msg = new Message();
                                                    msg.arg1=1;
                                                    msg.obj = new IAsynCallBackListener() {
                                                        @Override
                                                        public void onFinish(Object sender, Object data) {
                                                            try {
                                                                UpdateManager.DownFile(getBaseContext(), jsonObject.getJSONObject("JsonData").getString("DownUrl"),
                                                                        true, new IDownProgress() {
                                                                            @Override
                                                                            public void DownProgress(int fileSize, int downLoad, String localFileName) {
                                                                                ProgressBar progressBar = dialog.findViewById(R.id.update_progress);
                                                                                progressBar.setMax(fileSize);
                                                                                progressBar.setProgress(downLoad);
                                                                                filename = localFileName;
                                                                                if(downLoad >= fileSize){
                                                                                    dialog.dismiss();
                                                                                }
                                                                            }
                                                                        });
                                                                view.setEnabled(true);
                                                            }catch (Exception e){

                                                            }
                                                        }

                                                        @Override
                                                        public void onError(Object sender, Exception e) {

                                                        }
                                                    };
                                                    dialog.setCanceledOnTouchOutside(false);
                                                    dialog.setCancelable(false);
                                                    mHandler.sendMessage(msg);
                                                    dialog.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            UpdateManager.DownCancel(filename);
                                                            dialog.dismiss();
                                                            view.setEnabled(true);
                                                        }
                                                    });
                                                }
                                            });
                                            dialog.setCanceledOnTouchOutside(false);
                                            dialog.setCancelable(false);
                                            dialog.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    dialog.dismiss();
                                                    view.setEnabled(true);
                                                }
                                            });
                                        }else {
                                            Msgbox.Show(getBaseContext(), "已是最新版本，不用升级。",
                                                    MsgType.msg_Hint, new IAsynCallBackListener() {
                                                        @Override
                                                        public void onFinish(Object sender, Object data) {

                                                        }

                                                        @Override
                                                        public void onError(Object sender, Exception e) {

                                                        }
                                                    });
                                            view.setEnabled(true);
                                        }
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onError(Object sender, Exception e) {

                                }
                            };
                            mHandler.sendMessage(msg);
                        }

                        @Override
                        public void onError(Object sender, Exception e) {

                        }
                    });
        } catch (Exception e){
            e.printStackTrace();
            alert(GetExceptionMsg(e));
        }
    }

    public void cancelBtnClick(View view) {
        viewPager.setCurrentItem(2);
    }


}
