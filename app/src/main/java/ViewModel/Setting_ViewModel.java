package ViewModel;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.example.administrator.drinkdevice.MainActivity;
import com.example.administrator.drinkdevice.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;

import Component.CustomViewPager;
import Helper.FlexBoxLayout;
import Interface.IAsynCallBackListener;
import views.BtnSetting;


public class Setting_ViewModel extends Base_ViewModel{

    private CustomViewPager viewPager;
    private MainActivity_ViewModel mainViewModel;
    private JSONArray settings = new JSONArray();
    private FlexBoxLayout flexBox;

    public Setting_ViewModel(Context ctx, CustomViewPager viewPager, MainActivity_ViewModel mainViewModel) {
        super(ctx);
        this.viewPager = viewPager;
        this.mainViewModel = mainViewModel;
        initSettings();
    }

    public void Init() {
        try {
            flexBox = ((MainActivity)getBaseContext()).findViewById(R.id.flexbox);
            if (flexBox.getChildCount() == 0){
                flexBox.removeAllViews();
                flexBox.setHorizontalSpace(84);
                flexBox.setVerticalSpace(62);
                for (int i = 0; i < settings.length(); i++) {
                    JSONObject setting = settings.getJSONObject(i);
                    int resID = getResource(setting.getString("drawable"));
                    BtnSetting btnSetting = new BtnSetting(getBaseContext(), resID, setting.getString("name"), setting.getInt("pageIndex"), new IAsynCallBackListener() {
                        @Override
                        public void onFinish(Object sender, Object data) {
                            viewPager.setCurrentItem((int) data);
                        }

                        @Override
                        public void onError(Object sender, Exception e) {

                        }
                    });
                    flexBox.addView(btnSetting);
                }
            }
        } catch (Exception e){
            e.printStackTrace();
            alert(GetExceptionMsg(e));
        }
    }

    //初始化设置项
    private void initSettings() {
        try {
            JSONObject setting = new JSONObject();
            setting.put("drawable", "icon_serverset");
            setting.put("name", "服务器设置");
            setting.put("pageIndex", "3");
            settings.put(setting);
            setting = new JSONObject();
            setting.put("drawable", "icon_goodssetting");
            setting.put("name", "卖品设置");
            setting.put("pageIndex", "4");
            settings.put(setting);
            setting = new JSONObject();
            setting.put("drawable", "icon_salesum");
            setting.put("name", "当天售卖汇总");
            setting.put("pageIndex", "6");
            settings.put(setting);
            setting = new JSONObject();
            setting.put("drawable", "icon_updatepwd");
            setting.put("name", "修改管理员\r\n密码");
            setting.put("pageIndex", "7");
            settings.put(setting);
            setting = new JSONObject();
            setting.put("drawable", "icon_checkdevice");
            setting.put("name", "外设检测");
            setting.put("pageIndex", "8");
            settings.put(setting);
            setting = new JSONObject();
            setting.put("drawable", "icon_checkupdate");
            setting.put("name", "检查更新");
            setting.put("pageIndex", "9");
            settings.put(setting);
        } catch (Exception e){
            e.printStackTrace();
            alert(GetExceptionMsg(e));
        }
    }

    //名称获取资源
    public int getResource(String imageName){
        Class drawable = R.drawable.class;
        try {
            Field field = drawable.getField(imageName);
            int resId = field.getInt(imageName);
            return resId;
        } catch (NoSuchFieldException e) {
            //如果没有在"drawable"下找到imageName,将会返回0
            return 0;
        } catch (IllegalAccessException e) {
            return 0;
        }
    }

    public void cancelBtnClick(View view) {
        viewPager.setCurrentItem(0);
    }

}
