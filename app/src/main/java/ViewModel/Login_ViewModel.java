package ViewModel;

import android.content.Context;
import android.view.View;
import android.view.WindowManager;

import com.example.administrator.drinkdevice.MainActivity;
import com.example.administrator.drinkdevice.R;

import org.json.JSONObject;

import Component.CustomViewPager;
import Const.PublicDefine;
import DataAccess.OptionDataAccess;
import Helper.ClearEditText;


public class Login_ViewModel extends Base_ViewModel{

    private CustomViewPager viewPager;
    private MainActivity_ViewModel mainViewModel;

    public EditBind pwdEdit = new EditBind();
    private ClearEditText loginPwd;

    public Login_ViewModel(Context ctx, CustomViewPager viewPager, MainActivity_ViewModel mainViewModel) {
        super(ctx);
        this.viewPager = viewPager;
        this.mainViewModel = mainViewModel;
    }

    public void Init() {
        try {
            pwdEdit.TextValue.set("");
            loginPwd = ((MainActivity)getBaseContext()).findViewById(R.id.loginPwd);
            loginPwd.setFocusable(true);
            loginPwd.setFocusableInTouchMode(true);
            loginPwd.requestFocus();
        } catch (Exception e){
            e.printStackTrace();
            alert(GetExceptionMsg(e));
        }
    }

    public void okBtnClick(View view) {
        try {
            String pwd = "123456";
            JSONObject pwdJson = PublicDefine.optionData.GetOption("adminPwd");
            if (pwdJson != null && !pwdJson.getString("svalue").equals("")){
                pwd = pwdJson.getString("svalue");
            }
            if (!pwdEdit.TextValue.get().equals(pwd)){
                throw new Exception("密码不正确");
            }

            //跳转设置页面
            viewPager.setCurrentItem(2);
        } catch (Exception e){
            e.printStackTrace();
            alert(GetExceptionMsg(e));
        }
    }

    public void cancelClick(View view) {
        //跳转选卖品页面
        viewPager.setCurrentItem(0);
    }
}
