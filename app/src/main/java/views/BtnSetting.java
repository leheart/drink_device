package views;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.administrator.drinkdevice.R;

import Interface.IAsynCallBackListener;


public class BtnSetting extends LinearLayout {
    public View view;

    public BtnSetting(Context context,int drawableTop,String name, final int pageIndex, final IAsynCallBackListener callback) {
        super(context);
        view = inflate(context, R.layout.btn_setting, this);
        ImageView settingImg = view.findViewById(R.id.settingImg);
        TextView settingName = view.findViewById(R.id.settingName);

        settingImg.setImageDrawable(getResources().getDrawable(drawableTop));
        settingName.setText(name);

        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onFinish(v,pageIndex);
            }
        });
    }

}
