package views;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administrator.drinkdevice.R;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.math.BigDecimal;

import Const.PublicDefine;
import DataAccess.DeviceDataAccess;
import Interface.IAsynCallBackListener;


public class View_GoodsSetting_GoodsInfo extends LinearLayout {
    public View view;
    private Context ctx;
    private TextView name;
    private TextView price;
    private TextView time;
    private Button btnTest;
    private TextView btnDelete;

    public View_GoodsSetting_GoodsInfo(Context context, final JSONObject goodsInfo, final IAsynCallBackListener deleteCallBack,final IAsynCallBackListener testCallBack) throws Exception {
        super(context);
        ctx = context;
        view = inflate(context, R.layout.view_goodssetting_goodsinfo, this);

        name = view.findViewById(R.id.name);
        price = view.findViewById(R.id.price);
        time = view.findViewById(R.id.time);
        btnTest = view.findViewById(R.id.btnTest);
        btnDelete = view.findViewById(R.id.btnDelete);

        name.setText(goodsInfo.getString("goodsName"));
        price.setText(new BigDecimal(goodsInfo.getDouble("goodsPrice")).setScale(2, BigDecimal.ROUND_HALF_UP)+"");
        if (goodsInfo.has("pickTime")){
            JSONObject pickTime = goodsInfo.getJSONObject("pickTime");
            String timeStr = pickTime.getInt("big")+"s";
            if (pickTime.has("small")){
                timeStr += "("+pickTime.getInt("small")+"s)";
            } else {
                timeStr += "(--)";
            }
            time.setText(timeStr);
        } else {
            time.setText("--(--)");
        }

        btnTest.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                testCallBack.onFinish(null,goodsInfo);
            }
        });

        btnDelete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteCallBack.onFinish(null,goodsInfo);
            }
        });
    }

}
