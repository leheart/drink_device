package views;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.administrator.drinkdevice.R;

import org.json.JSONObject;

import java.math.BigDecimal;

import Interface.IAsynCallBackListener;


public class View_SaleTodaySum_GoodsInfo extends LinearLayout {
    public View view;
    private Context ctx;
    private TextView name;
    private TextView num;
    private TextView price;

    public View_SaleTodaySum_GoodsInfo(Context context, final JSONObject goodsInfo) throws Exception {
        super(context);
        ctx = context;
        view = inflate(context, R.layout.view_saletodaysum_goodsinfo, this);

        num = view.findViewById(R.id.num);
        name = view.findViewById(R.id.name);
        price = view.findViewById(R.id.price);

        name.setText(goodsInfo.getString("goodsName"));
        num.setText(goodsInfo.getString("saleAmount"));
        price.setText(new BigDecimal(goodsInfo.getDouble("salePrice")).add(new BigDecimal(goodsInfo.getDouble("memberPrice"))).setScale(2, BigDecimal.ROUND_HALF_UP)+"");
    }

}
