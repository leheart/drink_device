package views;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.administrator.drinkdevice.R;

import org.json.JSONObject;

import java.math.BigDecimal;

import Interface.IAsynCallBackListener;


public class View_SelectGoods_GoodsInfo extends LinearLayout {
    public View view;
    private Context ctx;
    private TextView name;
    private TextView price;
    private TextView time;
    private TextView btnIce;
    private TextView btnNoIce;

    public View_SelectGoods_GoodsInfo(Context context, final JSONObject goodsInfo, final IAsynCallBackListener busiCallBack) throws Exception {
        super(context);
        ctx = context;
        view = inflate(context, R.layout.view_selectgoods_goodsinfo, this);

        name = view.findViewById(R.id.name);
        price = view.findViewById(R.id.price);
        time = view.findViewById(R.id.time);
        btnIce = view.findViewById(R.id.btnIce);
        btnNoIce = view.findViewById(R.id.btnNoIce);

        name.setText(goodsInfo.getString("goodsName"));
        price.setText(new BigDecimal(goodsInfo.getDouble("goodsPrice")).setScale(2, BigDecimal.ROUND_HALF_UP)+"");
        if (goodsInfo.has("pickTime")){
            JSONObject pickTime = goodsInfo.getJSONObject("pickTime");
            String timeStr = pickTime.getInt("big")+"s";
            if (pickTime.has("small")){
                timeStr += "("+pickTime.getInt("small")+"s)";
            } else {
                timeStr += "(--)";
            }
            time.setText(timeStr);
        } else {
            time.setText("--(--)");
        }

        btnIce.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                busiCallBack.onFinish(true,goodsInfo);
            }
        });

        btnNoIce.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                busiCallBack.onFinish(false,goodsInfo);
            }
        });
    }

}
