package views;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administrator.drinkdevice.MainActivity;
import com.example.administrator.drinkdevice.R;

import org.json.JSONArray;
import org.json.JSONObject;

import Const.PublicDefine;
import DataAccess.DeviceDataAccess;
import Device.colaMixing.slave_DeviceInfo;
import Interface.IAsynCallBackListener;


public class View_GoodsSetting_DeviceInfo extends LinearLayout {
    public View view;
    private Context ctx;

    private EditText name;
    private TextView id;
    private TextView online;
    private LinearLayout goodsList;
    private TextView addBtn;
    private Button position;
    private DeviceDataAccess deviceDataAccess;
    private JSONObject deviceInfo;
    private Dialog_GoodsSetting_AddGoods addGoodsView;
    private String deviceId;
    private LinearLayout addLinear;
    private IAsynCallBackListener callBack;

    public View_GoodsSetting_DeviceInfo(Context context, final String deviceId, String deviceOnline, IAsynCallBackListener callBack) throws Exception {
        super(context);
        ctx = context;
        view = inflate(context, R.layout.view_goodssetting_deviceinfo, this);
        this.deviceId = deviceId;
        this.callBack = callBack;

        name = view.findViewById(R.id.name);
        id = view.findViewById(R.id.id);
        online = view.findViewById(R.id.online);
        goodsList = view.findViewById(R.id.goodsList);
        addBtn = view.findViewById(R.id.addBtn);
        position = view.findViewById(R.id.position);
        addLinear = view.findViewById(R.id.addLinear);

        //获取设备信息
        deviceDataAccess = new DeviceDataAccess(context);
        deviceInfo = deviceDataAccess.getById(deviceId);
        if (deviceInfo == null){
            deviceInfo = new JSONObject();
            deviceInfo.put("id",deviceId);
            deviceInfo.put("name","未命名");
            deviceDataAccess.setDevice(deviceInfo);
        }

        showGoodsList();

        //设置显示信息
        name.setText(deviceInfo.getString("name"));
        id.setText(deviceId);
        online.setText(deviceOnline);

        //定位按钮点击事件
        position.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                PublicDefine.colaReader.position_Device(deviceId);
            }
        });

        //添加按钮点击事件
        addBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                addGoodsView = new Dialog_GoodsSetting_AddGoods(ctx, new IAsynCallBackListener() {
                    @Override
                    public void onFinish(Object sender, Object data) {
                        try {
                            JSONObject selectGoods = (JSONObject) data;
                            if (selectGoods == null){
                                throw new Exception("请选择商品");
                            }

                            deviceInfo = deviceDataAccess.getById(deviceId);
                            JSONArray goodsArray = deviceInfo.getJSONArray("goodsList");
                            for (int i = 0; i < goodsArray.length(); i++) {
                                JSONObject goodsItem = goodsArray.getJSONObject(i);
                                if (goodsItem.getInt("goodsId") == selectGoods.getInt("goodsId")){
                                    throw new Exception("此商品已选择");
                                }
                            }

                            goodsArray.put(selectGoods);
                            deviceInfo.put("goodsList",goodsArray);
                            deviceDataAccess.setDevice(deviceInfo);
                            showGoodsList();
                            addGoodsView.dismiss();
                        } catch (Exception e){
                            e.printStackTrace();
                            alert(e.getMessage());
                        }
                    }

                    @Override
                    public void onError(Object sender, Exception e) {
                        e.printStackTrace();
                        alert(e.getMessage());
                    }
                });
            }
        });

        name.setOnFocusChangeListener(new android.view.View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                try {
                    if (!hasFocus) {
                        deviceInfo.put("name", name.getText());
                        deviceDataAccess.setDevice(deviceInfo);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }


    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.obj instanceof IAsynCallBackListener) {
                ((IAsynCallBackListener) msg.obj).onFinish("", msg.arg1);
            }
        }
    };

    public void alert(final String info) {
        Message msg = new Message();
        msg.obj = new IAsynCallBackListener() {
            @Override
            public void onFinish(Object sender, Object data) {
                View toastRoot = ((MainActivity)ctx).getLayoutInflater().inflate(R.layout.my_toast, null);
                Toast toast=new Toast(ctx);
                toast.setView(toastRoot);
                TextView tv=(TextView)toastRoot.findViewById(R.id.TextViewInfo);
                tv.setText(info);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }

            @Override
            public void onError(Object sender, Exception e) {

            }
        };
        mHandler.sendMessage(msg);
    }

    public void showGoodsList() throws Exception {
        deviceInfo = deviceDataAccess.getById(deviceId);
        JSONArray goodsArray = deviceInfo.getJSONArray("goodsList");
        goodsList.removeAllViews();
        for (int i = 0; i < goodsArray.length(); i++) {
            JSONObject goodsInfo = goodsArray.getJSONObject(i);
            View_GoodsSetting_GoodsInfo goodsView = new View_GoodsSetting_GoodsInfo(ctx, goodsInfo, new IAsynCallBackListener() {
                @Override
                public void onFinish(Object sender, Object data) {
                    try {
                        JSONObject goodsInfo = (JSONObject) data;
                        deviceInfo = deviceDataAccess.getById(deviceId);
                        JSONArray goodsArray = deviceInfo.getJSONArray("goodsList");
                        for (int i = 0; i < goodsArray.length(); i++) {
                            JSONObject goodsItem = goodsArray.getJSONObject(i);
                            if (goodsItem.getInt("goodsId") == goodsInfo.getInt("goodsId")) {
                                goodsArray.remove(i);
                                break;
                            }
                        }
                        deviceInfo.put("goodsList", goodsArray);
                        deviceDataAccess.setDevice(deviceInfo);
                        showGoodsList();
                    } catch (Exception e) {
                        e.printStackTrace();
                        alert(e.getMessage());
                    }
                }

                @Override
                public void onError(Object sender, Exception e) {

                }
            }, new IAsynCallBackListener() {
                @Override
                public void onFinish(Object sender, Object data) {
                    try {
                        JSONObject goodsInfo = (JSONObject) data;
                        JSONObject res = new JSONObject();
                        res.put("deviceId", deviceId);
                        res.put("goodsId", goodsInfo.getInt("goodsId"));
                        callBack.onFinish(null, res);
                    } catch (Exception e){
                        e.printStackTrace();
                        alert(e.getMessage());
                    }
                }

                @Override
                public void onError(Object sender, Exception e) {

                }
            });
            goodsList.addView(goodsView);
        }
        if (goodsArray.length() >= 3){
            addLinear.setVisibility(GONE);
        } else {
            addLinear.setVisibility(VISIBLE);
        }
    }
}
