package views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administrator.drinkdevice.MainActivity;
import com.example.administrator.drinkdevice.R;

import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;

import BaseActivity.BaseActivity;
import Helper.Log;
import Interface.IAsynCallBackListener;


public class Activity_SelectGoods_GoodsInfo extends LinearLayout {
    public View view;
    private Context ctx;
    private LinearLayout line;
    private ImageView img;
    private TextView name;
    private TextView cash;
    private TextView code;
    private JSONObject goodsInfo;

    public Activity_SelectGoods_GoodsInfo(Context context, final JSONObject goodsInfo, final IAsynCallBackListener callBack) throws Exception {
        super(context);
        ctx = context;
        view = inflate(context, R.layout.activity_selectgoods_goodsinfo, this);

        view.setTag(false);

        line = view.findViewById(R.id.line);
        img = view.findViewById(R.id.img);
        cash = view.findViewById(R.id.cash);
        name = view.findViewById(R.id.name);
        code = view.findViewById(R.id.code);

        this.goodsInfo = goodsInfo;
        setWebImg(goodsInfo.getString("goodsImageXl"));
        cash.setText(new BigDecimal(goodsInfo.getDouble("goodsPrice")).setScale(2, BigDecimal.ROUND_HALF_UP) + "");
        name.setText(goodsInfo.getString("goodsName"));
        code.setText(goodsInfo.getString("goodsCode"));

        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onFinish(view, goodsInfo);
            }
        });
    }

    private void setWebImg(String path) throws Exception {
        try {
            if (path == null || path.equals("")) {
                return;
            }
            new DownloadImageTask().execute(path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Drawable> {

        protected Drawable doInBackground(String... urls) {
            return loadImageFromNetwork(urls[0]);
        }

        protected void onPostExecute(Drawable result) {

            if (result != null) {
                img.setImageDrawable(result);
            }
        }
    }

    private Drawable loadImageFromNetwork(String imageUrl) {
        Drawable drawable = null;
        try {
            // 可以在这里通过文件名来判断，是否本地有此图片
            drawable = Drawable.createFromStream(
                    new URL(imageUrl).openStream(), "image.jpg");
        } catch (IOException e) {
            Log.write("test", e.getMessage());
        }
        if (drawable == null) {
            Log.d("test", "null drawable");
        } else {
            Log.d("test", "not null drawable");
        }

        return drawable;
    }

    public void setCheck(boolean check) {
        try {
            view.setTag(check);
            if (check) {
                LinearLayout.LayoutParams lp = (LayoutParams) line.getLayoutParams();
                lp.width = 330;
                lp.height = 450;
                lp.setMargins(0,0,0,0);
                line.setLayoutParams(lp);
                line.setBackground(ctx.getResources().getDrawable(R.drawable.border_all_pink_corners));

                lp = (LayoutParams) img.getLayoutParams();
                lp.width = 330;
                lp.height = 265;
                img.setLayoutParams(lp);

                cash.setTextSize(27);
                name.setTextSize(27);
                code.setTextSize(25);
            } else {
                LinearLayout.LayoutParams lp = (LayoutParams) line.getLayoutParams();
                lp.width = 300;
                lp.height = 390;
                lp.setMargins(15,25,15,25);
                line.setLayoutParams(lp);
                line.setBackground(ctx.getResources().getDrawable(R.drawable.border_all_corners));

                lp = (LayoutParams) img.getLayoutParams();
                lp.width = 300;
                lp.height = 255;
                img.setLayoutParams(lp);

                cash.setTextSize(25);
                name.setTextSize(22);
                code.setTextSize(23);
            }
        } catch (Exception e) {
            e.printStackTrace();
            alert(e.getMessage());
        }
    }

    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.obj instanceof IAsynCallBackListener) {
                ((IAsynCallBackListener) msg.obj).onFinish("", msg.arg1);
            }
        }
    };

    public void alert(final String info) {
        Message msg = new Message();
        msg.obj = new IAsynCallBackListener() {
            @Override
            public void onFinish(Object sender, Object data) {
                View toastRoot = ((MainActivity)ctx).getLayoutInflater().inflate(R.layout.my_toast, null);
                Toast toast=new Toast(ctx);
                toast.setView(toastRoot);
                TextView tv=(TextView)toastRoot.findViewById(R.id.TextViewInfo);
                tv.setText(info);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }

            @Override
            public void onError(Object sender, Exception e) {

            }
        };
        mHandler.sendMessage(msg);
    }
}
