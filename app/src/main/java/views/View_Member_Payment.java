package views;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.administrator.drinkdevice.R;

import org.json.JSONObject;

import java.math.BigDecimal;

import Interface.IAsynCallBackListener;


public class View_Member_Payment extends LinearLayout {
    public View view;
    private Context ctx;
    private ImageView paymentCheckImg;
    private TextView paymentName;
    private TextView paymentCash;
    private TextView payCashInfo;

    public View_Member_Payment(Context context, final JSONObject paymentInfo, final IAsynCallBackListener busiCallBack) throws Exception {
        super(context);
        ctx = context;
        view = inflate(context, R.layout.view_payment, this);
        view.setTag(false);

        paymentCheckImg = view.findViewById(R.id.paymentCheckImg);
        paymentName = view.findViewById(R.id.paymentName);
        paymentCash = view.findViewById(R.id.paymentCash);
        payCashInfo = view.findViewById(R.id.payCashInfo);

        paymentCheckImg.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_uncheck));
        paymentName.setText(paymentInfo.getString("storedcardName"));
        paymentCash.setText(new BigDecimal(paymentInfo.getDouble("value")).setScale(2, BigDecimal.ROUND_HALF_UP) + "");

        paymentCheckImg.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                busiCallBack.onFinish(view,paymentInfo);
            }
        });
    }

    public void setCheck(boolean check,String payInfo) {
        view.setTag(check);
        if (check){
            paymentCheckImg.setImageDrawable(ctx.getResources().getDrawable(R.drawable.icon_check));
            payCashInfo.setText(payInfo);
        } else {
            paymentCheckImg.setImageDrawable(ctx.getResources().getDrawable(R.drawable.icon_uncheck));
            payCashInfo.setText("");
        }
    }
}
