package views;


import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.administrator.drinkdevice.AddSelectGoodsActivity;
import com.example.administrator.drinkdevice.MainActivity;
import com.example.administrator.drinkdevice.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.URL;

import Const.PublicDefine;
import Entity.ActivityResult;
import Helper.Log;
import Interface.IAsynCallBackListener;

public class Dialog_Settlement {
    public Context context;
    private Dialog dialog;
    private Button cancelBtn;
    private Button okBtn;
    private TextView name;
    private TextView code;
    private TextView price;
    private ImageView img;
    private ImageView check1;
    private ImageView check2;
    private LinearLayout commentLiear;

    private int check = 0;


    public Dialog_Settlement(final Context context, JSONObject goodsInfo, final IAsynCallBackListener callBack) {
        try {
            this.context = context;
            dialog = new Dialog(context);
            dialog.show();
            dialog.setCancelable(false);
            Window window = dialog.getWindow();
            window.setGravity(Gravity.CENTER);
            window.setContentView(R.layout.dialog_settlement);
            window.setDimAmount(0);
            WindowManager.LayoutParams params = window.getAttributes();
            // 去除四角黑色背景
            window.setBackgroundDrawable(new BitmapDrawable());
            // 设置周围的暗色系数
            params.dimAmount = 0.5f;
            window.setAttributes(params);

            name = dialog.findViewById(R.id.name);
            code = dialog.findViewById(R.id.code);
            price = dialog.findViewById(R.id.price);
            img = dialog.findViewById(R.id.img);
            okBtn = dialog.findViewById(R.id.settleOk);
            cancelBtn = dialog.findViewById(R.id.settleCancel);
            check1 = dialog.findViewById(R.id.checkbox1);
            check2 = dialog.findViewById(R.id.checkbox2);
            commentLiear = dialog.findViewById(R.id.commentLiear);


            name.setText(goodsInfo.getString("goodsName"));
            code.setText(goodsInfo.getString("goodsCode"));
            price.setText(new BigDecimal(goodsInfo.getDouble("goodsPrice")).setScale(2, BigDecimal.ROUND_HALF_UP) + "");
            setWebImg(goodsInfo.getString("goodsImageXl"));

            if (goodsInfo.has("pickTime")) {
                JSONObject pickTime = goodsInfo.getJSONObject("pickTime");
                if (pickTime.has("big") && pickTime.has("small")) {
                    commentLiear.setVisibility(View.VISIBLE);
                    check = 1;
                } else {
                    if (pickTime.has("big")) {
                        check = 1;
                    } else if (pickTime.has("small")) {
                        check = 2;
                    }
                }
            }

            check1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setCheck(v);
                }
            });

            check2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setCheck(v);
                }
            });

            //返回回调
            cancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            //确定回调
            okBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBack.onFinish(null, check);
                    dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            callBack.onError(null, e);
        }
    }

    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.obj instanceof IAsynCallBackListener) {
                ((IAsynCallBackListener) msg.obj).onFinish("", msg.arg1);
            }
        }
    };

    private void setWebImg(String path) throws Exception {
        try {
            if (path == null || path.equals("")) {
                return;
            }
            new DownloadImageTask().execute(path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Drawable> {

        protected Drawable doInBackground(String... urls) {
            return loadImageFromNetwork(urls[0]);
        }

        protected void onPostExecute(Drawable result) {

            if (result != null) {
                img.setImageDrawable(result);
            }
        }
    }

    private Drawable loadImageFromNetwork(String imageUrl) {
        Drawable drawable = null;
        try {
            // 可以在这里通过文件名来判断，是否本地有此图片
            drawable = Drawable.createFromStream(
                    new URL(imageUrl).openStream(), "image.jpg");
        } catch (IOException e) {
            Log.write("test", e.getMessage());
        }
        if (drawable == null) {
            Log.d("test", "null drawable");
        } else {
            Log.d("test", "not null drawable");
        }

        return drawable;
    }

    public void dismiss() {
        dialog.dismiss();
    }

    private void setCheck(View view) {
        if (view == check1) {
            check1.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_check));
            check2.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_uncheck));
            check = 1;
        } else {
            check1.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_uncheck));
            check2.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_check));
            check = 2;
        }
    }
}
