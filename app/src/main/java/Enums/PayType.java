package Enums;

import Annotation.Description;

public enum PayType {
    @Description("未知")
    PAYMENT_UNKNOW,
    @Description("积分")
    PAYMENT_CREDIT,
    @Description("微信")
    PAYMENT_WECHAT,
    @Description("余额")
    PAYMENT_BALANCE,
    @Description("现金")
    PAYMENT_CASH,
    @Description("微信刷卡")
    PAYMENT_WECHATMIR,
    @Description("储值卡")
    PAYMENT_STORECARD,
    @Description("支付宝刷卡")
    PAYMENT_ALIMIR
}
