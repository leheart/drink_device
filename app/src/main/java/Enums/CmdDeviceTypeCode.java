package Enums;

import Annotation.Description;

/**
 * 出卡板命令的设备类型码
 * Created by 王彦鹏 on 2018-03-29.
 */

public enum CmdDeviceTypeCode {
    @Description("未定义")
    Device_Null,
    @Description("电子投币器")
    Device_CoinBox,
    @Description("彩票机")
    Device_Lottory,
    @Description("闸机、吧台读卡客显板")
    Device_GateDisplayBoard,
    Device_Null4,
    @Description("自助吧台出币机板")
    Device_AtmCoinBoard,
    @Description("自助吧台协处理器")
    Device_AtmMainBoard,
    @Description("ATM06出卡机控制板")
    Device_ATM06MainBoard
}
