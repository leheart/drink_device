package Struct;

import Annotation.StructIndex;

public class ScanRes extends StructBase {
    /**
     * 版本
     */
    @StructIndex(0)
    private BinaryType ver=new BinaryType(1,byte.class);
    @StructIndex(1)
    private BinaryType result=new BinaryType(1,byte.class);
    @StructIndex(2)
    private BinaryType reserve=new BinaryType(12,byte[].class);
    @StructIndex(3)
    private BinaryType crc=new BinaryType(2,short.class);

    public BinaryType getVer() {
        return ver;
    }

    public void setVer(BinaryType ver) {
        this.ver = ver;
    }

    public BinaryType getResult() {
        return result;
    }

    public void setResult(BinaryType result) {
        this.result = result;
    }

    public BinaryType getReserve() {
        return reserve;
    }

    public void setReserve(BinaryType reserve) {
        this.reserve = reserve;
    }

    public BinaryType getCrc() {
        return crc;
    }

    public void setCrc(BinaryType crc) {
        this.crc = crc;
    }
}
