package Struct;

import Annotation.Description;
import Annotation.StructIndex;

/**
 * Created by 王彦鹏 on 2018-04-11.
 */

public class OPTypeConfig extends StructBase {
    @StructIndex(0)
    @Description("业务类型")
    private BinaryType opType=new BinaryType(1,byte.class);
    @StructIndex(1)
    @Description("位置与是否启用（位置高 7 位,是否启用，低 1 位）")
    private BinaryType state=new BinaryType(1,byte.class);
    @StructIndex(2)
    @Description("数据校验")
    private BinaryType crcCode=new BinaryType(2,short.class);

    public BinaryType getCrcCode() {
        return crcCode;
    }

    public void setCrcCode(BinaryType crcCode) {
        this.crcCode = crcCode;
    }

    public BinaryType getOpType() {
        return opType;
    }

    public void setOpType(BinaryType opType) {
        this.opType = opType;
    }

    public BinaryType getState() {
        return state;
    }

    public void setState(BinaryType state) {
        this.state = state;
    }
}
