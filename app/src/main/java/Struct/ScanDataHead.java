package Struct;

import Annotation.StructIndex;

public class ScanDataHead extends StructBase {
    @StructIndex(0)
    private BinaryType head = new BinaryType(3,byte[].class);
    /**
     * 版本
     */
    @StructIndex(1)
    private BinaryType ver=new BinaryType(1,byte.class);
    @StructIndex(2)
    private BinaryType cmdId=new BinaryType(4,int.class);
    @StructIndex(3)
    private BinaryType cmd=new BinaryType(2,short.class);
    @StructIndex(4)
    private BinaryType dataLen=new BinaryType(4,int.class);

    public BinaryType getHead() {
        return head;
    }

    public void setHead(BinaryType head) {
        this.head = head;
    }

    public BinaryType getVer() {
        return ver;
    }

    public void setVer(BinaryType ver) {
        this.ver = ver;
    }

    public BinaryType getCmdId() {
        return cmdId;
    }

    public void setCmdId(BinaryType cmdId) {
        this.cmdId = cmdId;
    }

    public BinaryType getCmd() {
        return cmd;
    }

    public void setCmd(BinaryType cmd) {
        this.cmd = cmd;
    }

    public BinaryType getDataLen() {
        return dataLen;
    }

    public void setDataLen(BinaryType dataLen) {
        this.dataLen = dataLen;
    }
}
