package Struct;

import Annotation.StructIndex;

/**
 * 设备通讯协议
 * Created by 王彦鹏 on 2018-04-03.
 */
public class DeviceBoardProtocol extends StructBase {
    /**
     * 命令头
     */
    @StructIndex(0)
    private BinaryType head=new BinaryType(3,byte[].class);
    @StructIndex(1)
    private BinaryType len=new BinaryType(1,byte.class);
    @StructIndex(2)
    private BinaryType lenReverse=new BinaryType(1,byte.class);
    @StructIndex(3)
    private BinaryType DeviceType=new BinaryType(1,byte.class);
    @StructIndex(4)
    private BinaryType cmd=new BinaryType(1,byte.class);
    @StructIndex(5)
    private BinaryType data=new BinaryType(0,byte[].class);
    @StructIndex(6)
    private BinaryType sumCode=new BinaryType(1,byte.class);
    @StructIndex(7)
    private BinaryType endCode=new BinaryType(1,byte.class);

    public BinaryType getHead() {
        return head;
    }

    /**
     * 设置命令头
     * @param head
     */
    public void setHead(BinaryType head) {
        this.head = head;
    }

    public BinaryType getLen() {
        return len;
    }

    public void setLen(BinaryType len) {
        this.len = len;
    }

    public BinaryType getLenReverse() {
        return lenReverse;
    }

    public void setLenReverse(BinaryType lenReverse) {
        this.lenReverse = lenReverse;
    }

    public BinaryType getDeviceType() {
        return DeviceType;
    }

    public void setDeviceType(BinaryType deviceType) {
        DeviceType = deviceType;
    }

    public BinaryType getCmd() {
        return cmd;
    }

    public void setCmd(BinaryType cmd) {
        this.cmd = cmd;
    }

    public BinaryType getData() {
        return data;
    }

    public void setData(BinaryType data) {
        this.data = data;
    }

    public BinaryType getSumCode() {
        return sumCode;
    }

    public void setSumCode(BinaryType sumCode) {
        this.sumCode = sumCode;
    }

    public BinaryType getEndCode() {
        return endCode;
    }

    public void setEndCode(BinaryType endCode) {
        this.endCode = endCode;
    }
}
