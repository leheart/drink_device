package Struct;

import Annotation.Description;
import Annotation.StructIndex;

/**
 * 作者：王彦鹏 on 2019-04-29 13:47
 * QQ:5284328
 * 邮箱：snowstorm_wypeng@qq.com
 * 公司：盈加电子科技有限公司
 */
public class ReaderState extends StructBase {
    /*
    BYTE HardVer[8];//硬件版本
	BYTE SoftVer[8];//软件版本
	BYTE CardState;//卡状态：0=无卡，1=卡存在
	BYTE CardType;//卡种类：01=s50卡，02=s70，03=ML
	BYTE DevId[12];//设备ID


    * */
    @StructIndex(0)
    @Description("硬件版本")
    private BinaryType hardVer=new BinaryType(8,String.class);
    @StructIndex(1)
    @Description("软件版本")
    private BinaryType softVer=new BinaryType(8,String.class);
    @StructIndex(2)
    @Description("卡状态：0=无卡，1=卡存在")
    private BinaryType cardState=new BinaryType(1,byte.class);
    @StructIndex(3)
    @Description("卡种类：01=s50卡，02=s70，03=ML")
    private BinaryType cardKind=new BinaryType(1,byte.class);
    @StructIndex(4)
    @Description("设备ID")
    private BinaryType deviceId=new BinaryType(1,byte.class);

    public BinaryType getHardVer() {
        return hardVer;
    }

    public void setHardVer(BinaryType hardVer) {
        this.hardVer = hardVer;
    }

    public BinaryType getSoftVer() {
        return softVer;
    }

    public void setSoftVer(BinaryType softVer) {
        this.softVer = softVer;
    }

    public BinaryType getCardState() {
        return cardState;
    }

    public void setCardState(BinaryType cardState) {
        this.cardState = cardState;
    }

    public BinaryType getCardKind() {
        return cardKind;
    }

    public void setCardKind(BinaryType cardKind) {
        this.cardKind = cardKind;
    }

    public BinaryType getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(BinaryType deviceId) {
        this.deviceId = deviceId;
    }
}
