package Struct;

import Annotation.StructIndex;

public class QrData extends StructBase {
    @StructIndex(0)
    private BinaryType ver = new BinaryType(1,byte.class);
    /**
     * 版本
     */
    @StructIndex(1)
    private BinaryType data=new BinaryType(1,String.class);
    @StructIndex(2)
    private BinaryType crc=new BinaryType(2,short.class);

    public BinaryType getVer() {
        return ver;
    }

    public void setVer(BinaryType ver) {
        this.ver = ver;
    }

    public BinaryType getData() {
        return data;
    }

    public void setData(BinaryType data) {
        this.data = data;
    }

    public BinaryType getCrc() {
        return crc;
    }

    public void setCrc(BinaryType crc) {
        this.crc = crc;
    }
}
