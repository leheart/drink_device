package Struct;

import Annotation.Description;
import Annotation.StructIndex;

/**
 * Created by 王彦鹏 on 2018-04-11.
 */

public class BusiState extends StructBase {
    @StructIndex(0)
    @Description("业务类型")
    private BinaryType busiType =new BinaryType(1,byte.class);
    @StructIndex(1)
    @Description("业务状态")
    private BinaryType busiState=new BinaryType(1,byte.class);
    @StructIndex(2)
    @Description("业务金额（分）")
    private BinaryType cash=new BinaryType(4,int.class);
    @StructIndex(3)
    @Description("订单号")
    private BinaryType orderNo=new BinaryType(30,String.class);
    @StructIndex(4)
    @Description("订单状态")
    private BinaryType orderState=new BinaryType(1,int.class);
    @StructIndex(5)
    @Description("支付类型")
    private BinaryType payType=new BinaryType(2,byte.class);
    @StructIndex(6)
    @Description("业务时间")
    private BinaryType busiDate=new BinaryType(6,String.class);
    @StructIndex(7)
    @Description("卡号")
    private BinaryType cardNo=new BinaryType(4,int.class);
    @StructIndex(8)
    @Description("币值")
    private BinaryType busiCoin=new BinaryType(2,short.class);
    @StructIndex(9)
    @Description("保留")
    private BinaryType rsved=new BinaryType(13,byte[].class);
    @StructIndex(10)
    @Description("数据校验")
    private BinaryType crcCode=new BinaryType(2,short.class);

    public BinaryType getBusiCoin() {
        return busiCoin;
    }

    public void setBusiCoin(BinaryType busiCoin) {
        this.busiCoin = busiCoin;
    }

    public BinaryType getCrcCode() {
        return crcCode;
    }

    public void setCrcCode(BinaryType crcCode) {
        this.crcCode = crcCode;
    }

    public BinaryType getBusiType() {
        return busiType;
    }

    public void setBusiType(BinaryType busiType) {
        this.busiType = busiType;
    }

    public BinaryType getBusiState() {
        return busiState;
    }

    public void setBusiState(BinaryType busiState) {
        this.busiState = busiState;
    }

    public BinaryType getCash() {
        return cash;
    }

    public void setCash(BinaryType cash) {
        this.cash = cash;
    }

    public BinaryType getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(BinaryType orderNo) {
        this.orderNo = orderNo;
    }

    public BinaryType getOrderState() {
        return orderState;
    }

    public void setOrderState(BinaryType orderState) {
        this.orderState = orderState;
    }

    public BinaryType getPayType() {
        return payType;
    }

    public void setPayType(BinaryType payType) {
        this.payType = payType;
    }

    public BinaryType getBusiDate() {
        return busiDate;
    }

    public void setBusiDate(BinaryType busiDate) {
        this.busiDate = busiDate;
    }

    public BinaryType getRsved() {
        return rsved;
    }

    public void setRsved(BinaryType rsved) {
        this.rsved = rsved;
    }

    public BinaryType getCardNo() {
        return cardNo;
    }

    public void setCardNo(BinaryType cardNo) {
        this.cardNo = cardNo;
    }
}
