package Struct;

import Annotation.StructIndex;

/**
 * Created by 王彦鹏 on 2018-03-14.
 */

public class UpdFileStruct extends StructBase {
    @StructIndex(0)
    private BinaryType diviceType = new BinaryType(4,int.class);//   版本	1
    @StructIndex(1)
    private BinaryType deviceVer = new BinaryType(4,int.class);//    押金 1
    @StructIndex(2)
    private BinaryType chipType = new BinaryType(4,int.class);//    累计充A币	3
    @StructIndex(3)
    private BinaryType remain = new BinaryType(4,int.class);//   累计充币	3
    @StructIndex(4)
    private BinaryType fileSize = new BinaryType(4,int.class);//   累计充币	3

    public BinaryType getDiviceType() {
        return diviceType;
    }

    public void setDiviceType(BinaryType diviceType) {
        this.diviceType = diviceType;
    }

    public BinaryType getDeviceVer() {
        return deviceVer;
    }

    public void setDeviceVer(BinaryType deviceVer) {
        this.deviceVer = deviceVer;
    }

    public BinaryType getChipType() {
        return chipType;
    }

    public void setChipType(BinaryType chipType) {
        this.chipType = chipType;
    }

    public BinaryType getRemain() {
        return remain;
    }

    public void setRemain(BinaryType remain) {
        this.remain = remain;
    }

    public BinaryType getFileSize() {
        return fileSize;
    }

    public void setFileSize(BinaryType fileSize) {
        this.fileSize = fileSize;
    }



}
