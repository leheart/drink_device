package Component;

import android.app.AlertDialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.administrator.drinkdevice.R;

import Device.Android.PhoneSound;
import Enums.MsgType;
import Helper.Msgbox;
import Interface.IAsynCallBackListener;

public class tipDialog{

    public View view;
    private AlertDialog dialog;
    private IAsynCallBackListener ExchangeEvent;

    public tipDialog(Context ctx, String title, String message,
                     MsgType msgType, final IAsynCallBackListener callback) {
        dialog= Msgbox.ShowDialog(ctx, R.layout.alert_dialog);
        //获取当前Activity所在的窗体
        Window dialogWindow = dialog.getWindow();
        //设置Dialog从窗体底部弹出
        dialogWindow.setGravity( Gravity.CENTER);
        //获得窗体的属性
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        //设置窗口宽度为充满全屏
        lp.width = 700;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.y = 20;//设置Dialog距离底部的距离
        //将属性设置给窗体
        dialogWindow.setAttributes(lp);
        dialog.setCancelable(false);
        ExchangeEvent=callback;
        ((TextView)dialog.findViewById(R.id.alertText)).setText(message);
        ((TextView)dialog.findViewById(R.id.tv_dialog_title)).setText(title);
        ((Button)dialog.findViewById(R.id.btnsuccess)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExchangeEvent.onFinish("","");
            }
        });
        switch (msgType)
        {
            case msg_Error:
            {
                PhoneSound.play(ctx, R.raw.hint_2);
                break;
            }
            case msg_Succeed:
            {
                PhoneSound.play(ctx,R.raw.hint_8);
                break;
            }
            case msg_Input:
            {
                PhoneSound.play(ctx,R.raw.hint_10);
                //items.add(new ItemBean(R.drawable.icon_hint,message));
                final EditText et = new EditText(ctx);
                et.setSingleLine();
                et.setText(message);
                break;
            }
            case msg_warning: {
                PhoneSound.play(ctx, R.raw.hint_3);
                break;
            }
            case msg_Query:
            {
                PhoneSound.play(ctx,R.raw.hint_8);
                break;
            }
            default:{
                PhoneSound.play(ctx,R.raw.hint_1);
                break;
            }
        }
    }
}
